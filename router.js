import 'react-native-gesture-handler';
import React from "react";
import {isIphoneX} from 'react-native-iphone-x-helper';
import { Platform, Alert, StatusBar, Image, Dimensions, View } from "react-native";
import { createAppContainer, NavigationActions, TabView, TabBarBottom } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Colors from './constants/Colors';
import Common from './Utilites/Common';
import NavBar from './components/Main/NavBar';
import ProfileNavBar from './components/ProfileNavBar';
import ItemNavBar from './components/ItemNavBar';
import InfoNavBar from './components/InfoNavBar';

import SplashScreen from './screens/SplashScreen';
import ScannerScreen from './screens/ScannerScreen';
import LoginScreen from './screens/LoginScreen';
import CodeScreen from './screens/CodeScreen';
import MainScreen from './screens/MainScreen';
import ProfileScreen from './screens/ProfileScreen';
import ChatScreen from './screens/ChatScreen';
import NotificationsScreen from './screens/NotificationsScreen';
import ItemScreen from './screens/ItemScreen';
import CouItemScreen from './screens/CouItemScreen';
import CaseSettingsScreen from './screens/CaseSettingsScreen';
import CouCaseSettingsScreen from './screens/CouCaseSettingsScreen';
import CompanyScreen from './screens/CompanyScreen';
import CompanyInfoScreen from './screens/CompanyInfoScreen';
import OkvedsScreen from './screens/OkvedsScreen';
import WordScreen from './screens/WordScreen';

import BackButton from './components/Buttons/BackButton';
import ExitButton from './components/Buttons/ExitButton';
import ChatButton from './components/Buttons/ChatButton';
import SettingsButton from './components/Buttons/SettingsButton';
import NotificationButton from './components/Buttons/NotificationButton';

import CustomTabbar from './components/CustomTabbar'; 

const LoginStack = createStackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: false,
        // headerLeft: <MenuButton navigation={navigation} />,
        // headerRight: <View />,
        titleStyle: {
          alignSelf: 'center',
        },
        headerForceInset: {
          left: 0,
          right: 0,
          top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
          bottom: 0,
        },
        headerTitleStyle: {
            color: 'white',
            textAlign: 'center',
            alignSelf:'center',
            flexGrow: 1,
        },
        headerTransparent: true,
        headerStyle: {
            backgroundColor: 'transparent',
            borderBottomWidth: 0,
            elevation: 0,
            shadowOpacity: 0,
            shadowColor: 'transparent',
            marginTop: Platform.OS === "android" ? (Platform.Version < 21 ? StatusBar.currentHeight : 0) : 0,
        },
        headerTintColor: {
            color: 'red'
        },
      }
    }
  },
  Code: {
    screen: CodeScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: false,
        headerLeft: <View />,
        headerRight: <View />,
        titleStyle: {
          alignSelf: 'center',
        },
        headerForceInset: {
          left: 0,
          right: 0,
          top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
          bottom: 0,
        },
        headerTitleStyle: {
            color: 'white',
            textAlign: 'center',
            alignSelf:'center',
            flexGrow: 1,
        },
        headerTransparent: true,
        headerStyle: {
            backgroundColor: 'transparent',
            borderBottomWidth: 0,
            elevation: 0,
            shadowOpacity: 0,
            shadowColor: 'transparent',
            marginTop: Platform.OS === "android" ? (Platform.Version < 21 ? StatusBar.currentHeight : 0) : 0,
        },
        headerTintColor: {
            color: 'red'
        },
      }
    }
  },
  Profile: {
    screen: ProfileScreen,
    navigationOptions: ({navigation}) => {
      return {
        gesturesEnabled: false,
        headerLeft: <View />,
        headerRight: <View />,
        titleStyle: {
          alignSelf: 'center',
        },
        headerForceInset: {
          left: 0,
          right: 0,
          top: Platform.OS === 'ios' ? (isIphoneX() ? 44 : 20) : 0,
          bottom: 0,
        },
        headerTitleStyle: {
            color: 'white',
            textAlign: 'center',
            alignSelf:'center',
            flexGrow: 1,
        },
        headerTransparent: true,
        headerStyle: {
            backgroundColor: 'transparent',
            borderBottomWidth: 0,
            elevation: 0,
            shadowOpacity: 0,
            shadowColor: 'transparent',
            marginTop: Platform.OS === "android" ? (Platform.Version < 21 ? StatusBar.currentHeight : 0) : 0,
        },
        headerTintColor: {
            color: 'red'
        },
      }
    }
  },
}, {
  headerLayoutPreset: 'left',
});

export const SignedIn = createBottomTabNavigator(
  {
    MainTab: {
      screen: createStackNavigator({
        Main: {
          screen: MainScreen,
          headerMode: 'none',
          // header: null,
          navigationOptions: ({navigation}) => {
            return {
              header: (<NavBar navigation={navigation} button={<View style={{flexDirection: 'row'}}><NotificationButton navigation={navigation}/></View>} />),
              gesturesEnabled: false,
              headerTransparent: true,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
              },
              // headerForceInset: { top: 'never', bottom: 'never' }
            }
          }
        },
        Notifications: {
          screen: NotificationsScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: (<ItemNavBar navigation={navigation} />),
              headerTransparent: true,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
        Item: {
          screen: ItemScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: (<ItemNavBar navigation={navigation} settings={true} />),
              headerTransparent: true,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
        CouItem: {
          screen: CouItemScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: (<ItemNavBar navigation={navigation} settings={true} />),
              headerTransparent: true,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
        CaseSettings: {
          screen: CaseSettingsScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: (<ItemNavBar navigation={navigation} />),
              headerTransparent: true,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
        CouCaseSettings: {
          screen: CouCaseSettingsScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: (<ItemNavBar navigation={navigation} />),
              headerTransparent: true,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
        Word: {
          screen: WordScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: (<ItemNavBar navigation={navigation} />),
              headerTransparent: true,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
        Company: {
          screen: CompanyScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: (<ItemNavBar navigation={navigation} />),
              headerTransparent: true,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
        CompanyInfo: {
          screen: CompanyInfoScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: (<InfoNavBar navigation={navigation} />),
              headerTransparent: false,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(70) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
        Okveds: {
          screen: OkvedsScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: (<InfoNavBar navigation={navigation} />),
              headerTransparent: false,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(70) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
      }, {
        // headerMode: 'none',
      }),
      navigationOptions: ({navigation}) => {
        return {
          tabBarIcon: ({ tintColor }) => {
            return (<Image
              source={require('./assets/ic-tab1.png')}
              style={{
                resizeMode: 'contain',
                width: 24,
                height: 24,
                // marginTop: 8,
                // tintColor: tintColor,
              }}
            />)
          },
          title: 'Главная',
          tabBarOnPress: ({ navigation, defaultHandler }) => {
            let parentNavigation = navigation.dangerouslyGetParent();
            let prevRoute = parentNavigation.state.routes[parentNavigation.state.index];
            let nextRoute = navigation.state;
            // console.log({ prevRoute, nextRoute });
            defaultHandler();
          }
        }
      }
    },
    ChatTab: {
      screen: createStackNavigator({
        Chat: {
          screen: ChatScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: null,
              headerTransparent: true,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
      }, {
        // headerMode: 'none',
      }),
      navigationOptions: ({navigation}) => {
        return {
          tabBarIcon: ({ tintColor }) => {
            return (<Image
              source={require('./assets/ic-tab2.png')}
              style={{
                resizeMode: 'contain',
                width: 24,
                height: 24,
                // marginTop: 8,
                // tintColor: tintColor,
              }}
            />)
          },
          title: 'Профиль',
            tabBarOnPress: ({ navigation, defaultHandler }) => {
              let parentNavigation = navigation.dangerouslyGetParent();
              let prevRoute = parentNavigation.state.routes[parentNavigation.state.index];
              let nextRoute = navigation.state;
              // console.log({ prevRoute, nextRoute });
              defaultHandler();
            }
        }
      }
    },
    ProfileTab: {
      screen: createStackNavigator({
        Profile2: {
          screen: ProfileScreen,
          headerMode: 'none',
          navigationOptions: ({navigation}) => {
            return {
              gesturesEnabled: true,
              header: (<ProfileNavBar navigation={navigation} />),
              headerTransparent: true,
              headerStyle: {
                height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
              },
            }
          }
        },
      }, {
        // headerMode: 'none',
      }),
      navigationOptions: ({navigation}) => {
        return {
          tabBarIcon: ({ tintColor }) => {
            return (<Image
              source={require('./assets/ic-tab3.png')}
              style={{
                resizeMode: 'contain',
                width: 24,
                height: 24,
                // marginTop: 8,
                // tintColor: tintColor,
              }}
            />)
          },
          title: 'Профиль',
            tabBarOnPress: ({ navigation, defaultHandler }) => {
              let parentNavigation = navigation.dangerouslyGetParent();
              let prevRoute = parentNavigation.state.routes[parentNavigation.state.index];
              let nextRoute = navigation.state;
              // console.log({ prevRoute, nextRoute });
              defaultHandler();
            }
        }
      }
    },
  },
  {
    tabBarComponent: (props) => <CustomTabbar navigation={props.navigation} />,
    lazy: false,
    initialRouteName: 'MainTab',
    tabBarOptions: {
      activeBackgroundColor: 'white',
      inactiveBackgroundColor: 'white',
      activeTintColor: Colors.violetColor,
      inactiveTintColor: Colors.placeholderColor,
      showLabel: true,
      showIcon: true,
      // safeAreaInset: {
      //   bottom: 'never',
      // },
      indicatorStyle: {
        opacity: 0
      },
      style: {
        // paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        backgroundColor: Colors.tabBarColor,
        // height: 100,
      },
      tabStyle: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      },
      labelStyle: {
        marginBottom: Platform.OS === "android" ? 5 : 5,
        fontSize: Common.getLengthByIPhone7(10),
        lineHeight: Common.getLengthByIPhone7(12),
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
      },
    },
    navigationOptions: {
      scrollEnabled: false,
      headerMode: "none",
      mode: 'card',
      // header: {
      //   visible: true,
      // },
      configureTransition: (currentTransitionProps,nextTransitionProps) => ({
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0,
     })
    },
  }
);

// export const SignedIn2 = createStackNavigator({
//   Main: {
//     screen: MainScreen,
//     headerMode: 'none',
//     // header: null,
//     navigationOptions: ({navigation}) => {
//       return {
//         header: (<NavBar navigation={navigation} button={<View style={{flexDirection: 'row'}}><ChatButton navigation={navigation}/><SettingsButton navigation={navigation}/><NotificationButton navigation={navigation}/></View>} />),
//         gesturesEnabled: false,
//         headerTransparent: true,
//         headerStyle: {
//           height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
//         },
//         // headerForceInset: { top: 'never', bottom: 'never' }
//       }
//     }
//   },
//   Profile2: {
//     screen: ProfileScreen,
//     headerMode: 'none',
//     navigationOptions: ({navigation}) => {
//       return {
//         gesturesEnabled: true,
//         header: (<ProfileNavBar navigation={navigation} />),
//         headerTransparent: true,
//         headerStyle: {
//           height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
//         },
//       }
//     }
//   },
//   Notifications: {
//     screen: NotificationsScreen,
//     headerMode: 'none',
//     navigationOptions: ({navigation}) => {
//       return {
//         gesturesEnabled: true,
//         header: (<ItemNavBar navigation={navigation} />),
//         headerTransparent: true,
//         headerStyle: {
//           height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
//         },
//       }
//     }
//   },
//   Chat: {
//     screen: ChatScreen,
//     headerMode: 'none',
//     navigationOptions: ({navigation}) => {
//       return {
//         gesturesEnabled: true,
//         header: (<ItemNavBar navigation={navigation} />),
//         headerTransparent: true,
//         headerStyle: {
//           height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
//         },
//       }
//     }
//   },
//   Item: {
//     screen: ItemScreen,
//     headerMode: 'none',
//     navigationOptions: ({navigation}) => {
//       return {
//         gesturesEnabled: true,
//         header: (<ItemNavBar navigation={navigation} settings={true} />),
//         headerTransparent: true,
//         headerStyle: {
//           height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
//         },
//       }
//     }
//   },
//   CaseSettings: {
//     screen: CaseSettingsScreen,
//     headerMode: 'none',
//     navigationOptions: ({navigation}) => {
//       return {
//         gesturesEnabled: true,
//         header: (<ItemNavBar navigation={navigation} />),
//         headerTransparent: true,
//         headerStyle: {
//           height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
//         },
//       }
//     }
//   },
//   Company: {
//     screen: CompanyScreen,
//     headerMode: 'none',
//     navigationOptions: ({navigation}) => {
//       return {
//         gesturesEnabled: true,
//         header: (<ItemNavBar navigation={navigation} />),
//         headerTransparent: true,
//         headerStyle: {
//           height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
//         },
//       }
//     }
//   },
//   CompanyInfo: {
//     screen: CompanyInfoScreen,
//     headerMode: 'none',
//     navigationOptions: ({navigation}) => {
//       return {
//         gesturesEnabled: true,
//         header: (<InfoNavBar navigation={navigation} />),
//         headerTransparent: false,
//         headerStyle: {
//           height: isIphoneX() ? Common.getLengthByIPhone7(70) : Common.getLengthByIPhone7(60),
//         },
//       }
//     }
//   },
//   Okveds: {
//     screen: OkvedsScreen,
//     headerMode: 'none',
//     navigationOptions: ({navigation}) => {
//       return {
//         gesturesEnabled: true,
//         header: (<InfoNavBar navigation={navigation} />),
//         headerTransparent: false,
//         headerStyle: {
//           height: isIphoneX() ? Common.getLengthByIPhone7(70) : Common.getLengthByIPhone7(60),
//         },
//       }
//     }
//   },
//   Word: {
//     screen: WordScreen,
//     headerMode: 'none',
//     navigationOptions: ({navigation}) => {
//       return {
//         gesturesEnabled: true,
//         header: (<ItemNavBar navigation={navigation} />),
//         headerTransparent: true,
//         headerStyle: {
//           height: isIphoneX() ? Common.getLengthByIPhone7(90) : Common.getLengthByIPhone7(60),
//         },
//       }
//     }
//   },
// });

export const LoginTab = createStackNavigator(
  {
    Login: {
      screen: LoginStack,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null,
        gesturesEnabled: false,
        animationEnabled: false,
      }
    },
  },
  {
    headerMode: "none",
    // mode: "card",
    initialRouteName: 'Login',
  }
);

export const AllScreens = createStackNavigator(
  {
    Splash: {
      screen: SplashScreen,
      headerMode: 'none',
      header: null,
      navigationOptions: ({navigation}) => {
        return {
          gesturesEnabled: false,
        }
      }
    },
    LoginTab: {
      screen: LoginTab,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Tabs: {
      screen: SignedIn,
      navigationOptions: {
        gesturesEnabled: false,
      }
    },
    Scanner: {
      screen: ScannerScreen,
      headerMode: 'none',
      header: null,
      navigationOptions: ({navigation}) => {
        return {
          gesturesEnabled: false,
        }
      }
    },
  },
  {
    headerMode: "none",
    mode: "modal",
    initialRouteName: 'Splash',
  }
);

export const createRootNavigator = () => {

  return createAppContainer(AllScreens);
};
