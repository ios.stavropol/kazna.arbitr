import React from 'react';
import {observable} from 'mobx';
import * as mobx from 'mobx';
import {Platform} from 'react-native';
import Config from './../constants/Config';
import AsyncStorage from '@react-native-async-storage/async-storage';

class Network extends React.Component {

  @observable access_token = '';
  @observable userProfile = null;
  @observable.shallow casesList = [];
  @observable.shallow companiesList = [];
  @observable.shallow keywordsList = [];
  @observable.shallow clipsList = [];
  @observable.shallow caseClipsList = [];
  @observable.shallow calendarList = [];
  @observable.shallow delayList = [];
  @observable storyIndex = 0;
  @observable storyItemIndex = 0;
  @observable storyReady = false;
  @observable order = null;
  
  exitFun = null;
  mainRefresh = null;
  refreshList = null;
  barcode = null;

  storyScroll = React.createRef();
  direction = 1;
  storyItemWidth = [];
  navigation = null;

  constructor(props) {
    super(props);
    mobx.autorun(() => {});
    
  }
}

const network = new Network();
export default network;

export function getCode(phone) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/auth/get-auth-code?phone=' + phone, {
      method: 'get',
      headers: {
        // 'Authorization': 'JWT ' + network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      // console.warn('response: '+JSON.stringify(response));
      let status = response.status;
      response.json().then(data => {
        console.warn('getCode: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function sendCode(phone, code) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/auth/check-auth-code?phone=' + phone + '&code=' + code, {
      method: 'get',
      headers: {
        // 'Authorization': 'JWT ' + network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      // console.warn('response: '+JSON.stringify(response));
      let status = response.status;
      response.json().then(data => {
        console.warn('sendCode: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          AsyncStorage.setItem('token', data.token);
          network.access_token = data.token;
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getProfile() {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/auth/user-detail', {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getProfile: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          network.userProfile = data.data;
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getSubscribtions() {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/get-subscribtions', {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getSubscribtions: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          AsyncStorage.setItem('subscriptions', JSON.stringify(data.data));
          network.casesList = data.data.cases;
          network.companiesList = data.data.companies;
          network.keywordsList = data.data.keywords !== undefined && data.data.keywords !== null ? data.data.keywords : [];
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function newSubscribtion(type, value, sou) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/new-subscribtion', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        type,
        value,
        sou,
      }),
    })
    .then(response => {
      console.warn('newSubscribtion response: ' + JSON.stringify(response));
      let status = response.status;
      response.json().then(data => {
        console.warn('newSubscribtion: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          network.casesList = data.data.cases;
          network.companiesList = data.data.companies;
          // network.keywordsList = data.data.keywords;
          network.keywordsList = data.data.keywords !== undefined && data.data.keywords !== null ? data.data.keywords : [];
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function editProfile(first_name, last_name, email, type) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/auth/edit-profile', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        first_name,
        last_name,
        email,
        type,
      }),
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('editProfile: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          network.userProfile = data.data;
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getStories(display) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/api/stories?display=' + display, {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getStories: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          let main = [];
          let case2 = [];

          // for (let i = 0; i < data.data.length; i++) {
          //   if (data.data[i].display === 'main') {
          //     main.push(data.data[i]);
          //   } else {
          //     case2.push(data.data[i]);
          //   }
          // }

          if (display === 'case') {
            network.caseClipsList = data.data;
          } else {
            network.clipsList = data.data;
          }
          
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getItem() {

  return new Promise(function(resolve, reject) {
    fetch('http://kad.crona.tech/api/v3/case?case=%D0%9033-2414/2021&token=jeffit-aswend-97461', {
      method: 'get',
      headers: {
        // 'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getItem: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          // network.clipsList = data.data;
          resolve(data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getCase(id) {

  return new Promise(function(resolve, reject) {
    console.warn(Config.apiDomain + '/subs/detail-case?id=' + id);
    console.warn('token: ' + network.access_token);
    fetch(Config.apiDomain + '/subs/detail-case?id=' + id, {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getCase: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          if (data.data === null || data.data === undefined) {
            reject(data.message);
          } else {
            resolve(data.data);
          }
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getKeyword(id) {

  return new Promise(function(resolve, reject) {
    console.warn(Config.apiDomain + '/subs/detail-keyword?id=' + id);
    console.warn('token: ' + network.access_token);
    fetch(Config.apiDomain + '/subs/detail-keyword?id=' + id, {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getKeyword: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          if (data.data === null || data.data === undefined) {
            reject(data.message);
          } else {
            resolve(data.data);
          }
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getCompany(id) {

  return new Promise(function(resolve, reject) {
    console.warn(Config.apiDomain + '/subs/detail-company?id=' + id);
    console.warn(network.access_token);
    fetch(Config.apiDomain + '/subs/detail-company?id=' + id, {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getCompany: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function deleteSubscription(id, type) {

  console.warn(Config.apiDomain + '/subs/delete?id=' + id + '&type=' + type);
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/delete?id=' + id + '&type=' + type, {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      console.warn('rrrrr: '+JSON.stringify(response));
      let status = response.status;
      response.json().then(data => {
        console.warn('deleteSubscription: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function setPushId(uid) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/auth/edit-push-uid', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        uid,
      })
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('setPushId: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function renameCase(id, name) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/rename-case', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id,
        name,
      })
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('renameCase: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          if (data.data === null || data.data === undefined) {
            reject(data.message);
          } else {
            resolve(data.data);
          }
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function renameKeyword(id, name) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/rename-keyword', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id,
        name,
      })
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('renameKeyword: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          if (data.data === null || data.data === undefined) {
            reject(data.message);
          } else {
            resolve(data.data);
          }
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function renameCompany(id, name) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/rename-company', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id,
        name,
      })
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('renameCompany: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          if (data.data === null || data.data === undefined) {
            reject(data.message);
          } else {
            resolve(data.data);
          }
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getNotifications() {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/get-notiffications', {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getNotifications: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function searchCompanies(query) {

  return new Promise(function(resolve, reject) {
    fetch('https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party', {
      method: 'post',
      headers: {
        'Authorization': 'Token cec00da4ce8539807a1ac56f2639ad1b873ca509',
        'Content-Type': 'application/json',
        "Accept": "application/json",
      },
      body: JSON.stringify({
        query,
      }),
    })
    .then(response => {
      let status = response.status;
      console.warn('response: '+JSON.stringify(response));
      response.json().then(data => {
        console.warn('searchCompanies: ' + JSON.stringify(data));
        if (status !== 200) {
          reject('Неизвестная ошибка. Повторите снова.');
        } else {
          resolve(data.suggestions);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getCalendar() {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/get-calendar', {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getCalendar: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          network.calendarList = data.data;
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function mutePushCase(id, mute_all) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/mute-case-settings', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id,
        mute_all,
      })
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('mutePushCase: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function muteSidesCase(id, muted_list) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/mute-case-settings', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id,
        muted_list,
      })
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('muteSidesCase: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getMessages() {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/api/messages', {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        // console.warn('getMessages: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function sendMessage(text) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/api/new-message', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        text,
      }),
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('sendMessage: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function makeCaseUnread(id) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/make-case-unread?id=' + id, {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('makeCaseUnread: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function makeCompanyUnread(id) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/make-company-unread?id=' + id, {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('makeCompanyUnread: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve();
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getCategoriesCases() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/get-categories-cases', {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getCategoriesCases: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getCourts() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/get-courts', {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getCourts: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function editKeyword(body) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/edit-keyword', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('editKeyword: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getInstances() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/get-instances', {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getInstances: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function validateReceipt(receipt, store_type, tarif) {
  console.warn(JSON.stringify({
    receipt,
    store_type,
    tarif,
  }));
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/api/validate-receipt', {
      method: 'post',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        receipt,
        store_type,
        tarif,
      }),
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('validateReceipt: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          network.userProfile = data.data;
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function getDelays() {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/get-delays', {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('getDelays: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          network.delayList = data.data;
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}

export function searchDelays(case_number) {

  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain + '/subs/search-delay?case_number=' + case_number, {
      method: 'get',
      headers: {
        'Authorization': network.access_token,
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      let status = response.status;
      response.json().then(data => {
        console.warn('searchDelays: ' + JSON.stringify(data));
        if (status !== 200) {
          reject(data.message);
        } else {
          // network.delayList = data.data;
          resolve(data.data);
        }
      });
    })
    .catch(err => {
        console.warn(err);
        reject('Неизвестная ошибка. Повторите снова.');
    });
  });
}