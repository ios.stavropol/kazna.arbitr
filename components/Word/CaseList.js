import React from 'react';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    Animated,
    Image,
    Alert,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import { SvgXml } from 'react-native-svg';

const months = ['янв', 'фев', 'мар', 'апр', 'мая', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];

export default class CaseList extends React.Component {

  state = {
    data: null,
    showPdf: false,
  };

  UNSAFE_componentWillMount() {
    console.warn(this.props.data);
    this.setState({
        data: this.props.data,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    console.warn(props.data);
    this.setState({
        data: props.data,
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);
  }

  renderView = item => {
    let date = new Date(item.created_at);
    let dd = date.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    return (<TouchableOpacity style={{
      width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
      // height: Common.getLengthByIPhone7(93),
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      // backgroundColor: 'red',
    }}
    onPress={() => {
      // this.props.navigation.navigate('Item', {data: item});
      if (this.props.onOpen) {
        this.props.onOpen(item.link);
      }
    }}>
      <View style={{
        width: Common.getLengthByIPhone7(50),
        height: Common.getLengthByIPhone7(50),
        borderRadius: Common.getLengthByIPhone7(8),
        backgroundColor: '#F6F6F6',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <Text style={{
          color: Colors.textColor,
          fontFamily: 'Montserrat-Bold',
          fontWeight: 'bold',
          textAlign: 'center',
          fontSize: Common.getLengthByIPhone7(13),
          lineHeight: Common.getLengthByIPhone7(18),
          // letterSpacing: -0.55,
        }}>
          {dd}{`\n`}{months[date.getMonth()]}
        </Text>
      </View>
      <View style={{
        width: Common.getLengthByIPhone7(277),
        // height: Common.getLengthByIPhone7(93),
        borderBottomWidth: 1,
        borderBottomColor: '#e8e8e8',
        paddingTop: Common.getLengthByIPhone7(10),
        paddingBottom: Common.getLengthByIPhone7(10),
      }}>
        <View style={{
          width: Common.getLengthByIPhone7(277),
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
          <Text style={{
            color: 'black',
            fontFamily: 'Montserrat-Bold',
            fontWeight: 'bold',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(16),
            lineHeight: Common.getLengthByIPhone7(19),
            // letterSpacing: -0.55,
          }}>
            {item.case}
          </Text>
          <Text style={{
            color: '#bdbdbd',
            fontFamily: 'Montserrat-Regular',
            fontWeight: 'normal',
            textAlign: 'right',
            fontSize: Common.getLengthByIPhone7(9),
            lineHeight: Common.getLengthByIPhone7(11),
            // letterSpacing: -0.55,
          }}>
            {item.court}
          </Text>
        </View>
        <Text style={{
          width: Common.getLengthByIPhone7(277),
          marginTop: 3,
          color: 'black',
          fontFamily: 'Montserrat-Regular',
          fontWeight: 'normal',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(14),
          lineHeight: Common.getLengthByIPhone7(17),
          // letterSpacing: -0.55,
        }}>
            {item.istec} - {item.otvetchik}
          </Text>
      </View>
    </TouchableOpacity>);
}

  render() {

    if (this.state.data === null || this.state.data === undefined) {
        return null;
    }

    let rows = [];

    for (let i=0;i<this.state.data.length;i++) {
      rows.push(this.renderView(this.state.data[i]));
    }

    if (rows.length === 0) {
      let xml = `<svg viewBox="0 0 79 86" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><linearGradient id="linearGradient-1-1" x1="38.8503086%" y1="0%" x2="61.1496914%" y2="100%"><stop stop-color="#FCFCFD" offset="0%"></stop><stop stop-color="#EEEFF3" offset="100%"></stop></linearGradient><linearGradient id="linearGradient-2-1" x1="0%" y1="9.5%" x2="100%" y2="90.5%"><stop stop-color="#FCFCFD" offset="0%"></stop><stop stop-color="#E9EBEF" offset="100%"></stop></linearGradient><rect id="path-3-1" x="0" y="0" width="17" height="36"></rect></defs><g id="Illustrations" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="B-type" transform="translate(-1268.000000, -535.000000)"><g id="Group-2" transform="translate(1268.000000, 535.000000)"><path id="Oval-Copy-2" d="M39.5,86 C61.3152476,86 79,83.9106622 79,81.3333333 C79,78.7560045 57.3152476,78 35.5,78 C13.6847524,78 0,78.7560045 0,81.3333333 C0,83.9106622 17.6847524,86 39.5,86 Z" fill="#F7F8FC"></path><polygon id="Rectangle-Copy-14" fill="#E5E7E9" transform="translate(27.500000, 51.500000) scale(1, -1) translate(-27.500000, -51.500000) " points="13 58 53 58 42 45 2 45"></polygon><g id="Group-Copy" transform="translate(34.500000, 31.500000) scale(-1, 1) rotate(-25.000000) translate(-34.500000, -31.500000) translate(7.000000, 10.000000)"><polygon id="Rectangle-Copy-10" fill="#E5E7E9" transform="translate(11.500000, 5.000000) scale(1, -1) translate(-11.500000, -5.000000) " points="2.84078316e-14 3 18 3 23 7 5 7"></polygon><polygon id="Rectangle-Copy-11" fill="#EDEEF2" points="-3.69149156e-15 7 38 7 38 43 -3.69149156e-15 43"></polygon><rect id="Rectangle-Copy-12" fill="url(#linearGradient-1-1)" transform="translate(46.500000, 25.000000) scale(-1, 1) translate(-46.500000, -25.000000) " x="38" y="7" width="17" height="36"></rect><polygon id="Rectangle-Copy-13" fill="#F8F9FB" transform="translate(39.500000, 3.500000) scale(-1, 1) translate(-39.500000, -3.500000) " points="24 7 41 7 55 -3.63806207e-12 38 -3.63806207e-12"></polygon></g><rect id="Rectangle-Copy-15" fill="url(#linearGradient-2-1)" x="13" y="45" width="40" height="36"></rect><g id="Rectangle-Copy-17" transform="translate(53.000000, 45.000000)"><mask id="mask-4-1" fill="white"><use xlink:href="#path-3-1"></use></mask><use id="Mask" fill="#E0E3E9" transform="translate(8.500000, 18.000000) scale(-1, 1) translate(-8.500000, -18.000000) " xlink:href="#path-3-1"></use><polygon id="Rectangle-Copy" fill="#D5D7DE" mask="url(#mask-4-1)" transform="translate(12.000000, 9.000000) scale(-1, 1) translate(-12.000000, -9.000000) " points="7 0 24 0 20 18 -1.70530257e-13 16"></polygon></g><polygon id="Rectangle-Copy-18" fill="#F8F9FB" transform="translate(66.000000, 51.500000) scale(-1, 1) translate(-66.000000, -51.500000) " points="62 45 79 45 70 58 53 58"></polygon></g></g></g></svg>`;
      rows.push(<View style={{
        alignItems: 'center',
      }}>
        <SvgXml xml={xml} width={Common.getLengthByIPhone7(150)} height={Common.getLengthByIPhone7(150)} />
        <Text style={{
          marginTop: Common.getLengthByIPhone7(10),
          color: Colors.textColor,
          fontFamily: 'Montserrat-Regular',
          fontWeight: 'normal',
          textAlign: 'center',
          fontSize: Common.getLengthByIPhone7(18),
          lineHeight: Common.getLengthByIPhone7(22),
        }}>
          {this.props.status === 'loading' ? 'Идет поиск судебных дел' : 'Судебных дел нет'}{`,\nкак только появятся,\nмы сразу вам сообщим`}
        </Text>
      </View>);
    }

    return (<View style={{
        marginTop: Common.getLengthByIPhone7(26),
        width: Common.getLengthByIPhone7(0),
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}>
        <View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            marginBottom: Common.getLengthByIPhone7(20),
        }}>
            <Image
              source={require('./../../assets/ic-folder.png')}
              style={{
                  width: Common.getLengthByIPhone7(25),
                  height: Common.getLengthByIPhone7(25),
                  resizeMode: 'contain',
              }}/>
            <Text style={{
              marginLeft: Common.getLengthByIPhone7(23),
              color: Colors.textColor,
              fontFamily: 'Montserrat-Bold',
              fontWeight: 'bold',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(18),
              lineHeight: Common.getLengthByIPhone7(22),
            }}>
              Судебные акты
            </Text>
        </View>
        {rows}
      </View>);
  }
}
