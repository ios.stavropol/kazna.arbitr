import React, {Component} from 'react';
import { Platform, View, TouchableOpacity, Text, Alert, StyleSheet, Dimensions, Image, Animated } from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Network from './../Utilites/Network'
import Common from './../Utilites/Common'
import { isIphoneX } from 'react-native-iphone-x-helper';
import { observer } from 'mobx-react';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import DropShadow from "react-native-drop-shadow";

const {width} = Dimensions.get('window');

@observer
class CustomTabbar extends Component {

  state = {
    refresh: false,
  };

  UNSAFE_componentWillMount() {
    Network.refreshTab = this.refreshTab;
  }

  refreshTab = () => {
    this.setState({
      refresh: !this.state.refresh,
    });
  }
  
    render() {
      // console.log('props: '+JSON.stringify(this.props));
      // console.warn('Network.pubData: '+JSON.stringify(Network.pubData));
        let navigation = this.props.navigation;

        let images = [
          {icon: require('./../assets/ic-tab1.png'), width: 22, height: 22},
          {icon: require('./../assets/ic-tab2.png'), width: 24, height: 24},
          {icon: require('./../assets/ic-tab3.png'), width: 21, height: 21},
        ];

        const { routes, index } = navigation.state;
        return (
            <Animated.View style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              width,
              borderWidth: 0,
              borderColor: 'transparent',
              position: 'absolute',
              left: 0,
              bottom: 0,//Network.TabBarBottom,
              // borderTopWidth: 1,
              // borderTopColor: (Network.themeMode == 'light' ? Colors.lightGray : 'rgba(47, 48, 54, 0.3)'),
              backgroundColor: 'transparent',
              // zIndex: 1,
            }}>
              <DropShadow
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                  flex: 1,
                  backgroundColor: 'white',
                  shadowColor: 'rgba(0, 0, 0, 0.1)',
                  shadowOffset: { width: 0, height: -2},
                  shadowOpacity: Platform.OS === 'ios' ? 1 : 0.3,
                  shadowRadius: Platform.OS === 'ios' ? 20 : 0,
                }}
              >
                {routes.map((route, idx) => {
                    const tintColor = (index !== idx) ? '#959595' : 'black';
                    const isActive = index === idx;

                    return (
                      <TouchableOpacity
                          onPress={() => {
                            navigation.navigate(route.routeName);
                          }}
                          style={{
                            flex: 1,
                            backgroundColor: 'white',
                            alignItems: 'center',
                            justifyContent: 'center',
                            // paddingTop: (isIphoneX() ? 10 : 5),
                            height: Platform.OS === 'ios' ? (isIphoneX() ? 86 : 52) : 52,
                            zIndex: 10,
                          }}
                          key={route.routeName}
                      activeOpacity={1}>
                          <View style={{
                            width: Common.getLengthByIPhone7(80),
                            height: Common.getLengthByIPhone7(40),
                            borderRadius: Common.getLengthByIPhone7(10),
                            backgroundColor: isActive ? Colors.mainColor : 'white',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                            <Image
                              source={images[idx].icon}
                              style={{
                                resizeMode: 'contain',
                                // tintColor: color,
                                width: images[idx].width,
                                height: images[idx].height,
                                // marginBottom: -5,
                                tintColor: isActive ? 'white' : '#200E32',
                              }}
                            />
                          </View>
                      </TouchableOpacity>
                    )
                })}
              </DropShadow>
            </Animated.View>
        );
    }
}

export default CustomTabbar;
