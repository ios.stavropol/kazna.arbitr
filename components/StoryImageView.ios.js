// @flow
import React, { Fragment, PureComponent } from "react";
import { View, Image, Text, ActivityIndicator, TouchableOpacity, Linking } from "react-native";
import Common from './../Utilites/Common'
import Network, { getPicture, setView } from './../Utilites/Network'
import Colors from './../constants/Colors'
import styles from "./storyItemStyles";
import { BlurView } from "@react-native-community/blur"
import { observer } from 'mobx-react';

@observer
export default class extends PureComponent {

  state = {
    ready: false,
    viewRef: null,
    image: null,
  }

  UNSAFE_componentDidMount() {
    
  }

  UNSAFE_componentWillMount() {
    Network.storyReady = false;
    if (this.props.data !== null && this.props.data !== undefined) {
      this.setState({
        image: this.props.data.image,
        ready: false,
      }, () => {
        // console.warn('image: ' + this.state.image);
      });
    }
  }

  UNSAFE_componentWillReceiveProps(props) {
    Network.storyReady = false;
    console.warn('props: '+JSON.stringify(props));
    this.setState({
      image: null,
      ready: false,
    }, () => {
      if (props.data !== undefined && props.data !== null) {
        this.setState({
          image: props.data.image,
          ready: false,
        }, () => {
          
        });
      }
    });
  }
  
  render() {
    
    if (this.state.image === null) {
      return (
        <View
          style={{
            flex: 1,
            zIndex: 9999,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "black"
          }}
        >
          <ActivityIndicator size="large" color="white" />
        </View>
      )
    }
    return (
      <View style={{
        flex: 1,
        backgroundColor: "black"
      }}>
        <View style={styles.container}>
          <Image
            onError={() => {
              console.warn('image error');
              if (this.props.onClose) {
                this.props.onClose();
              }
            }}
            key={(new Date()).getTime()}
            onLoad={() => {
              console.warn('onLoad image: ' + this.state.image);
              this.setState({
                ready: true,
              }, () => {
                Network.storyReady = true;
              });
            }}
            style={[styles.image, {
              resizeMode: 'cover',
            }]}
            source={{uri: this.state.image, cache: 'reload', headers: {Pragma: 'no-cache'}}}
          />
          {this.props.data.has_button ? (<TouchableOpacity style={{
            position: 'absolute',
            bottom: Common.getLengthByIPhone7(30),
            // bottom: Common.getLengthByIPhone7(30),
            left: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(300))/2,
            width: Common.getLengthByIPhone7(300),
            minHeight: Common.getLengthByIPhone7(40),
            borderRadius: Common.getLengthByIPhone7(8),
            backgroundColor: Colors.mainColor,
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 9998, 
          }}
          onPress={() => {
            let url = this.props.data.button_link + '?id=' + Network.userProfile.id;
            Linking.canOpenURL(url).then(supported => {
              if (supported) {
                Linking.openURL(url);
              } else {
                console.log("Don't know how to open URI: " + url);
              }
            });
          }}>
            <Text style={{
              color: 'white',
              fontFamily: 'Montserrat-Bold',
              fontWeight: 'bold',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(12),
              lineHeight: Common.getLengthByIPhone7(15),
            }}>
              {this.props.data.button_text}
            </Text>
          </TouchableOpacity>) : null}
        </View>
        {!this.state.ready ? (
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              zIndex: 9999,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "black"
            }}
          >
            <ActivityIndicator size="large" color="white" />
          </View>
        ) : null}
      </View>
    );
  }
}
