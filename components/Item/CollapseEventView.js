import React from 'react';
import {
    Text,
    View,
    Alert,
    TouchableOpacity,
    Linking,
    Animated,
    Image,
    Easing,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';

export default class CollapseEventView extends React.Component {

    maxHeight = new Animated.Value(0);
    opacity = new Animated.Value(0);

  state = {
    data: null,
    hidden: true,
  };

  UNSAFE_componentWillMount() {
    this.setState({
      data: this.props.data,
      hidden: this.props.index === 1 ? false : true,
    }, () => {
        if (this.props.index === 1) {
            Animated.parallel([
                Animated.timing(this.maxHeight, {
                    toValue: 1,
                    duration: 300,
                    easing: Easing.linear,
                    useNativeDriver: false,
                }),
                Animated.timing(this.opacity, {
                    toValue: 1,
                    duration: 300,
                    easing: Easing.linear,
                    useNativeDriver: false,
                })
            ]).start(() => {
                
            });
        }
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      data: props.data,
      hidden: props.index === 1 ? false : true,
    }, () => {
        if (props.index === 1) {
            Animated.parallel([
                Animated.timing(this.maxHeight, {
                    toValue: 1,
                    duration: 300,
                    easing: Easing.linear,
                    useNativeDriver: false,
                }),
                Animated.timing(this.opacity, {
                    toValue: 1,
                    duration: 300,
                    easing: Easing.linear,
                    useNativeDriver: false,
                })
            ]).start(() => {
                
            });
        }
    });
  }

  UNSAFE_componentWillUnmount() {}

  constructor(props) {
    super(props);
  }

  renderItem = data => {
    return (<View style={{
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
        marginLeft: Common.getLengthByIPhone7(30),
        marginBottom: Common.getLengthByIPhone7(14),
    }}>
        <View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
            flexDirection: 'row',
            // alignItems: 'center',
            justifyContent: 'flex-start',
        }}>
            <View style={{
                height: Common.getLengthByIPhone7(15),
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <View style={{
                    height: 1,
                    width: Common.getLengthByIPhone7(11),
                    backgroundColor: '#e7e7e7',
                }}/>
            </View>
            <View style={{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(54),
                flexDirection: 'row',
                marginLeft: Common.getLengthByIPhone7(8),
            }}>
                <Text style={{
                    width: Common.getLengthByIPhone7(250),
                    // marginLeft: Common.getLengthByIPhone7(5),
                    color: Colors.textColor,
                    fontFamily: 'Montserrat-Bold',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(12),
                    lineHeight: Common.getLengthByIPhone7(15),
                }}>
                    {data.DocumentTypeName}
                </Text>
                <Text style={{
                    // marginLeft: Common.getLengthByIPhone7(8),
                    color: Colors.textColor,
                    fontFamily: 'Montserrat-Bold',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(12),
                    lineHeight: Common.getLengthByIPhone7(15),
                }}>
                    {data.DisplayDate}
                </Text>
            </View>
        </View>
        {data.Addressee != null && data.Addressee.length ? (<Text style={{
            marginLeft: Common.getLengthByIPhone7(19),
            width: Common.getLengthByIPhone7(240),
            color: Colors.textColor,
            fontFamily: 'Montserrat-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(16),
            lineHeight: Common.getLengthByIPhone7(20),
        }}>
            {data.Addressee}
        </Text>) : null}
        {data.FileName !== null && data.FileName.length ? (<View style={{
            marginLeft: Common.getLengthByIPhone7(19),
            marginTop: Common.getLengthByIPhone7(8),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingBottom: Common.getLengthByIPhone7(13),
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30) - Common.getLengthByIPhone7(19),
            borderBottomWidth: 1,
            borderBottomColor: '#f5f5f5',
        }}>
            <TouchableOpacity style={{

            }}
            activeOpacity={1}
            onPress={() => {
                // Alert.alert(
                //     Config.appName,
                //     'Вы хотите открыть документ?',
                //     [
                //         {
                //         text: "Нет",
                //         onPress: () => console.log("Cancel Pressed"),
                //         style: "cancel"
                //         },
                //         { text: "Да", onPress: () => {
                            // let url = 'https://kad.arbitr.ru/Document/Pdf/' + data.CaseId + '/' + data.Id + '/' + data.FileName;
                            let url = 'http://kad.crona.tech/api/v3/pdf?caseId=' + data.CaseId + '&eventId=' + data.Id + '&name=' + data.FileName;
                            // console.warn(url);
                            // return;
                            if (this.props.openUrl) {
                                this.props.openUrl(url);
                            }
                            return;
                            Linking.canOpenURL(url).then(supported => {
                                if (supported) {
                                    Linking.openURL(url)
                                    .catch((err) => {
                                        console.warn(err);
                                    });
                                } else {
                                    Alert(Config.appName, 'Данный тип документа невозможно открыть!');
                                    console.log("Don't know how to open URI: " + this.props.url);
                                }
                            });
                //         }}
                //     ]
                // );
            }}>
                <Text style={{
                    width: Common.getLengthByIPhone7(240),
                    color: Colors.mainColor,
                    fontFamily: 'Montserrat-Regular',
                    fontWeight: 'normal',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(16),
                    lineHeight: Common.getLengthByIPhone7(20),
                    textDecorationLine: 'underline',
                }}>
                    {data.ContentTypes}
                </Text>
            </TouchableOpacity>
        </View>) : (<View style={{
            marginLeft: Common.getLengthByIPhone7(19),
            marginTop: Common.getLengthByIPhone7(8),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingBottom: Common.getLengthByIPhone7(13),
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30) - Common.getLengthByIPhone7(19),
            borderBottomWidth: 1,
            borderBottomColor: '#f5f5f5',
        }}>
            <Text style={{
                width: Common.getLengthByIPhone7(240),
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(16),
                lineHeight: Common.getLengthByIPhone7(20),
            }}>
                {data.ContentTypes}
            </Text>
        </View>)}
    </View>);
  }

  render() {
    if (this.state.data === null || this.state.data === undefined) {
      return null;
    }

    let items = [];
    for (let i = 0; i < this.state.data.length; i++) {
        items.push(this.renderItem(this.state.data[i]));
    }
    return (<View style={{
        width: Common.getLengthByIPhone7(0),
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        alignItems: 'center',
        minHeight: Common.getLengthByIPhone7(60),
      }}>
        <View style={{
            width: (this.props.last ? (this.state.hidden ? 0 : 1) : 1),
            position: 'absolute',
            left: Common.getLengthByIPhone7(30),
            top: 0,
            bottom: 0,
            backgroundColor: '#e7e7e7',
        }}/>
        <TouchableOpacity style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            marginBottom: Common.getLengthByIPhone7(21)
        }}
        activeOpacity={1}
        onPress={() => {
            if (JSON.stringify(this.maxHeight) == 0) {
                this.setState({
                    hidden: false,
                }, () => {
                    Animated.parallel([
                        Animated.timing(this.maxHeight, {
                            toValue: 1,
                            duration: 300,
                            easing: Easing.linear,
                            useNativeDriver: false,
                        }),
                        Animated.timing(this.opacity, {
                            toValue: 1,
                            duration: 300,
                            easing: Easing.linear,
                            useNativeDriver: false,
                        })
                    ]).start(() => {
                        
                    });
                });
            } else {
                this.setState({
                    hidden: true,
                }, () => {
                    Animated.parallel([
                        Animated.timing(this.maxHeight, {
                            toValue: 0,
                            duration: 300,
                            easing: Easing.linear,
                            useNativeDriver: false,
                        }),
                        Animated.timing(this.opacity, {
                            toValue: 0,
                            duration: 200,
                            easing: Easing.linear,
                            useNativeDriver: false,
                        })
                    ]).start(() => {
                        
                    });
                });
            }
        }}>
            <View style={{
                width: Common.getLengthByIPhone7(20),
                height: Common.getLengthByIPhone7(20),
                borderRadius: Common.getLengthByIPhone7(2),
                backgroundColor: '#e7e7e7',
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                {this.state.hidden ? (<Image
                    source={require('./../../assets/ic-plus2.png')}
                    style={{
                        width: Common.getLengthByIPhone7(20),
                        height: Common.getLengthByIPhone7(20),
                        resizeMode: 'contain',
                    }}
                />) : (<Image
                    source={require('./../../assets/ic-minus2.png')}
                    style={{
                        width: Common.getLengthByIPhone7(20),
                        height: Common.getLengthByIPhone7(20),
                        resizeMode: 'contain',
                    }}
                />)}
            </View>
            <Text style={{
              marginLeft: Common.getLengthByIPhone7(9),
              color: Colors.textColor,
              fontFamily: 'Montserrat-Regular',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(18),
              lineHeight: Common.getLengthByIPhone7(22),
            }}>
              {this.props.name}
            </Text>
        </TouchableOpacity>
        <Animated.View style={{
            opacity: this.opacity,
            maxHeight: this.maxHeight.interpolate({ 
                inputRange: [0, 1], 
                outputRange: [0, 5000]
            })
        }}>
            {items}
        </Animated.View>
      </View>);
  }
}
