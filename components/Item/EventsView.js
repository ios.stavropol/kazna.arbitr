import React from 'react';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    Animated,
    Image,
    Platform,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import CollapseEventView from './CollapseEventView';

export default class EventsView extends React.Component {

  state = {
    data: null,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        data: this.props.data,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
        data: props.data,
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);
  }

  render() {

    if (this.state.data === null || this.state.data === undefined) {
        return null;
    }

    let items = [];

    for (let i=0;i<this.state.data.length;i++) {
        console.warn('item-'+i+': '+JSON.stringify(this.state.data[i]));
        items.push(<CollapseEventView
            key={i}
            index={i + 1}
            name={this.state.data[i]['instance-name']}
            data={this.state.data[i].data.Result.Items}
            last={i + 1 === this.state.data.length ? true : false}
            openUrl={url => {
              if (this.props.openUrl) {
                this.props.openUrl(url);
              }
            }}
        />);
    }
    return (<View style={{
        marginTop: Common.getLengthByIPhone7(26),
        width: Common.getLengthByIPhone7(0),
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}>
        <View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            marginBottom: Common.getLengthByIPhone7(20),
        }}>
            <Image
              source={require('./../../assets/ic-calendar.png')}
              style={{
                  width: Common.getLengthByIPhone7(25),
                  height: Common.getLengthByIPhone7(25),
                  resizeMode: 'contain',
              }}/>
            <Text style={{
              marginLeft: Common.getLengthByIPhone7(23),
              color: Colors.textColor,
              fontFamily: 'Montserrat-Bold',
              fontWeight: 'bold',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(18),
              lineHeight: Common.getLengthByIPhone7(22),
            }}>
              События
            </Text>
        </View>
        {items}
      </View>);
  }
}
