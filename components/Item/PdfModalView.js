import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import Modal from 'react-native-modal';
import Pdf from 'react-native-pdf';
import { WebView } from 'react-native-webview';

@observer
export default class PdfModalView extends React.Component {

  state = {
    show: false,
    url: '',
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
        url: this.props.url,
      });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
      console.warn('props.url: '+props.url);
    this.setState({
      show: props.show,
      url: props.url,
    });
  }

  constructor(props) {
    super(props);

  }

  render() {

    return (<View style={{
        // flex: 1,
      }}>
        <Modal isVisible={this.state.show}>
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
                <View style={{
                    width: Common.getLengthByIPhone7(0),
                    backgroundColor: 'white',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    marginBottom: -20,
                    marginTop: -20,
                    flex: 1,
                }}>
                    <View style={{
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                        height: Common.getLengthByIPhone7(70),
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                    }}>
                        <TouchableOpacity style={{
                            marginTop: Common.getLengthByIPhone7(15),
                            width: Common.getLengthByIPhone7(30),
                            height: Common.getLengthByIPhone7(30),
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                        onPress={() => {
                            if (this.props.onClose) {
                                this.props.onClose();
                            }
                        }}>
                            <Image
                                source={require('./../../assets/ic-close.png')}
                                style={{
                                    width: Common.getLengthByIPhone7(12),
                                    height: Common.getLengthByIPhone7(12),
                                    resizeMode: 'contain',
                                }}
                            />
                        </TouchableOpacity>
                    </View>
                    <WebView
                        source={{ uri: this.state.url}}
                        style={{
                            width: Common.getLengthByIPhone7(0),
                            flex: 1,
                        }}
                        onError={(syntheticEvent) => {
                            const { nativeEvent } = syntheticEvent;
                            console.warn('WebView error: ', nativeEvent);
                        }}
                        onLoad={(syntheticEvent) => {
                            const { nativeEvent } = syntheticEvent;
                            // this.url = nativeEvent.url;
                            console.warn('onLoad: ', nativeEvent.url);
                        }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        decelerationRate="normal"
                        startInLoadingState={true}
                        // onNavigationStateChange={navState => {
                        //     var wb_url=navState.url;
                        //     var lastPart = wb_url.substr(wb_url.lastIndexOf('.') + 1);
                        //     if (lastPart === "pdf") {        
                        //         var DEFAULT_URL = 'http://docs.google.com/gview?embedded=true&url='+wb_url;
                        //         this.setState({url:DEFAULT_URL})
                        //     } 
                        // }}
                    />
                </View>
            </View>
        </Modal>
        <Spinner
          visible={this.state.loading}
          textContent={'Загрузка...'}
          overlayColor={'rgba(32, 42, 91, 0.3)'}
          textStyle={{color: '#FFF'}}
        />
      </View>);

// http://docs.google.com/gview?embedded=true&url=https://kad.arbitr.ru/Document/Pdf/089c9ab7-dc10-45c4-ad6a-cfc20788c697/a6182634-acf0-4a42-b0bd-bde36c090a2f/A33-2414-2021_20210428_Opredelenie.pdf

{/* <Pdf
source={{uri: this.state.url}}
onLoadComplete={(numberOfPages,filePath)=>{
    console.log(`number of pages: ${numberOfPages}`);
}}
onPageChanged={(page,numberOfPages)=>{
    console.log(`current page: ${page}`);
}}
onError={(error)=>{
    console.log(error);
}}
onPressLink={(uri)=>{
    console.log(`Link presse: ${uri}`)
}}
style={{
    flex: 1,
}}
/> */}

{/* <Text style={{
    // marginTop: Common.getLengthByIPhone7(24),
    color: Colors.textColor,
    fontSize: Common.getLengthByIPhone7(22),
    lineHeight: Common.getLengthByIPhone7(27),
    letterSpacing: -0.55,
    fontFamily: 'Montserrat-Regular',
    textAlign: 'center',
}}
allowFontScaling={false}>
    Добавление нового дела
</Text> */}
  }
}
