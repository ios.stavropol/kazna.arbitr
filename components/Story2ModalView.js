import React from 'react';
import { Modal, Text, Image, View, Dimensions, TouchableOpacity, FlatList, Alert, Animated, Easing } from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config'
import Common from './../Utilites/Common'
import Network from './../Utilites/Network'
import { observer } from 'mobx-react';
import Spinner from 'react-native-loading-spinner-overlay'
import { Header } from 'react-navigation';
// import Modal from "react-native-modalbox";
import Stories from './StoriesView'
import {SafeAreaView} from 'react-navigation'
import { isIphoneX } from 'react-native-iphone-x-helper'

@observer
export default class Story2ModalView extends React.Component {

  state = {
    show: false,
    index: 0,
  };

  UNSAFE_componentWillMount() {
    this.setState({
      show: this.props.show,
      index: this.props.index,
    });
  }

  UNSAFE_componentDidMount() {
    // StatusBar.setHidden(true);
  }

  UNSAFE_componentWillUnmount() {
    // StatusBar.setHidden(false);
  }

  constructor(props) {
    super(props);
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      show: props.show,
      index: props.index,
    });
  }

  render() {

    // console.warn('stories: '+JSON.stringify(this.props.stories));
    if(isIphoneX()) {
      return (<Modal
        style={{
          height: Dimensions.get("window").height,
          width: Dimensions.get("window").width,
          flex: 1,
          backgroundColor: 'black',
        }}
        isOpen={this.state.show}
        onClosed={() => {
          if (this.props.onClose) {
            this.props.onClose();
          }
        }}
        position="center"
        swipeToClose
        coverScreen
        swipeArea={250}
        backButtonClose
      >
        <SafeAreaView style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
          justifyContent: 'flex-end',
          alignItems: 'center',
          backgroundColor: 'black',
        }}>
          <Stories
            navigation={this.props.navigation}
            selectedStory={this.state.index}
            stories={this.props.stories}
            display={this.props.display}
            onClose={() => {
              console.warn('storyModal onClose={() => {');
              if (this.props.onClose) {
                this.props.onClose();
              }
            }}
          />
          <TouchableOpacity style={{
            position: 'absolute',
            top: Common.getLengthByIPhone7(65),
            left: Common.getLengthByIPhone7(20),
            zIndex: 10000,
            // backgroundColor: 'red',
          }}
          onPress={() => {
            if (this.props.onClose) {
              this.props.onClose();
            }
          }}>
            <View style={{
              width: Common.getLengthByIPhone7(30),
              height: Common.getLengthByIPhone7(30),
              borderRadius: Common.getLengthByIPhone7(30)/2,
              backgroundColor: 'rgba(0, 0, 0, 0.2)',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <Image source={require('./../assets/ic-close.png')} style={{
                resizeMode: 'contain',
                width: Common.getLengthByIPhone7(24),
                height: Common.getLengthByIPhone7(24),
                tintColor: 'white',
              }} />
            </View>
          </TouchableOpacity>
        </SafeAreaView>
      </Modal>);
    } else {
      return (<Modal
        style={{
          // height: Dimensions.get("window").height,
          // marginTop: -20,
          width: Dimensions.get("window").width,
          flex: 1,
          backgroundColor: 'black',
        }}
        isOpen={this.state.show}
        onClosed={() => {
          if (this.props.onClose) {
            this.props.onClose();
          }
        }}
        position="center"
        swipeToClose
        coverScreen
        swipeArea={250}
        backButtonClose
      >
        <Stories
          navigation={this.props.navigation}
          selectedStory={this.state.index}
          stories={this.props.stories}
          display={this.props.display}
          onClose={() => {
            console.warn('storyModal onClose={() => {');
            if (this.props.onClose) {
              this.props.onClose();
            }
          }}
        />
        <TouchableOpacity style={{
          position: 'absolute',
          top: Common.getLengthByIPhone7(40),
          left: Common.getLengthByIPhone7(30),
        }}
        onPress={() => {
          if (this.props.onClose) {
            this.props.onClose();
          }
        }}>
          <View style={{
            width: Common.getLengthByIPhone7(30),
            height: Common.getLengthByIPhone7(30),
            borderRadius: Common.getLengthByIPhone7(30)/2,
            backgroundColor: 'rgba(0, 0, 0, 0.2)',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
            <Image source={require('./../assets/ic-close.png')} style={{
              resizeMode: 'contain',
              width: Common.getLengthByIPhone7(24),
              height: Common.getLengthByIPhone7(24),
              tintColor: 'white',
            }} />
          </View>
        </TouchableOpacity>
      </Modal>);
    }
  }
}
