// @flow
import React, { Fragment, PureComponent } from "react";
import { View, Image, Text, ActivityIndicator, Linking, TouchableOpacity, findNodeHandle } from "react-native";
import Common from './../Utilites/Common'
import Network, { getPicture } from './../Utilites/Network'
import Colors from './../constants/Colors'
import styles from "./storyItemStyles";
import { BlurView } from "@react-native-community/blur"
import { observer } from 'mobx-react';

@observer
export default class extends PureComponent {

  state = {
    ready: false,
    viewRef: null,
    data: null,
  }

  UNSAFE_componentDidMount() {
    
  }

  UNSAFE_componentWillMount() {
    this.setState({ 
      data: this.props.data,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      data: props.data,
    });
  }

  imageLoaded() {
    // Workaround for a tricky race condition on initial load.
    console.warn('imageLoaded');
    // InteractionManager.runAfterInteractions(() => {
      // setTimeout(() => {
        this.setState({
          viewRef: findNodeHandle(this.backgroundImage),
          ready: true,
        }, () => {
          console.warn('viewRef: '+this.state.viewRef);
        });
      // }, 500);
    // });
  }


  render() {

    // console.warn('this.state.data: '+JSON.stringify(this.state.data));

    if (this.state.data === null || this.state.data === undefined) {
      return null;
    }
    
    return (
      <View style={{
        flex: 1,
        backgroundColor: "black"
      }}>
        <View style={styles.container}>
          <Image
            onLoad={() => {
              this.setState({
                ready: true,
              }, () => {
                Network.storyReady = true;
              });
            }}
            ref={img => {
              this.backgroundImage = img;
            }}
            onLoadEnd={this.imageLoaded.bind(this)}
            style={[styles.image, {
              resizeMode: 'cover',
              // backgroundColor: 'black',//'rgba(255, 255, 255, 0.4)',
            }]}
            source={{uri: this.state.data.image}}
          />
          {this.state.data.has_button ? (<TouchableOpacity style={{
            position: 'absolute',
            bottom: Common.getLengthByIPhone7(30),
            left: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(300))/2,
            width: Common.getLengthByIPhone7(300),
            minHeight: Common.getLengthByIPhone7(40),
            borderRadius: Common.getLengthByIPhone7(8),
            backgroundColor: Colors.mainColor,
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 9998,
          }}
          onPress={() => {
            let url = this.state.data.button_link + '?id=' + Network.userProfile.id;
            Linking.canOpenURL(url).then(supported => {
              if (supported) {
                Linking.openURL(url);
              } else {
                console.log("Don't know how to open URI: " + url);
              }
            });
          }}>
            <Text style={{
              color: 'white',
              fontFamily: 'Montserrat-Bold',
              fontWeight: 'bold',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(12),
              lineHeight: Common.getLengthByIPhone7(15),
            }}>
              {this.state.data.button_text}
            </Text>
          </TouchableOpacity>) : null}
        </View>
        {!this.state.ready && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              zIndex: 9999,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "black"
            }}
          >
            <ActivityIndicator size="large" color="white" />
          </View>
        )}
      </View>
    );
  }
}
