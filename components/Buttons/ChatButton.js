import React from "react";
import { View, Image, Dimensions, TouchableOpacity, Alert, AsyncStorage } from "react-native";
import { NavigationActions } from "react-navigation";
import Common from './../../Utilites/Common'
import Network from './../../Utilites/Network'
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import { Header } from 'react-navigation-stack'

export default class ChatButton extends React.Component {
    goToSettings = () => {
      this.props.navigation.navigate('Chat');
    };

    render() {

      return (
        <TouchableOpacity onPress={this.goToSettings}>
        <View style={{
            width: Common.getLengthByIPhone7(45),
            height: Common.getLengthByIPhone7(45),
            alignItems: 'flex-start',
            justifyContent: 'center',
        }}>
            <Image source={require('./../../assets/ic-chat.png')} style={{
            resizeMode: 'contain',
            marginTop: Common.getLengthByIPhone7(10),
            width: Common.getLengthByIPhone7(23),
            height: Common.getLengthByIPhone7(23),
            }} />
        </View>
        </TouchableOpacity>
      )
    }
}
