import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Common from './../../Utilites/Common';

export default class AnimatedButton extends React.Component {

  state = {
    backgroundColor: Colors.violetColor,
    textColor: 'white',
  };

  UNSAFE_componentWillMount() {
    this.setState({
      backgroundColor: this.props.activeColor,
      textColor: this.props.inactiveColor,
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      backgroundColor: props.activeColor,
      textColor: props.inactiveColor,
    });
  }

  constructor(props) {
    super(props);

  }

  render() {

    let rightPart = (<Image
      source={require('./../../assets/ic-arrow-right.png')}
      style={{
        width: Common.getLengthByIPhone7(14),
        height: Common.getLengthByIPhone7(14),
        resizeMode: 'contain',
        tintColor: this.state.textColor,
    }}/>);

    let leftPart = (<Text style={[{
      color: this.state.textColor,
      fontFamily: 'Rubik',
      fontWeight: '500',
      textAlign: 'center',
      fontSize: Common.getLengthByIPhone7(16),
      marginRight: Common.getLengthByIPhone7(10)
    }, this.props.textStyle]}
    allowFontScaling={false}>
       {this.props.title}
    </Text>);

    if (this.props.direct == 'left') {
      rightPart = (<Text style={[{
        color: this.state.textColor,
        fontFamily: 'Rubik',
        fontWeight: '500',
        textAlign: 'center',
        fontSize: Common.getLengthByIPhone7(16),
      }, this.props.textStyle]}
      allowFontScaling={false}>
         {this.props.title}
      </Text>);
      leftPart = (<Image
        source={require('./../../assets/ic-arrow-left.png')}
        style={{
          width: Common.getLengthByIPhone7(14),
          height: Common.getLengthByIPhone7(14),
          resizeMode: 'contain',
          tintColor: this.state.textColor,
          marginRight: Common.getLengthByIPhone7(10)
      }}/>);
    }

    if (this.props.direct == null) {
      leftPart = (<Text style={[{
        color: this.state.textColor,
        fontFamily: 'Montserrat-Regular',
        fontWeight: 'normal',
        textAlign: 'center',
        fontSize: Common.getLengthByIPhone7(18),
      }, this.props.textStyle]}
      allowFontScaling={false}>
         {this.props.title}
      </Text>);
      rightPart = null;
    }
    return (<TouchableOpacity style={[{
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: Common.getLengthByIPhone7(15),
      borderWidth: 1,
      borderColor: (this.props.borderColor ? this.props.borderColor : Colors.violetColor),
      backgroundColor: this.state.backgroundColor,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      shadowColor: '#CED6E8',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 1,
      shadowRadius: Common.getLengthByIPhone7(10),
      elevation: 4,
    }, this.props.style]}
    activeOpacity={1}
    onLayout={e => {
      if (this.props.onLayout) {
        this.props.onLayout(e);
      }
    }}
    onPress={() => {
      if (this.props.onPress) {
        this.props.onPress();
      }
    }}
    onPressIn={() => {
      this.setState({
        backgroundColor: this.props.inactiveColor,
        textColor: this.props.activeColor,
      });
    }}
    onPressOut={() => {
      this.setState({
        backgroundColor: this.props.activeColor,
        textColor: this.props.inactiveColor,
      });
    }}>
      {leftPart}
      {rightPart}
    </TouchableOpacity>);
  }
}
