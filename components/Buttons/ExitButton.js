import React from "react";
import { View, Image, Dimensions, TouchableWithoutFeedback, Alert, AsyncStorage } from "react-native";
import { NavigationActions } from "react-navigation";
import Common from './../../Utilites/Common'
import Network from './../../Utilites/Network'
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import { Header } from 'react-navigation-stack'

export default class ExitButton extends React.Component {
    goToSettings = () => {
      Alert.alert(
        Config.appName,
        "Вы хотите выйти?",
        [
          {
            text: "Нет",
            onPress: () => {

            },
            style: "cancel"
          },
          { text: "Да", onPress: () => {
            AsyncStorage.setItem('token', '');
            Network.access_token = '';
            Network.userProfile = null;
            this.props.navigation.navigate('LoginTab')
          }}
        ],
        { cancelable: false }
      );
    };

    render() {

      let scale = Header.HEIGHT/88;
      scale = 1;
      return (
          <View style={{
            backgroundColor: 'transparent',
            flex: 0,
          }}>
              <TouchableWithoutFeedback onPress={this.goToSettings}>
                <View style={{
                  width: Common.getLengthByIPhone7(45),
                  height: Common.getLengthByIPhone7(45),
                  alignItems: 'flex-start',
                  justifyContent: 'center',
                }}>
                  <Image source={require('./../../assets/ic-logout.png')} style={{
                    resizeMode: 'contain',
                    width: Common.getLengthByIPhone7(30)*scale,
                    height: Common.getLengthByIPhone7(30)*scale,
                    tintColor: 'white',
                  }} />
                </View>
              </TouchableWithoutFeedback>
          </View>
      )
    }
}
