import React, { PureComponent } from "react";
import {
  StatusBar,
  StyleSheet,
  Animated,
  Dimensions,
  Platform,
  View,
  ScrollView
} from "react-native";
import styles from "./storiesStyles";
import Common from './../Utilites/Common'
import Network from './../Utilites/Network'
import StoryItem from './storyItemView';

const { width } = Dimensions.get("window");
const perspective = width;
const angle = Math.atan(perspective / (width / 2));
const ratio = Platform.OS === "ios" ? 2 : 1.2;

export default class Stories extends PureComponent {

  stories = [];
  scroll = null;

  state = {
    x: new Animated.Value(0),
    ready: false,
    reload: false,
  };

  constructor(props) {
    super(props);
    Network.storyScroll = React.createRef();
    this.stories = props.stories.map(() => React.createRef());
  }

  UNSAFE_componentWillUnmount() {
    StatusBar.setHidden(false);
  }

  async UNSAFE_componentWillMount() {
    // StatusBar.setHidden(true);
    Network.storyScroll = React.createRef();
    this.stories = this.props.stories.map(() => React.createRef());
    const { x } = this.state;
    await x.addListener(() => 
      this.stories.forEach((story, index) => {
        const offset = index * width;
        const inputRange = [offset - width, offset + width];
        const translateX = x
          .interpolate({
            inputRange,
            outputRange: [width / ratio, -width / ratio],
            extrapolate: "clamp"
          })
          .__getValue();

        const rotateY = x
          .interpolate({
            inputRange,
            outputRange: [`${angle}rad`, `-${angle}rad`],
            extrapolate: "clamp"
          })
          .__getValue();

        const parsed = parseFloat(
          rotateY.substr(0, rotateY.indexOf("rad")),
          10
        );
        const alpha = Math.abs(parsed);
        const gamma = angle - alpha;
        const beta = Math.PI - alpha - gamma;
        const w = width / 2 - ((width / 2) * Math.sin(gamma)) / Math.sin(beta);
        const translateX2 = parsed > 0 ? w : -w;

        const style = {
          transform: [
            { perspective },
            { translateX },
            { rotateY },
            { translateX: translateX2 }
          ]
        };
        if(story != null && story.current != null) {
          story.current.setNativeProps({ style });
        }
      })
    );
  }

  render() {

    const { x, ready } = this.state;
    const { stories, selectedStory } = this.props;

    return (
      <View style={styles.container}>
        {stories
          .map((story, i) => {
            return (
            <Animated.View
              ref={this.stories[i]}
              style={[StyleSheet.absoluteFill, {
                backgroundColor: 'black',
              }]}
              key={i}
            >
              <StoryItem
                navigation={this.props.navigation}
                selectedStory={selectedStory}
                index={i}
                display={this.props.display}
                onClose={() => {
                  if (this.props.onClose) {
                    console.warn('storiesView onClose');
                    this.props.onClose();
                  }
                }}
                {...{story}}
              />
            </Animated.View>
          )})
          .reverse()}
        <Animated.ScrollView
          ref={el => (Network.storyScroll = el)}
          style={{
            width: Common.getLengthByIPhone7(0),
            height: Dimensions.get('window').height - Common.getLengthByIPhone7(180),
            marginTop: Common.getLengthByIPhone7(90),
            marginBottom: Common.getLengthByIPhone7(90),
            backgroundColor: 'transparent',
            // backgroundColor: 'red'
          }}
          showsHorizontalScrollIndicator={true}
          scrollEventThrottle={16}
          snapToInterval={width}
          contentContainerStyle={{
            width: width * stories.length,
          }}
          onLayout={() => {
            Network.storyScroll.getNode().scrollTo({x: Network.storyIndex*width, y: 0, animated: false});
          }}
          onTouchStart={() => {

          }}
          onTouchMove={event => {
            // console.warn('event.nativeEvent: '+event.nativeEvent);
            // Animated.timing(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex]).stop(value => {
            //   console.warn('value: '+value);
            // });
          }}
          onTouchEnd={event => {
            // console.warn('onTouchEnd');
            let pageX = event.nativeEvent.pageX;
            if(pageX < Common.getLengthByIPhone7(0)/2) {
              Network.direction = 0;
              // console.warn('json: '+JSON.stringify(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex]));
              if(JSON.stringify(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex]) == (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20) - Common.getLengthByIPhone7(5)*((this.props.display === 'case' ? Network.caseClipsList[Network.storyIndex].clips.length : Network.clipsList[Network.storyIndex].clips.length) - 1))/(this.props.display === 'case' ? Network.caseClipsList[Network.storyIndex].clips.length : Network.clipsList[Network.storyIndex].clips.length)) {
                if(Network.storyIndex === 0) {
                  console.warn('left: 0');
                  if(Network.storyItemIndex > 0) {
                    Animated.timing(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex], {
                      toValue: 0,
                      duration: 0,
                    }).start(() => {
                      Animated.timing(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex - 1], {
                        toValue: 0,
                        duration: 0,
                      }).start(() => {
                        Network.storyItemIndex--;
                      });
                    });
                  } else {
                    if (this.props.onClose) {
                      this.props.onClose();
                    }
                  }
                } else {
                  console.warn('left: 1');
                  if(Network.storyItemIndex > 0) {
                    Animated.timing(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex], {
                      toValue: 0,
                      duration: 0,
                    }).start(() => {
                      Animated.timing(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex - 1], {
                        toValue: 0,
                        duration: 0,
                      }).start(() => {
                        Network.storyItemIndex--;
                      });
                    });
                  } else {

                  }
                }
              } else {
                if (Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex] !== undefined) {
                  Animated.timing(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex]).stop();
                }
              }
            } else {
              Network.direction = 1;
              // console.warn('dddd: '+JSON.stringify(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex]));
              if (Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex] !== undefined) {
                Animated.timing(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex]).stop();
              }
            }
          }}
          // onMomentumScrollEnd={event => {
          //   console.warn('onMomentumScrollEnd');
          //   this.setState({
          //     reload: !this.state.reload,
          //   });
          // }}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {x}
                }
              }
            ],
            { useNativeDriver: true }
          )}
          decelerationRate={0.99}
          horizontal
        />
      </View>
    );
  }
}
