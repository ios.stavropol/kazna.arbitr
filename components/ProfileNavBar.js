import React from 'react';
import {
  Alert,
  View,
  Image,
  Platform
} from 'react-native';
import Colors from './../constants/Colors';
import Common from './../Utilites/Common';
import { observer } from 'mobx-react';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Config from '../constants/Config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Network from '../Utilites/Network';

@observer
export default class ProfileNavBar extends React.Component {

  state = {
    
  };

  UNSAFE_componentWillMount() {
    
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  render() {

    return (<View style={{
        flex: 1,
        height: isIphoneX() ? Common.getLengthByIPhone7(88) : (Platform.OS === 'ios' ? Common.getLengthByIPhone7(68) + 20 : Common.getLengthByIPhone7(68)),
        // height: Dimensions.get('window').height,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        backgroundColor: 'white',
        paddingBottom: isIphoneX() ? Common.getLengthByIPhone7(20) : (Platform.OS === 'ios' ? Common.getLengthByIPhone7(30) : Common.getLengthByIPhone7(30)),
      }}>
        <TouchableOpacity style={{
            marginRight: Common.getLengthByIPhone7(16),
        }}
        onPress={() => {
            Alert.alert(
                Config.appName,
                "Вы хотите выйти?",
                [
                    {
                    text: "Нет",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                    },
                    { text: "Да", onPress: () => {
                        AsyncStorage.setItem('token', '');
                        Network.access_token = '';
                        this.props.navigation.navigate('Login');
                    }}
                ]
            );

        }}>
            <Image
            source={require('./../../assets/ic-logout.png')}
            style={{
                width: Common.getLengthByIPhone7(25),
                height: Common.getLengthByIPhone7(25),
                resizeMode: 'contain',
            }}/>
        </TouchableOpacity>
      </View>);
  }
}
