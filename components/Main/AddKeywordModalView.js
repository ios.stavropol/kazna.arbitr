import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  TouchableOpacity,
  Linking,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native';
import Colors from '../../constants/Colors';
import Config from '../../constants/Config';
import Common from '../../Utilites/Common';
import Network, {newSubscribtion} from '../../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import Modal from 'react-native-modal';
import AnimatedButton from '../components/Buttons/AnimatedButton';
import AnimatedTextInputView from '../AnimatedTextInputView';

@observer
export default class AddKeywordModalView extends React.Component {

  state = {
    refresh: false,
    show: false,
    text: '',
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
        text: '',
      });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      show: props.show,
      text: '',
    });
  }

  constructor(props) {
    super(props);

  }

  render() {

    return (<View style={{
        // flex: 1,
      }}>
        <Modal isVisible={this.state.show}>
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "padding" : "height"}
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                    }}
                >
                <TouchableOpacity style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'transparent',
                }}
                onPress={() => {
                    this.setState({
                        text: '',
                    }, () => {
                        
                    });
                    if (this.props.onClose) {
                        this.props.onClose();
                    }
                }}
                />
                <View style={{
                    width: Common.getLengthByIPhone7(0),
                    backgroundColor: 'white',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    marginBottom: -20,
                }}>
                    <View style={{
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                        alignItems: 'flex-start',
                    }}>
                        <View style={{
                            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginTop: Common.getLengthByIPhone7(40),
                        }}>
                            <TouchableOpacity style={{
                                width: Common.getLengthByIPhone7(30),
                                height: Common.getLengthByIPhone7(30),
                                alignItems: 'flex-start',
                                justifyContent: 'flex-start',
                            }}
                            onPress={() => {
                                this.setState({
                                    text: '',
                                }, () => {
                                    
                                });
                                if (this.props.onClose) {
                                    this.props.onClose();
                                }
                            }}>
                                <Image
                                    source={require('./../../assets/ic-close.png')}
                                    style={{
                                        width: Common.getLengthByIPhone7(12),
                                        height: Common.getLengthByIPhone7(12),
                                        resizeMode: 'contain',
                                    }}
                                />
                            </TouchableOpacity>
                        </View>
                        <Text style={{
                            marginTop: Common.getLengthByIPhone7(24),
                            color: Colors.textColor,
                            fontSize: Common.getLengthByIPhone7(22),
                            lineHeight: Common.getLengthByIPhone7(27),
                            letterSpacing: -0.55,
                            fontFamily: 'Montserrat-Regular',
                            textAlign: 'center',
                        }}
                        allowFontScaling={false}>
                            Добавление новой фразы
                        </Text>
                        <AnimatedTextInputView
                            style={{
                                marginTop: Common.getLengthByIPhone7(42),
                                marginBottom: Common.getLengthByIPhone7(5),
                            }}
                            value={this.state.text}
                            title={'Введите фразу'}
                            autoCapitalize={'words'}
                            autoCorrect={false}
                            // keyboardType={this.state.index === 'company' ? 'number-pad' : 'default'}
                            onChangeText={text => {
                                this.setState({
                                    text,
                                });
                            }}
                        />
                    </View>
                    <AnimatedButton style={{
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                        height: Common.getLengthByIPhone7(35),
                        borderRadius: Common.getLengthByIPhone7(4),
                        marginTop: Common.getLengthByIPhone7(120),
                        marginBottom: Common.getLengthByIPhone7(44),
                        backgroundColor: '#F6895E',
                    }}
                    textStyle={{
                        fontSize: Common.getLengthByIPhone7(16),
                    }}
                    onLayout={this.onLayout}
                    onPress={() => {
                        if (this.state.text.length) {
                            if (this.props.onAdd) {
                                this.props.onAdd(this.state.text);
                            }
                        } else {
                            Alert.alert(Config.appName, 'Заполните поле!');
                        }
                    }}
                    title={'Загрузить фразу'}
                    borderColor={'#F6895E'}
                    activeColor={'#F6895E'}
                    inactiveColor={'white'}
                    />
                </View>
                </KeyboardAvoidingView>
            </View>
        </Modal>
      </View>);
  }
}
