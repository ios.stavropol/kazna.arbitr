import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import SwipeableItem from 'react-native-swipeable-item';
import Animated from 'react-native-reanimated';

export default class ItemView extends React.Component {

    itemRef = null;

  state = {
    index: 0,
    index2: 0,
    data: null,
    isActive: false,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        index: this.props.index,
        index2: this.props.index2,
        data: this.props.data,
        isActive: this.props.isActive,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
        index: props.index,
        index2: props.index2,
        data: props.data,
        isActive: props.isActive,
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  render() {

    if (this.state.data === null) {
        return null;
    }

    if (this.state.data.is_sou) {
        console.warn('data: '+JSON.stringify(this.state.data));
    }
    
    let name = this.state.data.name;
    let icon = null;
    let color = 'black';
    if (this.state.index === 1) {
        if (this.state.data.status === 'monitoring') {
            if (this.state.data.is_sou) {
                name = '';
                if (this.state.data.side_df) {
                    for (let i = 0; i < this.state.data.side_df.length; i++) {
                        name = name + this.state.data.side_df[i].nameSide;
                        if (this.state.data.side_df.length > i + 1) {
                            name = name + ', ';
                        }
                    }
                }
            } else {
                name = (this.state.data.side_pl != null ? this.state.data.side_pl : '') + ' - ' + (this.state.data.side_df != null ? this.state.data.side_df : '');
            }
        } else if (this.state.data.status === 'loading') {
            name = 'Идет синхронизация дела';
            color = '#717275';
            icon = (<Image
                source={require('./../../assets/ic-search.png')}
                style={{
                    width: Common.getLengthByIPhone7(12),
                    height: Common.getLengthByIPhone7(12),
                    marginRight: Common.getLengthByIPhone7(5),
                    resizeMode: 'contain',
                }}/>);
        } else {
            name = 'Дело не найдено';
            color = '#717275';
            icon = (<Image
                source={require('./../../assets/ic-red-cross.png')}
                style={{
                    width: Common.getLengthByIPhone7(6),
                    height: Common.getLengthByIPhone7(6),
                    marginRight: Common.getLengthByIPhone7(5),
                    resizeMode: 'contain',
                }}/>);
        }
        
    } else {
        name = this.state.data.inn;
    }

    if (this.state.data.new > 0) {
        icon = (<View style={{
            width: Common.getLengthByIPhone7(9),
            height: Common.getLengthByIPhone7(9),
            borderRadius: Common.getLengthByIPhone7(9) / 2,
            backgroundColor: Colors.orangeColor,
            marginRight: Common.getLengthByIPhone7(5),
        }} />);
    }

    let round = null;
    
    if (this.state.index === 0) {
        round = (<View style={{
            width: Common.getLengthByIPhone7(50),
            height: Common.getLengthByIPhone7(50),
            borderRadius: Common.getLengthByIPhone7(8),
            backgroundColor: '#F6F6F6',
            alignItems: 'center',
            justifyContent: 'center',
        }}>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(24),
                // lineHeight: Common.getLengthByIPhone7(27),
                // letterSpacing: -0.55,
            }}>
            {this.state.data.total_cases != null ? this.state.data.total_cases : this.state.data.total_evets}
            </Text>
            {this.state.data.new != null ? (<View style={{
                minWidth: Common.getLengthByIPhone7(16),
                height: Common.getLengthByIPhone7(16),
                borderRadius: Common.getLengthByIPhone7(8),
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: Colors.mainColor,
                position: 'absolute',
                top: -5,
                right: -5,
            }}>
                <Text style={{
                    color: 'white',
                    fontFamily: 'Montserrat-Bold',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(10),
                    // lineHeight: Common.getLengthByIPhone7(27),
                    // letterSpacing: -0.55,
                }}>
                    {this.state.data.new}
                </Text>
            </View>) : null}
        </View>);
    } else {
        if (this.state.data.status === 'monitoring') {
            round = (<View style={{
                width: Common.getLengthByIPhone7(50),
                height: Common.getLengthByIPhone7(50),
                borderRadius: Common.getLengthByIPhone7(8),
                backgroundColor: '#F6F6F6',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <Text style={{
                    color: Colors.textColor,
                    fontFamily: 'Montserrat-Bold',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(24),
                    // lineHeight: Common.getLengthByIPhone7(27),
                    // letterSpacing: -0.55,
                }}>
                {this.state.data.total_cases != null ? this.state.data.total_cases : this.state.data.total_evets}
                </Text>
                {this.state.data.new != null ? (<View style={{
                    minWidth: Common.getLengthByIPhone7(16),
                    height: Common.getLengthByIPhone7(16),
                    borderRadius: Common.getLengthByIPhone7(8),
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: Colors.mainColor,
                    position: 'absolute',
                    top: -5,
                    right: -5,
                }}>
                    <Text style={{
                        color: 'white',
                        fontFamily: 'Montserrat-Bold',
                        fontWeight: 'bold',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(10),
                        // lineHeight: Common.getLengthByIPhone7(27),
                        // letterSpacing: -0.55,
                    }}>
                        {this.state.data.new}
                    </Text>
                </View>) : null}
            </View>);
        } else if (this.state.data.status === 'loading') {
            round = (<View style={{
                width: Common.getLengthByIPhone7(50),
                height: Common.getLengthByIPhone7(50),
                borderRadius: Common.getLengthByIPhone7(8),
                backgroundColor: 'rgba(0, 5, 123, 0.1)',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <Image
                source={require('./../../assets/ic-info2.png')}
                style={{
                    width: Common.getLengthByIPhone7(31),
                    height: Common.getLengthByIPhone7(31),
                    resizeMode: 'contain',
                }}/>
            </View>);
        } else {
            round = (<View style={{
                width: Common.getLengthByIPhone7(50),
                height: Common.getLengthByIPhone7(50),
                borderRadius: Common.getLengthByIPhone7(8),
                backgroundColor: 'rgba(249, 0, 0, 0.06)',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <Image
                source={require('./../../assets/ic-close2.png')}
                style={{
                    width: Common.getLengthByIPhone7(31),
                    height: Common.getLengthByIPhone7(31),
                    resizeMode: 'contain',
                }}/>
            </View>);
        }
    }

    return (
        <SwipeableItem
            key={this.state.data.id}
            item={this.state.data}
            ref={ref => this.itemRef = ref}
            onChange={({ open }) => {
                // if (open) {
                // // Close all other open items
                // [...this.itemRefs.entries()].forEach(([key, ref]) => {
                //     if (key !== this.state.data.id && ref) ref.close();
                // });
                // }
            }}
            overSwipe={20}
            renderUnderlayLeft={({item, percentOpen}) => {
                return (<View style={{
                    width: Common.getLengthByIPhone7(0),
                    // width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(93)*2,
                    // width: Common.getLengthByIPhone7(93)*3,
                    height: '100%',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    flexDirection: 'row',
                }}>
                    <TouchableOpacity style={{
                        width: Common.getLengthByIPhone7(93),
                        height: '100%',
                        // height: Common.getLengthByIPhone7(93),
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: Colors.orangeColor,
                    }}
                    onPress={() => {
                        if (this.props.onEditCase) {
                            this.props.onEditCase(this.state.data);
                        }
                        this.itemRef.close();
                    }}>
                        <Image source={require('./../../assets/ic-pen.png')} style={{
                            resizeMode: 'contain',
                            width: Common.getLengthByIPhone7(30),
                            height: Common.getLengthByIPhone7(30),
                            tintColor: 'white',
                        }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        width: Common.getLengthByIPhone7(93),
                        height: '100%',
                        // height: Common.getLengthByIPhone7(93),
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: Colors.placeholderColor,
                    }}
                    onPress={() => {
                        if (this.props.onChangeStatus) {
                            this.props.onChangeStatus(this.state.data);
                        }
                        this.itemRef.close();
                    }}>
                        <Image source={this.state.data.new > 0 ? require('./../../assets/ic-read.png') : require('./../../assets/ic-unread.jpg')} style={{
                            resizeMode: 'contain',
                            width: Common.getLengthByIPhone7(24),
                            height: Common.getLengthByIPhone7(24),
                            tintColor: 'white',
                        }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        width: Common.getLengthByIPhone7(93),
                        height: '100%',
                        // height: Common.getLengthByIPhone7(93),
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'red',
                    }}
                    onPress={() => {
                        if (this.props.onDelete) {
                            this.props.onDelete(this.state.data);
                        }
                        this.itemRef.close();
                    }}>
                        <Image source={require('./../../assets/ic-trash.png')} style={{
                            resizeMode: 'contain',
                            width: Common.getLengthByIPhone7(18),
                            height: Common.getLengthByIPhone7(24),
                        }} />
                    </TouchableOpacity>
                </View>);
            }}
            snapPointsLeft={[this.state.index === 0 ? Common.getLengthByIPhone7(93)*3 : Common.getLengthByIPhone7(93)*3]}
            activationThreshold={10}
        >
            <TouchableOpacity style={{
                width: Common.getLengthByIPhone7(0),
                opacity: (this.state.isActive ? 0.3 : 1),
                backgroundColor: 'white',
            }}
            onLongPress={this.props.onLongPress}
            activeOpacity={1}
            onPress={() => {
                if (this.state.index === 0) {
                    this.props.navigation.navigate('Company', {data: this.state.data});
                } else {
                    if (this.state.data.status === 'monitoring') {
                        if (this.state.data.is_sou) {
                            this.props.navigation.navigate('CouItem', {data: this.state.data});
                        } else {
                            this.props.navigation.navigate('Item', {data: this.state.data});
                        }
                    }
                }
            }}>
                <View style={{
                    width: Common.getLengthByIPhone7(0),
                    // height: Common.getLengthByIPhone7(93),
                    paddingLeft: Common.getLengthByIPhone7(16),
                    paddingRight: Common.getLengthByIPhone7(16),
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    backgroundColor: 'white',
                    paddingTop: Common.getLengthByIPhone7(10),
                    opacity: (this.state.isActive ? 0.3 : 1),
                    // backgroundColor: (this.state.data.new > 0 ? 'rgba(0, 0, 0, 0.1)' : 'white'),
                    backgroundColor: 'white',
                }}>
                    <View style={{
                        // width: Common.getLengthByIPhone7(277),
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                        // height: Common.getLengthByIPhone7(93),
                        borderBottomWidth: 1,
                        borderBottomColor: '#e8e8e8',
                        justifyContent: 'center',
                        paddingBottom: Common.getLengthByIPhone7(10),
                    }}>
                        <View style={{
                            // width: Common.getLengthByIPhone7(277),
                            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'flex-start',
                        }}>
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                            }}>
                                {icon}
                                <Text style={{
                                    width: Common.getLengthByIPhone7(180),
                                    // backgroundColor: 'red',
                                    color: 'black',
                                    fontFamily: 'Montserrat-Regular',
                                    fontWeight: 'bold',
                                    textAlign: 'left',
                                    fontSize: Common.getLengthByIPhone7(16),
                                    lineHeight: Common.getLengthByIPhone7(19),
                                    // letterSpacing: -0.55,
                                }}
                                numberOfLines={2}>
                                    {this.state.index === 1 ? (this.state.data.name !== null ? this.state.data.name : this.state.data.value) : this.state.data.name_custom !== null ? this.state.data.name_custom : this.state.data.name}
                                </Text>
                            </View>
                            <Text style={{
                                color: '#bdbdbd',
                                fontFamily: 'Montserrat-Regular',
                                fontWeight: 'normal',
                                textAlign: 'right',
                                fontSize: Common.getLengthByIPhone7(9),
                                lineHeight: Common.getLengthByIPhone7(11),
                                // letterSpacing: -0.55,
                            }}>
                                {this.state.data.last_event}
                            </Text>
                        </View>
                        <Text style={{
                            // width: Common.getLengthByIPhone7(277),
                            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                            marginTop: 5,
                            color: color,
                            fontFamily: 'Montserrat-Regular',
                            fontWeight: 'normal',
                            textAlign: 'left',
                            fontSize: Common.getLengthByIPhone7(12),
                            lineHeight: Common.getLengthByIPhone7(15),
                        }}>
                            {name}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        </SwipeableItem>
    );
  }
}