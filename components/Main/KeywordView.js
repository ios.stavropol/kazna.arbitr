import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import SwipeableItem from 'react-native-swipeable-item';
import Animated from 'react-native-reanimated';

export default class KeywordView extends React.Component {

    itemRef = null;

  state = {
    index: 0,
    index2: 0,
    data: null,
    isActive: false,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        index2: this.props.index2,
        data: this.props.data,
        isActive: this.props.isActive,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
        index2: props.index2,
        data: props.data,
        isActive: props.isActive,
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  render() {

    if (this.state.data === null) {
        return null;
    }

    // console.warn('data: '+JSON.stringify(this.state.data));

    return (
        <SwipeableItem
            key={this.state.data.id}
            item={this.state.data}
            ref={ref => this.itemRef = ref}
            onChange={({ open }) => {
                // if (open) {
                // // Close all other open items
                // [...this.itemRefs.entries()].forEach(([key, ref]) => {
                //     if (key !== this.state.data.id && ref) ref.close();
                // });
                // }
            }}
            overSwipe={20}
            renderUnderlayLeft={({item, percentOpen}) => {
                return (<View style={{
                    width: Common.getLengthByIPhone7(0),
                    // width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(93)*2,
                    // width: Common.getLengthByIPhone7(93)*3,
                    height: '100%',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    flexDirection: 'row',
                }}>
                    <TouchableOpacity style={{
                        width: Common.getLengthByIPhone7(93),
                        height: '100%',
                        // height: Common.getLengthByIPhone7(93),
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: Colors.placeholderColor,
                    }}
                    onPress={() => {
                        if (this.props.onChangeStatus) {
                            this.props.onChangeStatus(this.state.data);
                        }
                        this.itemRef.close();
                    }}>
                        <Image source={this.state.data.new > 0 ? require('./../../assets/ic-read.png') : require('./../../assets/ic-unread.jpg')} style={{
                            resizeMode: 'contain',
                            width: Common.getLengthByIPhone7(24),
                            height: Common.getLengthByIPhone7(24),
                            tintColor: 'white',
                        }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        width: Common.getLengthByIPhone7(93),
                        height: '100%',
                        // height: Common.getLengthByIPhone7(93),
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'red',
                    }}
                    onPress={() => {
                        if (this.props.onDelete) {
                            this.props.onDelete(this.state.data);
                        }
                        this.itemRef.close();
                    }}>
                        <Image source={require('./../../assets/ic-trash.png')} style={{
                            resizeMode: 'contain',
                            width: Common.getLengthByIPhone7(18),
                            height: Common.getLengthByIPhone7(24),
                        }} />
                    </TouchableOpacity>
                </View>);
            }}
            snapPointsLeft={[Common.getLengthByIPhone7(93)*2]}
            activationThreshold={10}
        >
            <TouchableOpacity style={{
                width: Common.getLengthByIPhone7(0),
                opacity: (this.state.isActive ? 0.3 : 1),
                backgroundColor: 'white',
            }}
            onLongPress={this.props.onLongPress}
            activeOpacity={1}
            onPress={() => {
                this.props.navigation.navigate('Word', {data: this.state.data});
            }}>
                <View style={{
                    width: Common.getLengthByIPhone7(0),
                    // height: Common.getLengthByIPhone7(93),
                    paddingLeft: Common.getLengthByIPhone7(16),
                    paddingRight: Common.getLengthByIPhone7(16),
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    backgroundColor: 'white',
                    paddingTop: Common.getLengthByIPhone7(10),
                    opacity: (this.state.isActive ? 0.3 : 1),
                    backgroundColor: (this.state.data.new > 0 ? 'rgba(0, 0, 0, 0.1)' : 'white'),
                }}>
                    <View style={{
                        // width: Common.getLengthByIPhone7(277),
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                        // height: Common.getLengthByIPhone7(93),
                        borderBottomWidth: 1,
                        borderBottomColor: '#e8e8e8',
                        justifyContent: 'center',
                        paddingBottom: Common.getLengthByIPhone7(10),
                    }}>
                        <View style={{
                            // width: Common.getLengthByIPhone7(277),
                            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'flex-start',
                        }}>
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                            }}>
                                <Text style={{
                                    width: Common.getLengthByIPhone7(180),
                                    // backgroundColor: 'red',
                                    color: 'black',
                                    fontFamily: 'Montserrat-Regular',
                                    fontWeight: 'bold',
                                    textAlign: 'left',
                                    fontSize: Common.getLengthByIPhone7(16),
                                    lineHeight: Common.getLengthByIPhone7(19),
                                    // letterSpacing: -0.55,
                                }}
                                numberOfLines={2}>
                                    {this.state.data.value}
                                </Text>
                            </View>
                            <Text style={{
                                color: '#bdbdbd',
                                fontFamily: 'Montserrat-Regular',
                                fontWeight: 'normal',
                                textAlign: 'right',
                                fontSize: Common.getLengthByIPhone7(9),
                                lineHeight: Common.getLengthByIPhone7(11),
                                // letterSpacing: -0.55,
                            }}>
                                {this.state.data.last_event}
                            </Text>
                        </View>
                        <Text style={{
                            // width: Common.getLengthByIPhone7(277),
                            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                            marginTop: 5,
                            color: 'black',
                            fontFamily: 'Montserrat-Regular',
                            fontWeight: 'normal',
                            textAlign: 'left',
                            fontSize: Common.getLengthByIPhone7(12),
                            lineHeight: Common.getLengthByIPhone7(15),
                        }}>
                            Судебных актов за неделею: {this.state.data.total_cases}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        </SwipeableItem>
    );
  }
}