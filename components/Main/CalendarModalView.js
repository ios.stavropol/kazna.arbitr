import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  Animated,
  Easing,
  TouchableOpacity,
  Dimensions,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, { getCalendar } from './../../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import Modal from 'react-native-modal';
import {LocaleConfig, Agenda} from 'react-native-calendars';

LocaleConfig.locales['ru'] = {
  monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
  monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
  dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
  dayNamesShort: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
  today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'ru';


@observer
export default class CalendarModalView extends React.Component {

    spinValue = new Animated.Value(0);
    
  state = {
    refresh: false,
    show: false,
    days: {},
    selected: new Date(),
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
    });
    this._refresh();
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      show: props.show,
    });
    this._refresh();
  }

  constructor(props) {
    super(props);

  }

  _refresh = () => {
    getCalendar()
    .then(data => {
        let days = {};

        for (let i = 0; i < data.length; i++) {
            let start = data[i].datetime_start;
            start = Common.getEngDate(new Date(start));

            if (days[start]) {
                days[start].push({
                    start: data[i].datetime_start,
                    end: data[i].datetime_end,
                    case_id: data[i].case_id,
                    head: data[i].head,
                    second: data[i].second_line,
                    third: data[i].third_line,
                });
            } else {
                days[start] = [];
                days[start].push({
                    start: data[i].datetime_start,
                    end: data[i].datetime_end,
                    case_id: data[i].case_id,
                    head: data[i].head,
                    second: data[i].second_line,
                    third: data[i].third_line,
                });
            }
        }

        Object.keys(days).sort().forEach(function(key) {
            var value = days[key];
            delete days[key];
            days[key] = value;
        });

        if (Object.keys(days).length > 1) {
            let start = new Date(Object.keys(days)[0]);
            let current = new Date(Object.keys(days)[0]);
            let end = new Date(Object.keys(days)[Object.keys(days).length - 1]);
            while(current <= end) {
                if (!days[Common.getEngDate(current)]) {
                    days[Common.getEngDate(current)] = [];
                }
                current.setDate(current.getDate() + 1);
            }
            console.warn('start: '+start+' end: '+end);
        }

        console.warn('after: '+JSON.stringify(days));

        let selected = Common.getEngDate(new Date());

        if (Object.keys(days).length) {
            selected = Common.getEngDate(new Date(Object.keys(days)[0]));
        }

        this.setState({
            days,
            selected,
        });

    })
    .catch(err => {
      
    });
  }

  render() {
    
    console.warn('calendar: '+JSON.stringify(Network.calendarList));
    return (<View style={{
        // flex: 1,
      }}>
        <Modal isVisible={this.state.show}>
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
                <TouchableOpacity style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'transparent',
                }}
                onPress={() => {
                    this.setState({
                        text: '',
                        companies: [],
                    }, () => {
                        
                    });
                    if (this.props.onClose) {
                        this.props.onClose();
                    }
                }}
                />
                <View style={{
                    width: Common.getLengthByIPhone7(0),
                    height: Dimensions.get('window').height - Common.getLengthByIPhone7(60),
                    backgroundColor: 'white',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    marginBottom: -20,
                }}>
                    <View style={{
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                        alignItems: 'flex-start',
                    }}>
                        <View style={{
                            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginTop: Common.getLengthByIPhone7(40),
                        }}>
                            <TouchableOpacity style={{
                                width: Common.getLengthByIPhone7(30),
                                height: Common.getLengthByIPhone7(30),
                                alignItems: 'flex-start',
                                justifyContent: 'flex-start',
                            }}
                            onPress={() => {
                                this.setState({
                                    text: '',
                                    companies: [],
                                }, () => {
                                    
                                });
                                if (this.props.onClose) {
                                    this.props.onClose();
                                }
                            }}>
                                <Image
                                    source={require('./../../assets/ic-close.png')}
                                    style={{
                                        width: Common.getLengthByIPhone7(12),
                                        height: Common.getLengthByIPhone7(12),
                                        resizeMode: 'contain',
                                    }}
                                />
                            </TouchableOpacity>
                        </View>
                        <Text style={{
                            marginTop: Common.getLengthByIPhone7(10),
                            color: Colors.textColor,
                            fontSize: Common.getLengthByIPhone7(22),
                            lineHeight: Common.getLengthByIPhone7(27),
                            letterSpacing: -0.55,
                            fontFamily: 'Montserrat-Regular',
                            textAlign: 'center',
                        }}
                        allowFontScaling={false}>
                            Календарь заседаний
                        </Text>
                    </View>
                    <View style={{
                        width: Common.getLengthByIPhone7(0),
                        flex: 1,
                    }}>
                        <Agenda
                            firstDay={1}
                            // // The list of items that have to be displayed in agenda. If you want to render item as empty date
                            // // the value of date key has to be an empty array []. If there exists no value for date key it is
                            // // considered that the date in question is not yet loaded
                            items={this.state.days}
                            // // Callback that gets called when items for a certain month should be loaded (month became visible)
                            // loadItemsForMonth={(month) => {console.log('trigger items loading')}}
                            // // Callback that fires when the calendar is opened or closed
                            // onCalendarToggled={(calendarOpened) => {console.log(calendarOpened)}}
                            // // Callback that gets called on day press
                            onDayPress={day => {
                                console.warn('day pressed: '+JSON.stringify(day));
                            }}
                            // // Callback that gets called when day changes while scrolling agenda list
                            onDayChange={day => {
                                console.warn('day changed: '+JSON.stringify(day));
                            }}
                            // // Initially selected day
                            selected={this.state.selected}
                            // // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                            // minDate={'2012-05-10'}
                            // // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                            // maxDate={'2012-05-30'}
                            // // Max amount of months allowed to scroll to the past. Default = 50
                            pastScrollRange={1}
                            // // Max amount of months allowed to scroll to the future. Default = 50
                            futureScrollRange={50}
                            // // Specify how each item should be rendered in agenda
                            renderItem={(item, firstItemInDay) => {

                                return (
                                    <TouchableOpacity
                                        testID={'item'}
                                        style={[{
                                            backgroundColor: 'white',
                                            // flex: 1,
                                            borderRadius: 5,
                                            padding: Common.getLengthByIPhone7(10),
                                            marginRight: Common.getLengthByIPhone7(10),
                                            marginTop: Common.getLengthByIPhone7(10),
                                        }, {
                                            // height: item.height
                                        }]}
                                        onPress={() => {
                                            Alert.alert(
                                                Config.appName,
                                                "Перейти в карточку дела?",
                                                [
                                                    {
                                                    text: "Нет",
                                                    onPress: () => console.log("Cancel Pressed"),
                                                    style: "cancel"
                                                    },
                                                    { text: "Да", onPress: () => {
                                                        if (this.props.onClickCase) {
                                                            this.props.onClickCase(item.case_id);
                                                        }
                                                    }}
                                                ]
                                            );
                                        }}
                                    >
                                        <Text style={{
                                            // marginTop: Common.getLengthByIPhone7(10),
                                            color: Colors.textColor,
                                            fontFamily: 'Montserrat-Bold',
                                            fontWeight: 'bold',
                                            textAlign: 'left',
                                            fontSize: Common.getLengthByIPhone7(14),
                                            lineHeight: Common.getLengthByIPhone7(16),
                                        }}>
                                            {Common.getTime(new Date(item.start))} - {Common.getTime(new Date(item.end))}
                                        </Text>
                                        <Text style={{
                                            marginTop: Common.getLengthByIPhone7(5),
                                            color: Colors.textColor,
                                            fontFamily: 'Montserrat-Bold',
                                            fontWeight: 'bold',
                                            textAlign: 'left',
                                            fontSize: Common.getLengthByIPhone7(14),
                                            lineHeight: Common.getLengthByIPhone7(16),
                                        }}>
                                            {item.head}
                                        </Text>
                                        <Text style={{
                                            marginTop: Common.getLengthByIPhone7(5),
                                            color: Colors.textColor,
                                            fontFamily: 'Montserrat-Regular',
                                            fontWeight: 'normal',
                                            textAlign: 'left',
                                            fontSize: Common.getLengthByIPhone7(14),
                                            lineHeight: Common.getLengthByIPhone7(16),
                                        }}>
                                            {item.second}
                                        </Text>
                                        <Text style={{
                                            marginTop: Common.getLengthByIPhone7(5),
                                            color: Colors.textColor,
                                            fontFamily: 'Montserrat-Regular',
                                            fontWeight: 'normal',
                                            textAlign: 'left',
                                            fontSize: Common.getLengthByIPhone7(14),
                                            lineHeight: Common.getLengthByIPhone7(16),
                                        }}>
                                            {item.third}
                                        </Text>
                                    </TouchableOpacity>
                                );
                            }}
                            // // Specify how each date should be rendered. day can be undefined if the item is not first in that day
                            // // renderDay={(day, item) => {return (<View />);}}
                            // // Specify how empty date content with no items should be rendered
                            // // renderEmptyDate={() => {return (<View />);}}
                            // // Specify how agenda knob should look like
                            // renderKnob={() => {return (<View />);}}
                            // // Specify what should be rendered instead of ActivityIndicator
                            renderEmptyData = {() => {return (<View style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                <Text style={{
                                color: Colors.textColor,
                                fontFamily: 'Montserrat-Regular',
                                fontWeight: 'bold',
                                textAlign: 'left',
                                fontSize: Common.getLengthByIPhone7(20),
                                // lineHeight: Common.getLengthByIPhone7(16),
                                // letterSpacing: -0.55,
                                }}>
                                    Событий нет
                                </Text>
                            </View>);}}
                            // showClosingKnob={false}
                            // // Specify your item comparison function for increased performance
                            // rowHasChanged={(r1, r2) => {return r1.name !== r2.name}}
                            // // Hide knob button. Default = false
                            // hideKnob={false}
                            // // When `true` and `hideKnob` prop is `false`, the knob will always be visible and the user will be able to drag the knob up and close the calendar. Default = false
                            // showClosingKnob={false}
                            // // By default, agenda dates are marked if they have at least one item, but you can override this if needed
                            // markedDates={{
                            //     '2012-05-16': {selected: true, marked: true},
                            //     '2012-05-17': {marked: true},
                            //     '2012-05-18': {disabled: true}
                            // }}
                            // // If disabledByDefault={true} dates flagged as not disabled will be enabled. Default = false
                            // disabledByDefault={true}
                            // // If provided, a standard RefreshControl will be added for "Pull to Refresh" functionality. Make sure to also set the refreshing prop correctly
                            // onRefresh={() => console.log('refreshing...')}
                            // // Set this true while waiting for new data from a refresh
                            // refreshing={false}
                            // // Add a custom RefreshControl component, used to provide pull-to-refresh functionality for the ScrollView
                            // refreshControl={null}
                            // // Agenda theme
                            theme={{
                                selectedDayBackgroundColor: Colors.orangeColor,
                                dotColor: Colors.orangeColor,
                                agendaKnobColor: Colors.orangeColor,
                            }}
                        />
                    </View>
                </View>
            </View>
        </Modal>
        <Spinner
          visible={this.state.loading}
          // textContent={'Загрузка...'}
          onShow={() => {
            Animated.loop(
              Animated.timing(
                this.spinValue,
                {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear,
                useNativeDriver: true
                }
              )
            ).start();
          }}
          onDismiss={() => {

          }}
          customIndicator={<View style={{
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 20000,
          }}>
            <View style={{
              width: Common.getLengthByIPhone7(65),
              height: Common.getLengthByIPhone7(65),
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <Animated.Image
                style={{
                  width: Common.getLengthByIPhone7(65),
                  height: Common.getLengthByIPhone7(65),
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  transform: [{rotate: this.spinValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '360deg']
                  })}] 
                }}
                source={require('./../assets/ic-loader.png')}
              />
              <TouchableOpacity style={{
                // marginTop: Common.getLengthByIPhone7(20),
                padding: Common.getLengthByIPhone7(5),
                backgroundColor: 'rgba(255, 255, 255, 0.8)',
                borderRadius: Common.getLengthByIPhone7(5),
              }}
              onPress={() => {
                this.setState({
                  loading: false,
                });
              }}>
                <Image
                  style={{
                    width: Common.getLengthByIPhone7(18),
                    height: Common.getLengthByIPhone7(18),
                  }}
                  source={require('./../assets/ic-loader-close.png')}
                />
              </TouchableOpacity>
            </View>
          </View>}
          overlayColor={'rgba(32, 42, 91, 0.3)'}
          textStyle={{color: '#FFF'}}
        />
      </View>);
  }
}
