import React from 'react';
import {
  Text,
  View,
  Image,
  Platform,
} from 'react-native';
import Colors from './../constants/Colors';
import Common from './../Utilites/Common';
import { observer } from 'mobx-react';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { TouchableOpacity } from 'react-native-gesture-handler';

@observer
export default class NavBar extends React.Component {

  state = {
    
  };

  UNSAFE_componentWillMount() {
    
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  render() {

    return (<View style={{
        width: Common.getLengthByIPhone7(0),
        marginTop: Common.getLengthByIPhone7(10),
        height: isIphoneX() ? Common.getLengthByIPhone7(98) : (Platform.OS === 'ios' ? Common.getLengthByIPhone7(68) + 20 : Common.getLengthByIPhone7(68)),
        // height: Dimensions.get('window').height,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        backgroundColor: 'white',
        paddingBottom: isIphoneX() ? Common.getLengthByIPhone7(30) : (Platform.OS === 'ios' ? Common.getLengthByIPhone7(30) : Common.getLengthByIPhone7(30)),
      }}>
        <Text style={{
          marginLeft: Common.getLengthByIPhone7(16),
          color: '#EB5E1F',
          fontFamily: 'Montserrat-Bold',
          fontWeight: 'bold',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(24),
          lineHeight: Common.getLengthByIPhone7(24), 
        }}>
          L<Text style={{
            color: Colors.textColor,
            fontFamily: 'Montserrat-Bold',
            fontWeight: 'bold',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(24),
            lineHeight: Common.getLengthByIPhone7(24), 
          }}>
            egal.Track
          </Text>
        </Text>
        {this.props.button}
      </View>);
  }
}
