import React from 'react';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    Animated,
    Image,
    Platform,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import { observer } from 'mobx-react';
import AnimatedColorView from 'react-native-animated-colors';
import CardView from './CardView';

const { width } = Dimensions.get('window');
const SCREEN_WIDTH = Platform.OS === 'android' ? (Common.getLengthByIPhone7(110) + 3*Common.getLengthByIPhone7(232))/3 : Common.getLengthByIPhone7(232);

@observer
export default class CardRowView extends React.Component {

    offsetX = new Animated.Value(0);

  state = {
    refresh: false,
    activeIndex: 0,
    cases: 0,
    companies: 0,
    keywords: 0,
  };

  UNSAFE_componentWillMount() {
    this.setState({
      cases: this.props.cases,
      companies: this.props.companies,
      keywords: this.props.keywords,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      cases: props.cases,
      companies: props.companies,
      keywords: props.keywords,
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  rotateTransform = (index) => {
    return {
      transform: [{
        scale: this.offsetX.interpolate({
          inputRange: [
            (index - 1) * SCREEN_WIDTH,// - (Common.getLengthByIPhone7(0) - SCREEN_WIDTH)/2,
            index * SCREEN_WIDTH,// - (Common.getLengthByIPhone7(0) - SCREEN_WIDTH)/2,
            (index + 1) * SCREEN_WIDTH,// - (Common.getLengthByIPhone7(0) - SCREEN_WIDTH)/2
          ],
          // outputRange: [2.0, 2.14, 2.0],
          outputRange: [1.0, 1.25, 1.0],
        })
      }],
      zIndex: this.offsetX.interpolate({
        inputRange: [
          (index - 1) * SCREEN_WIDTH,// - (Common.getLengthByIPhone7(0) - SCREEN_WIDTH)/2,
          index * SCREEN_WIDTH,// - (Common.getLengthByIPhone7(0) - SCREEN_WIDTH)/2,
          (index + 1) * SCREEN_WIDTH,// - (Common.getLengthByIPhone7(0) - SCREEN_WIDTH)/2
        ],
        outputRange: [1, 2, 1]
      }),
    };
  }

  render() {

    let position = Animated.divide(this.offsetX, width);
    let pages = [];
    pages.push('1');
    pages.push('2');

    return (<View style={{
        marginTop: Common.getLengthByIPhone7(16),
        width: Common.getLengthByIPhone7(0),
        backgroundColor: 'white',
        justifyContent: 'flex-start',
      }}>
        <View
            style={{
                width: Common.getLengthByIPhone7(0),
                flexDirection: 'row',
                position: 'absolute',
                // bottom: Common.getLengthByIPhone7(33)+hh,
                top: 0,//Common.getLengthByIPhone7(5),
                alignItems: 'center',
                justifyContent: 'center'
            }}
        >
            <AnimatedColorView
                activeIndex={this.state.activeIndex}
                colors={['#989898', '#d8d8d8', '#d8d8d8']}
                duration={100}
                loop={false}
                style={{
                    width: 6,
                    height: 6,
                    borderRadius: 3,
                    marginRight: 3,
                    overflow: 'hidden',
                }}
            />
            <AnimatedColorView
                activeIndex={this.state.activeIndex}
                colors={['#d8d8d8', '#989898', '#d8d8d8']}
                duration={100}
                loop={false}
                style={{
                    width: 6,
                    height: 6,
                    borderRadius: 3,
                    marginRight: 3,
                    overflow: 'hidden',
                }}
            />
        </View>
        <ScrollView style={{
            width: Common.getLengthByIPhone7(0),
            height: Common.getLengthByIPhone7(220),
          }}
          contentContainerStyle={{
              alignItems: 'center'
          }}
          horizontal={true}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          decelerationRate={'fast'}
          snapToInterval={((Platform.OS === 'ios' ? Common.getLengthByIPhone7(210) : Common.getLengthByIPhone7(110)) + 3*Common.getLengthByIPhone7(232))/3}
          snapToAlignment={"center"}
          onMomentumScrollEnd={() => {
            if (this.props.onChangePage) {
              this.props.onChangePage(this.state.activeIndex);
            }
          }}
          onScroll={event => {
            //   console.warn('x: '+event.nativeEvent.contentOffset.x);
            Animated.timing(this.offsetX, {
              toValue: event.nativeEvent.contentOffset.x,
              duration: 0,
              useNativeDriver: true,
            })
            .start();
            this.setState({
                activeIndex: Math.round(event.nativeEvent.contentOffset.x/((Common.getLengthByIPhone7(210) + 3*Common.getLengthByIPhone7(232))/3)),
            }, () => {
                
            });
          }}
        //   onScroll={Animated.event(
        //     [
        //         {
        //             nativeEvent: {contentOffset: {x: this.offsetX}},
        //         },
        //     ],
        //     {
        //         // useNativeDriver: true,
        //     }
        // )}
        >
            <CardView
                title={'Компании'}
                subtitle={'Отслеживание исков к компаниям в арбитражных судах'}
                sub1={'Компании'}
                sub2={'Новые дела'}
                button={'Добавить компанию'}
                all={this.state.companies}
                index={0}
                activeIndex={this.state.activeIndex}
                rotateTransform={this.rotateTransform(0)}
                style={{
                    marginLeft: Common.getLengthByIPhone7(65)
                }}
                onClick={() => {
                  if (this.props.onClick) {
                    this.props.onClick('company');
                  }
                }}
            />
            <CardView
                title={'Арбитражные дела'}
                subtitle={'Отслеживайте изменения в судебных делах'}
                sub1={'Подписки'}
                sub2={'Новые события'}
                button={'Добавить дело'}
                all={this.state.cases}
                index={1}
                activeIndex={this.state.activeIndex}
                rotateTransform={this.rotateTransform(1)}
                style={{
                    marginLeft: Common.getLengthByIPhone7(40),
                    marginRight: Common.getLengthByIPhone7(65)
                }}
                onClick={() => {
                  if (this.props.onClick) {
                    this.props.onClick('case');
                  }
                }}
            />
        </ScrollView>
      </View>);

{/* <CardView
title={'Ключевые слова'}
subtitle={'Отслеживание судебныы акты по ключевым словам и фразам'}
sub1={'Подписки'}
sub2={'Новые акты'}
button={'Добавить подписку'}
all={this.state.keywords}
index={2}
activeIndex={this.state.activeIndex}
rotateTransform={this.rotateTransform(2)}
style={{
    marginLeft: Common.getLengthByIPhone7(45),
    marginRight: Common.getLengthByIPhone7(65)
}}
onClick={() => {
  if (this.props.onClick) {
    this.props.onClick('keyword');
  }
}}
/> */}
  }
}
