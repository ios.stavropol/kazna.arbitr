import React from 'react';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, {getStories} from './../../Utilites/Network';
import { observer } from 'mobx-react';
import { isIphoneX } from 'react-native-iphone-x-helper';

@observer
export default class TopButtonView extends React.Component {

  state = {
    refresh: false,
  };

  UNSAFE_componentWillMount() {
    getStories(this.props.display)
    .then(() => {
      this.setState({
        refresh: !this.state.refresh,
      }, () => {
        if (this.props.refresh) {
          this.props.refresh();
        }
      });
    })
    .catch(err => {

    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  renderButton = (index, data, style) => {
    return (<TouchableOpacity style={[{
      width: Common.getLengthByIPhone7(84),
      height: Common.getLengthByIPhone7(66),
      borderRadius: Common.getLengthByIPhone7(8),
      padding: Common.getLengthByIPhone7(10),
      backgroundColor: Colors.grayC4,
    }, style]}
    onPress={() => {
      if (this.props.onClick) {
        this.props.onClick(index);
      }
    }}>
      <Text style={{
        color: Colors.textColor,
        fontFamily: 'Montserrat-Bold',
        fontWeight: 'bold',
        textAlign: 'left',
        fontSize: Common.getLengthByIPhone7(9),
        lineHeight: Common.getLengthByIPhone7(11),
      }}>
        {data.header}
      </Text>
    </TouchableOpacity>);
  }

  render() {

    let buttons = [];

    if (this.props.display === 'case') {
      for (let i=0;i<Network.caseClipsList.length;i++) {
        buttons.push(this.renderButton(i, Network.caseClipsList[i], {marginLeft: i === 0 ? Common.getLengthByIPhone7(16) : Common.getLengthByIPhone7(14), marginRight: i + 1 === Network.caseClipsList.length ? Common.getLengthByIPhone7(16) : 0}));
      }
    } else {
      for (let i=0;i<Network.clipsList.length;i++) {
        buttons.push(this.renderButton(i, Network.clipsList[i], {marginLeft: i === 0 ? Common.getLengthByIPhone7(16) : Common.getLengthByIPhone7(14), marginRight: i + 1 === Network.clipsList.length ? Common.getLengthByIPhone7(16) : 0}));
      }
    }

    if (buttons.length === 0) {
      return null;
    }
    
    return (<View style={{
        // marginTop: Common.getLengthByIPhone7(34),
        marginTop: isIphoneX() ? Common.getLengthByIPhone7(116) : Common.getLengthByIPhone7(76),
        width: Common.getLengthByIPhone7(0),
        backgroundColor: 'white',
        paddingBottom: Common.getLengthByIPhone7(22),
        borderBottomColor: '#f5f5f5',
        borderBottomWidth: 1,
      }}>
        <ScrollView style={{
            width: Common.getLengthByIPhone7(0),
            height: Common.getLengthByIPhone7(66),
          }}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
        >
          {buttons}
        </ScrollView>
      </View>);
  }
}
