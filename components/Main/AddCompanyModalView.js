import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  TouchableOpacity,
  Dimensions,
  FlatList,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
  Animated,
  Easing,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, { searchCompanies, newSubscribtion } from './../../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import Modal from 'react-native-modal';
import AnimatedButton from '../components/Buttons/AnimatedButton';
import AnimatedTextInputView from './../AnimatedTextInputView';

@observer
export default class AddCompanyModalView extends React.Component {

    spinValue = new Animated.Value(0);
    
  state = {
    refresh: false,
    show: false,
    text: '',
    companies: [],
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
        companies: [],
      });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      show: props.show,
      companies: [],
    });
  }

  constructor(props) {
    super(props);

  }

  renderRow = (item, index) => {
    return (<TouchableOpacity style={{
        width: Common.getLengthByIPhone7(0),
        paddingTop: Common.getLengthByIPhone7(10),
        paddingBottom: Common.getLengthByIPhone7(10),
        paddingLeft: Common.getLengthByIPhone7(10),
        paddingRight: Common.getLengthByIPhone7(10),
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0, 0, 0, 0.1)',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    }}
    onPress={() => {
        Alert.alert(
            Config.appName,
            "Добавить компанию " + item.data.name.full_with_opf,
            [
                {
                text: "Нет",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
                },
                { text: "Да", onPress: () => {
                    this.setState({
                        loading: true,
                    }, () => {
                        newSubscribtion('company', item.data.inn)
                        .then(() => {
                            if (Network.refreshList != null) {
                                Network.refreshList();
                            }
                            this.setState({
                                loading: false,
                                text: '',
                                companies: [],
                            }, () => {
                                if (this.props.onClose) {
                                    this.props.onClose();
                                }
                            });
                        })
                        .catch(err => {
                            this.setState({
                                loading: false,
                            }, () => {
                                setTimeout(() => {
                                    Alert.alert(Config.appName, err);
                                }, 100);
                            });
                        });
                    });
                }}
            ]
        );
    }}>
        <Text style={{
            marginBottom: Common.getLengthByIPhone7(5),
            color: Colors.textColor,
            fontSize: Common.getLengthByIPhone7(16),
            // lineHeight: Common.getLengthByIPhone7(27),
            letterSpacing: -0.55,
            fontFamily: 'Montserrat-Regular',
            textAlign: 'left',
        }}
        allowFontScaling={false}>
            {item.data.name.full_with_opf}
        </Text>
        <Text style={{
            marginBottom: Common.getLengthByIPhone7(5),
            color: Colors.textColor,
            fontSize: Common.getLengthByIPhone7(14),
            // lineHeight: Common.getLengthByIPhone7(27),
            letterSpacing: -0.55,
            fontFamily: 'Montserrat-Regular',
            textAlign: 'left',
        }}
        allowFontScaling={false}>
            ИНН: {item.data.inn}
        </Text>
        <Text style={{
            marginBottom: Common.getLengthByIPhone7(5),
            color: Colors.textColor,
            fontSize: Common.getLengthByIPhone7(14),
            // lineHeight: Common.getLengthByIPhone7(27),
            letterSpacing: -0.55,
            fontFamily: 'Montserrat-Regular',
            textAlign: 'left',
        }}
        allowFontScaling={false}>
            Адрес: {item.data.address.value}
        </Text>
    </TouchableOpacity>);
  }

  render() {
    
    let body = (<View style={{
        width: Common.getLengthByIPhone7(0),
        height: Dimensions.get('window').height - Common.getLengthByIPhone7(60),
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: -20,
    }}>
        <View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
            alignItems: 'flex-start',
        }}>
            <View style={{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: Common.getLengthByIPhone7(40),
            }}>
                <TouchableOpacity style={{
                    width: Common.getLengthByIPhone7(30),
                    height: Common.getLengthByIPhone7(30),
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start',
                }}
                onPress={() => {
                    this.setState({
                        text: '',
                        companies: [],
                    }, () => {
                        
                    });
                    if (this.props.onClose) {
                        this.props.onClose();
                    }
                }}>
                    <Image
                        source={require('./../../assets/ic-close.png')}
                        style={{
                            width: Common.getLengthByIPhone7(12),
                            height: Common.getLengthByIPhone7(12),
                            resizeMode: 'contain',
                        }}
                    />
                </TouchableOpacity>
            </View>
            <Text style={{
                marginTop: Common.getLengthByIPhone7(10),
                color: Colors.textColor,
                fontSize: Common.getLengthByIPhone7(22),
                lineHeight: Common.getLengthByIPhone7(27),
                letterSpacing: -0.55,
                fontFamily: 'Montserrat-Regular',
                textAlign: 'center',
            }}
            allowFontScaling={false}>
                Добавление новой компании
            </Text>
            <AnimatedTextInputView
                style={{
                    marginTop: Common.getLengthByIPhone7(10),
                    marginBottom: Common.getLengthByIPhone7(5),
                }}
                value={this.state.text}
                title={'Введите название или ИНН компании'}
                autoCapitalize={'words'}
                autoCorrect={false}
                onChangeText={text => {
                    this.setState({
                        text,
                    }, () => {
                        if (text.length >= 3) {
                            searchCompanies(text)
                            .then(response => {
                                this.setState({
                                    companies: response,
                                });
                            })
                            .catch(err => {

                            });
                        }
                    });
                }}
            />
        </View>
        <FlatList
            style={{
                width: Common.getLengthByIPhone7(0),
                flex: 1,
                marginBottom: Common.getLengthByIPhone7(30),
                // backgroundColor: 'red',
            }}
            contentContainerStyle={{
                justifyContent: 'flex-start',
                alignItems: 'center',
            }}
            horizontal={false}
            numColumns={1}
            data={this.state.companies}
            extraData={this.state.companies}
            renderItem={({item, index}) => this.renderRow(item, index)}
            keyExtractor={category => category.id}
        />
    </View>);

    return (<View style={{
        // flex: 1,
      }}>
        <Modal isVisible={this.state.show}>
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
                <TouchableOpacity style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'transparent',
                }}
                onPress={() => {
                    this.setState({
                        text: '',
                        companies: [],
                    }, () => {
                        
                    });
                    if (this.props.onClose) {
                        this.props.onClose();
                    }
                }}
                />
                {Platform.OS === 'ios' ? body : (<KeyboardAvoidingView behavior={Platform.OS === 'ios' ? "position" : "height"}>
                    {body}
                </KeyboardAvoidingView>)}
            </View>
        </Modal>
        <Spinner
          visible={this.state.loading}
          // textContent={'Загрузка...'}
          onShow={() => {
            Animated.loop(
              Animated.timing(
                this.spinValue,
                {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear,
                useNativeDriver: true
                }
              )
            ).start();
          }}
          onDismiss={() => {

          }}
          customIndicator={<View style={{
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 20000,
          }}>
            <View style={{
              width: Common.getLengthByIPhone7(65),
              height: Common.getLengthByIPhone7(65),
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <Animated.Image
                style={{
                  width: Common.getLengthByIPhone7(65),
                  height: Common.getLengthByIPhone7(65),
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  transform: [{rotate: this.spinValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '360deg']
                  })}] 
                }}
                source={require('./../assets/ic-loader.png')}
              />
              <TouchableOpacity style={{
                // marginTop: Common.getLengthByIPhone7(20),
                padding: Common.getLengthByIPhone7(5),
                backgroundColor: 'rgba(255, 255, 255, 0.8)',
                borderRadius: Common.getLengthByIPhone7(5),
              }}
              onPress={() => {
                this.setState({
                  loading: false,
                });
              }}>
                <Image
                  style={{
                    width: Common.getLengthByIPhone7(18),
                    height: Common.getLengthByIPhone7(18),
                  }}
                  source={require('./../assets/ic-loader-close.png')}
                />
              </TouchableOpacity>
            </View>
          </View>}
          overlayColor={'rgba(32, 42, 91, 0.3)'}
          textStyle={{color: '#FFF'}}
        />
      </View>);
  }
}
