import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, {newSubscribtion} from './../../Utilites/Network';
import { observer } from 'mobx-react';
import Modal from 'react-native-modal';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { StoryContainer } from 'react-native-stories-view';

@observer
class StoriesModalView extends React.Component {

  state = {
    refresh: false,
    show: false,
    data: [],
    text: '',
  };

  UNSAFE_componentWillMount() {
      console.warn('this.props.data: '+this.props.data);
    this.setState({
        show: this.props.show,
        data: this.props.data != null && this.props.data.length ? this.props.data.clips : [],
      });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    console.warn('props.data: '+props.data);
    this.setState({
      show: props.show,
      data: props.data != null && props.data.length ? props.data.clips : [],
    });
  }

  constructor(props) {
    super(props);

  }

  render() {

    let images = [];

    for (let i=0;i<this.state.data.length;i++) {
        images.push(this.state.data[i].image);
    }

    return (<View style={{
        // flex: 1,
      }}>
        <Modal isVisible={this.state.show}>
            <StoryContainer
                visible={true}
                enableProgress={true}
                images={images}
                duration={20}  
                onComplete={() => alert("onComplete")}
                containerStyle={{
                    width: '100%',
                    height: '100%',
                }}
            />
        </Modal>
      </View>);
  }
}

export default () => {
    // const classes = useStyles();
    return (
        <StoriesModalView/>
    )
}