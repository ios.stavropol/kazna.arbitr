import React from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    Animated,
    Image,
} from 'react-native';
import Colors from './../../constants/Colors';;
import Common from './../../Utilites/Common';
import { observer } from 'mobx-react';

const { width } = Dimensions.get('window');
const SCREEN_WIDTH = Common.getLengthByIPhone7(232);

@observer
export default class CardView extends React.Component {

    opacity = new Animated.Value(0);

  state = {
    refresh: false,
    activeIndex: 0,
    hidden: true,
  };

  UNSAFE_componentWillMount() {
    // this.setState({
    //     activeIndex: this.props.activeIndex,
    // }, () => {
    //     if (this.props.index === this.props.activeIndex) {
    //         Animated.timing(this.opacity, {
    //             toValue: 0,
    //             duration: 500,
    //         }).start(() => {
    //             this.setState({
    //                 hidden: true,
    //             });
    //         });
    //     } else {
    //         this.setState({
    //             hidden: false,
    //         }, () => {
    //             Animated.timing(this.opacity, {
    //                 toValue: 1,
    //                 duration: 500,
    //             }).start(() => {
                    
    //             });
    //         });
    //     }
    // });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    // this.setState({
    //     activeIndex: props.activeIndex,
    // }, () => {
    //     if (props.index === this.props.activeIndex) {
    //         Animated.timing(this.opacity, {
    //             toValue: 0,
    //             duration: 0,
    //         }).start(() => {
    //             this.setState({
    //                 hidden: true,
    //             });
    //         });
    //     } else {
    //         this.setState({
    //             hidden: false,
    //         }, () => {
    //             Animated.timing(this.opacity, {
    //                 toValue: 1,
    //                 duration: 0,
    //             }).start(() => {
                    
    //             });
    //         });
    //     }
    // });
  }

  constructor(props) {
    super(props);

  }

  render() {

    return (<TouchableOpacity style={[{
        width: Common.getLengthByIPhone7(232),
        height: Common.getLengthByIPhone7(143),
        borderRadius: Common.getLengthByIPhone7(10),
        paddingLeft: Common.getLengthByIPhone7(20),
        paddingRight: Common.getLengthByIPhone7(20),
        paddingTop: Common.getLengthByIPhone7(23),
        paddingBottom: Common.getLengthByIPhone7(23),
        backgroundColor: Colors.mainColor,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.15,
        shadowRadius: Common.getLengthByIPhone7(10),
        elevation: 6,
        alignItems: 'flex-start',
        justifyContent: 'space-between',
    }, this.props.rotateTransform, this.props.style]}
    activeOpacity={1}>
        <View style={{

        }}>
            <Text style={{
                color: 'white',
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(8),
                lineHeight: Common.getLengthByIPhone7(10),
                opacity: 0.5,
            }}>
                {this.props.subtitle}
            </Text>
            <Text style={{
                marginTop: 2,
                color: 'white',
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(10),
                lineHeight: Common.getLengthByIPhone7(13),
            }}>
                {this.props.title}
            </Text>
        </View>
        <View style={{

        }}>
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
            }}>
                <View style={{

                }}>
                    <Text style={{
                        color: 'white',
                        fontFamily: 'Montserrat-Regular',
                        fontWeight: 'normal',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(8),
                        lineHeight: Common.getLengthByIPhone7(10),
                        opacity: 0.5,
                    }}>
                        {this.props.sub1}
                    </Text>
                    <Text style={{
                        marginTop: 2,
                        color: 'white',
                        fontFamily: 'Montserrat-Regular',
                        fontWeight: 'normal',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(10),
                        lineHeight: Common.getLengthByIPhone7(13),
                    }}>
                        {this.props.all}
                    </Text>
                </View>
                <View style={{
                    marginLeft: Common.getLengthByIPhone7(32),
                }}>
                    <Text style={{
                        color: 'white',
                        fontFamily: 'Montserrat-Regular',
                        fontWeight: 'normal',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(8),
                        lineHeight: Common.getLengthByIPhone7(10),
                        opacity: 0.5,
                    }}>
                        {this.props.sub2}
                    </Text>
                    <Text style={{
                        marginTop: 2,
                        color: 'white',
                        fontFamily: 'Montserrat-Regular',
                        fontWeight: 'normal',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(10),
                        lineHeight: Common.getLengthByIPhone7(13),
                    }}>
                        1
                    </Text>
                </View>
            </View>
            <TouchableOpacity style={{
                marginTop: Common.getLengthByIPhone7(20),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
            }}
            onPress={() => {
                if (this.props.onClick) {
                    this.props.onClick();
                }
            }}>
                <Image
                source={require('./../../assets/ic-plus.png')}
                style={{
                    width: Common.getLengthByIPhone7(12),
                    height: Common.getLengthByIPhone7(12),
                    resizeMode: 'contain',
                }}/>
                <Text style={{
                    marginLeft: Common.getLengthByIPhone7(5),
                    color: 'white',
                    fontFamily: 'Montserrat-Regular',
                    fontWeight: 'normal',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(10),
                    lineHeight: Common.getLengthByIPhone7(13),
                }}>
                    {this.props.button}
                </Text>
            </TouchableOpacity>
        </View>
    </TouchableOpacity>);

// {!this.state.hidden ? (<Animated.View style={{
//     backgroundColor: Colors.grayC1,
//     position: 'absolute',
//     left: 0,
//     top: 0,
//     right: 0,
//     bottom: 0,
//     borderRadius: Common.getLengthByIPhone7(10),
//     opacity: this.opacity,
// }}/>) : null}
  }
}
