import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  ActivityIndicator,
  TouchableOpacity,
  StatusBar,
  Platform,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, {getSubscribtions, getDelays, getCase, getCompany, makeCaseUnread, makeCompanyUnread, renameCase, renameCompany, deleteSubscription, getNotifications} from './../../Utilites/Network';
import { observer } from 'mobx-react';
import ItemView from './ItemView';
import KeywordView from './KeywordView';
import { SvgXml } from 'react-native-svg';
import DraggableFlatList from 'react-native-draggable-flatlist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import TopButtonView from './../components/Main/TopButtonView';
import Story2ModalView from './../components/Story2ModalView';
import Dialog from "react-native-dialog";
import { isIphoneX } from 'react-native-iphone-x-helper';

@observer
export default class MainScrollView extends React.Component {

  timer = null;

  state = {
    loading: false,
    showStories: false,
    index: 1,
    visible: false,
    casename: '',
    caseId: 0,
    refreshList: false,
    rows: []
  };

  UNSAFE_componentWillMount() {
    AsyncStorage.getItem('subscriptions')
    .then(subscriptions => {
      if (subscriptions !== null && subscriptions !== undefined && subscriptions.length) {
        subscriptions = JSON.parse(subscriptions);
        if (subscriptions !== null) {
          Network.casesList = subscriptions.cases;
          Network.companiesList = subscriptions.companies;
          Network.keywordsList = subscriptions.keywords;

          this.getRows();
        }
      }
    })
    .catch(err => {

    });
    this._refresh();
    Network.refreshList = this._refresh;
    getNotifications();
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  _refresh = () => {
    let rows = [];

    if (this.state.rows.length === 0) {
      rows.push({type: 'loader'});
    } else {
      
    }

    this.setState({
      loading: this.state.rows.length ? false : true,
      rows,
    }, () => {

      if (this.state.rows.length === 0) {
        this.getRows();
      }

      getSubscribtions()
      .then(() => {
        let exist = false;
        for (let i=0;i<Network.casesList.length;i++) {
          if (Network.casesList[i].status === 'loading') {
            exist = true;
            break;
          }
        }

        if (exist) {
          if (this.timer === null) {
            this.timer = setInterval(() => {
              this._refresh();
            }, 30000);
          }
        } else {
          if (this.timer !== null && this.timer !== undefined) {
            clearInterval(this.timer);
          }
          this.timer = null;
        }

        this.getRows();
      })
      .catch(err => {
        this.setState({
          loading: false,
        });
      });
    });

    getDelays()
    .then(() => {

    })
    .catch(err => {
      
    });
  }

  getRows = () => {
    let array = [];
    let array2 = [];
    if (this.state.index === 0) {
      if (Network.companiesList.length) {
        if (Network.order !== null && Network.order.company !== null && Network.order.company !== undefined) {
          for (let i = 0; i < Network.order.company.length; i++) {
            for (let y = 0; y < Network.companiesList.length; y++) {
              if (Network.order.company[i] == Network.companiesList[y].id) {
                array.push(Network.companiesList[y]);
                break;
              }
            }
          }
          for (let y = 0; y < Network.companiesList.length; y++) {
            if (!Network.order.company.includes(Network.companiesList[y].id)) {
              array2.push(Network.companiesList[y]);
            }
          }
          array = array.concat(array2);
        } else {
          array = array.concat(Network.companiesList);
        }
      } else {
        array.push({type: 'empty'});
      }
      // array = [{type: 'empty'}];
      this.setState({
        rows: array,
        loading: false,
      });
    } else if (this.state.index === 1) {
      if (Network.casesList.length) {
        if (Network.order !== null && Network.order.case !== null && Network.order.case !== undefined) {
          for (let i = 0; i < Network.order.case.length; i++) {
            for (let y = 0; y < Network.casesList.length; y++) {
              if (Network.order.case[i] == Network.casesList[y].id) {
                array.push(Network.casesList[y]);
                break;
              }
            }
          }
          for (let y = 0; y < Network.casesList.length; y++) {
            if (!Network.order.case.includes(Network.casesList[y].id)) {
              array2.push(Network.casesList[y]);
            }
          }
          array = array.concat(array2);
        } else {
          array = array.concat(Network.casesList);
        }
      } else {
        array.push({type: 'empty'});
      }
      
      this.setState({
        rows: array,
        loading: false,
      });
    } else {
      if (Network.keywordsList.length) {
        if (Network.order !== null && Network.order.keyword !== null && Network.order.keyword !== undefined) {
          for (let i = 0; i < Network.order.keyword.length; i++) {
            for (let y = 0; y < Network.keywordsList.length; y++) {
              if (Network.order.keyword[i] == Network.keywordsList[y].id) {
                array.push(Network.keywordsList[y]);
                break;
              }
            }
          }
          for (let y = 0; y < Network.keywordsList.length; y++) {
            if (!Network.order.keyword.includes(Network.keywordsList[y].id)) {
              array2.push(Network.keywordsList[y]);
            }
          }
          array = array.concat(array2);
        } else {
          array = array.concat(Network.keywordsList);
        }
      } else {
        array.push({type: 'empty'});
      }
      
      this.setState({
        rows: array,
        loading: false,
      });
    }
  }

  renderItem = ({ item, index, drag, isActive }) => {
    if (item.type === 'loader') {
      return (<View style={{
        width: Common.getLengthByIPhone7(0),
        height: Common.getLengthByIPhone7(100),
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <ActivityIndicator />
      </View>);
    } else if (item.type === 'empty') {
      let xml = `<svg viewBox="0 0 79 86" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><linearGradient id="linearGradient-1-1" x1="38.8503086%" y1="0%" x2="61.1496914%" y2="100%"><stop stop-color="#FCFCFD" offset="0%"></stop><stop stop-color="#EEEFF3" offset="100%"></stop></linearGradient><linearGradient id="linearGradient-2-1" x1="0%" y1="9.5%" x2="100%" y2="90.5%"><stop stop-color="#FCFCFD" offset="0%"></stop><stop stop-color="#E9EBEF" offset="100%"></stop></linearGradient><rect id="path-3-1" x="0" y="0" width="17" height="36"></rect></defs><g id="Illustrations" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="B-type" transform="translate(-1268.000000, -535.000000)"><g id="Group-2" transform="translate(1268.000000, 535.000000)"><path id="Oval-Copy-2" d="M39.5,86 C61.3152476,86 79,83.9106622 79,81.3333333 C79,78.7560045 57.3152476,78 35.5,78 C13.6847524,78 0,78.7560045 0,81.3333333 C0,83.9106622 17.6847524,86 39.5,86 Z" fill="#F7F8FC"></path><polygon id="Rectangle-Copy-14" fill="#E5E7E9" transform="translate(27.500000, 51.500000) scale(1, -1) translate(-27.500000, -51.500000) " points="13 58 53 58 42 45 2 45"></polygon><g id="Group-Copy" transform="translate(34.500000, 31.500000) scale(-1, 1) rotate(-25.000000) translate(-34.500000, -31.500000) translate(7.000000, 10.000000)"><polygon id="Rectangle-Copy-10" fill="#E5E7E9" transform="translate(11.500000, 5.000000) scale(1, -1) translate(-11.500000, -5.000000) " points="2.84078316e-14 3 18 3 23 7 5 7"></polygon><polygon id="Rectangle-Copy-11" fill="#EDEEF2" points="-3.69149156e-15 7 38 7 38 43 -3.69149156e-15 43"></polygon><rect id="Rectangle-Copy-12" fill="url(#linearGradient-1-1)" transform="translate(46.500000, 25.000000) scale(-1, 1) translate(-46.500000, -25.000000) " x="38" y="7" width="17" height="36"></rect><polygon id="Rectangle-Copy-13" fill="#F8F9FB" transform="translate(39.500000, 3.500000) scale(-1, 1) translate(-39.500000, -3.500000) " points="24 7 41 7 55 -3.63806207e-12 38 -3.63806207e-12"></polygon></g><rect id="Rectangle-Copy-15" fill="url(#linearGradient-2-1)" x="13" y="45" width="40" height="36"></rect><g id="Rectangle-Copy-17" transform="translate(53.000000, 45.000000)"><mask id="mask-4-1" fill="white"><use xlink:href="#path-3-1"></use></mask><use id="Mask" fill="#E0E3E9" transform="translate(8.500000, 18.000000) scale(-1, 1) translate(-8.500000, -18.000000) " xlink:href="#path-3-1"></use><polygon id="Rectangle-Copy" fill="#D5D7DE" mask="url(#mask-4-1)" transform="translate(12.000000, 9.000000) scale(-1, 1) translate(-12.000000, -9.000000) " points="7 0 24 0 20 18 -1.70530257e-13 16"></polygon></g><polygon id="Rectangle-Copy-18" fill="#F8F9FB" transform="translate(66.000000, 51.500000) scale(-1, 1) translate(-66.000000, -51.500000) " points="62 45 79 45 70 58 53 58"></polygon></g></g></g></svg>`;
      return (<View style={{
        alignItems: 'center',
      }}>
        <SvgXml xml={xml} width={Common.getLengthByIPhone7(150)} height={Common.getLengthByIPhone7(150)} />
        <Text style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
          marginTop: Common.getLengthByIPhone7(10),
          color: Colors.textColor,
          fontFamily: 'Montserrat-Bold',
          fontWeight: this.state.index === 2 ? 'normal' : 'bold',
          textAlign: 'center',
          fontSize: this.state.index === 2 ? Common.getLengthByIPhone7(14) : Common.getLengthByIPhone7(18),
          lineHeight: Common.getLengthByIPhone7(22),
        }}>
          {this.state.index === 2 ? `У вас пока нет подписок на практику, добавьте ключевое слово или фразу и вы будете получать уведомления о новых судебных актах вышедших сегодня и содержащих упоминание введеной фразы.` : `Подписок нет`}
        </Text>
      </View>);
    } else {
      if (this.state.index === 2) {
        return (<KeywordView
          data={item}
          index2={index}
          index={this.state.index}
          navigation={this.props.navigation}
          key={index}
          isActive={isActive}
          onLongPress={drag}
          onChangeStatus={() => {
            // if (item.new > 0) {
            //   let rows = JSON.parse(JSON.stringify(this.state.rows));
            //   item.new = 0;
            //   rows[index] = item;
            //   this.setState({
            //     rows,
            //     refreshList: !this.state.refreshList,
            //   });
            //   if (this.state.index === 0) {
            //     getCompany(item.id)
            //     .then(() => {
            //       // this._refresh();
            //     })
            //     .catch(err => {
  
            //     });
            //   } else if (this.state.index === 1) {
            //     getCase(item.id)
            //     .then(() => {
            //       // this._refresh();
            //     })
            //     .catch(err => {
  
            //     });
            //   }
            // } else {
            //   let rows = JSON.parse(JSON.stringify(this.state.rows));
            //   item.new = 1;
            //   rows[index] = item;
            //   this.setState({
            //     rows,
            //     refreshList: !this.state.refreshList,
            //   });
            //   if (this.state.index === 0) {
            //     makeCompanyUnread(item.id)
            //     .then(() => {
            //       // this._refresh();
            //     })
            //     .catch(err => {
  
            //     });
            //   } else if (this.state.index === 1) {
            //     makeCaseUnread(item.id)
            //     .then(() => {
            //       // this._refresh();
            //     })
            //     .catch(err => {
  
            //     });
            //   }
            // }
          }}
          onEditCase={caseItem => {
            // if (this.state.index === 1) {
            //   this.setState({
            //     casename: caseItem.name,
            //     caseId: caseItem.id,
            //     visible: true,
            //   });
            // } else if (this.state.index === 0) {
            //   this.setState({
            //     casename: caseItem.name_custom !== null ? caseItem.name_custom : caseItem.name,
            //     caseId: caseItem.id,
            //     visible: true,
            //   });
            // }
          }}
          onDelete={item2 => {
            Alert.alert(
              Config.appName,
              "Вы хотите удалить?",
              [
                {
                  text: "Нет",
                  onPress: () => console.log("Cancel Pressed"),
                  style: "cancel"
                },
                { text: "Да", onPress: () => {
                  deleteSubscription(item2.id, 'keyword')
                  .then(() => {
                    this._refresh();
                  })
                  .catch(err => {
                    this._refresh();
                    Alert.alert(Config.appName, err);
                  });
                }}
              ]
            );
          }}
        />);
      } else {
        return (<ItemView
          data={item}
          index2={index}
          index={this.state.index}
          navigation={this.props.navigation}
          key={index}
          isActive={isActive}
          onLongPress={drag}
          onChangeStatus={() => {
            if (item.new > 0) {
              let rows = JSON.parse(JSON.stringify(this.state.rows));
              item.new = 0;
              rows[index] = item;
              this.setState({
                rows,
                refreshList: !this.state.refreshList,
              });
              if (this.state.index === 0) {
                getCompany(item.id)
                .then(() => {
                  // this._refresh();
                })
                .catch(err => {
  
                });
              } else if (this.state.index === 1) {
                getCase(item.id)
                .then(() => {
                  // this._refresh();
                })
                .catch(err => {
  
                });
              }
            } else {
              let rows = JSON.parse(JSON.stringify(this.state.rows));
              item.new = 1;
              rows[index] = item;
              this.setState({
                rows,
                refreshList: !this.state.refreshList,
              });
              if (this.state.index === 0) {
                makeCompanyUnread(item.id)
                .then(() => {
                  // this._refresh();
                })
                .catch(err => {
  
                });
              } else if (this.state.index === 1) {
                makeCaseUnread(item.id)
                .then(() => {
                  // this._refresh();
                })
                .catch(err => {
  
                });
              }
            }
          }}
          onEditCase={caseItem => {
            if (this.state.index === 1) {
              this.setState({
                casename: caseItem.name,
                caseId: caseItem.id,
                visible: true,
              });
            } else if (this.state.index === 0) {
              this.setState({
                casename: caseItem.name_custom !== null ? caseItem.name_custom : caseItem.name,
                caseId: caseItem.id,
                visible: true,
              });
            }
          }}
          onDelete={item2 => {
            Alert.alert(
              Config.appName,
              "Вы хотите удалить?",
              [
                {
                  text: "Нет",
                  onPress: () => console.log("Cancel Pressed"),
                  style: "cancel"
                },
                { text: "Да", onPress: () => {
                  deleteSubscription(item2.id, this.state.index === 0 ? 'company' : (this.state.index === 1 ? 'case' : 'keyword'))
                  .then(() => {
                    this._refresh();
                  })
                  .catch(err => {
                    this._refresh();
                    Alert.alert(Config.appName, err);
                  });
                }}
              ]
            );
          }}
        />);
      }
    }
  }

  renderTab = (letter, title, index) => {
    return (<TouchableOpacity style={{
      width: Common.getLengthByIPhone7(114),
      height: Common.getLengthByIPhone7(34),
      borderRadius: Common.getLengthByIPhone7(10),
      backgroundColor: (this.state.index === index ? Colors.mainColor : 'transparent'),
      alignItems: 'center',
      justifyContent: 'center', 
    }}
    onPress={() => {
      this.setState({
        index: index,
      }, () => {
        this._refresh();
      });
    }}>
      <Text style={{
        color: (this.state.index === index ? Colors.orangeColor : Colors.textColor),
        fontFamily: 'Montserrat-Regular',
        fontWeight: (this.state.index === index ? '600' : 'normal'),
        textAlign: 'left',
        fontSize: Common.getLengthByIPhone7(12),
        lineHeight: Common.getLengthByIPhone7(15),
      }}>
        {letter}<Text style={{
        color: (this.state.index === index ? 'white' : Colors.textColor),
        fontFamily: 'Montserrat-Regular',
        fontWeight: (this.state.index === index ? '600' : 'normal'),
        textAlign: 'left',
        fontSize: Common.getLengthByIPhone7(12),
        lineHeight: Common.getLengthByIPhone7(15),
      }}>
        {title}
      </Text>
      </Text>
    </TouchableOpacity>);
  }

  render() {

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        marginTop: Common.getLengthByIPhone7(20),
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}>
        <View style={{
          width: Common.getLengthByIPhone7(0),
          alignItems: 'center'
        }}>
          <View style={{
            marginTop: Common.getLengthByIPhone7(90),
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            height: Common.getLengthByIPhone7(44),
            borderRadius: Common.getLengthByIPhone7(10),
            // borderWidth: 1,
            backgroundColor: 'rgba(50, 38, 97, 0.03)',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingLeft: Common.getLengthByIPhone7(5),
            paddingRight: Common.getLengthByIPhone7(5),
          }}>
            {this.renderTab('Д', 'ела', 1)}
            {this.renderTab('К', 'омпании', 0)}
            {this.renderTab('П', 'рактика', 2)}
          </View>
          <View style={{
            width: Common.getLengthByIPhone7(0),
            flexDirection: 'row',
            alignItems: 'center',
            // justifyContent: 'space-between',
            justifyContent: 'flex-start',
            marginBottom: Common.getLengthByIPhone7(25),
          }}>
            <TouchableOpacity style={{
              // width: Common.getLengthByIPhone7(0),
              marginTop: Common.getLengthByIPhone7(20),
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingLeft: Common.getLengthByIPhone7(16),
              // paddingRight: Common.getLengthByIPhone7(16),
            }}
            onPress={() => {
              if (this.props.onClick) {
                this.props.onClick(this.state.index === 0 ? 'company' : (this.state.index === 1 ? 'case' : 'keyword'));
              }
            }}>
              <Image
                source={require('./../../assets/ic-add2.png')}
                style={{
                    width: Common.getLengthByIPhone7(17),
                    height: Common.getLengthByIPhone7(17),
                    resizeMode: 'contain',
                    tintColor: Colors.textColor,
                }}
              />
              <Text style={{
                marginLeft: Common.getLengthByIPhone7(5),
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(13),
                lineHeight: Common.getLengthByIPhone7(16),
                // letterSpacing: -0.55,
              }}>
                Добавить {this.state.index === 0 ? 'компанию' : (this.state.index === 1 ? 'дело' : 'фразу')}
              </Text>
            </TouchableOpacity>
            {this.state.index !== 1 ? null : (<TouchableOpacity style={{
              // width: Common.getLengthByIPhone7(0),
              marginTop: Common.getLengthByIPhone7(20),
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-start',
              paddingLeft: Common.getLengthByIPhone7(16),
              // paddingRight: Common.getLengthByIPhone7(16),
            }}
            onPress={() => {
              if (this.props.onCalendarShow) {
                this.props.onCalendarShow();
              }
            }}>
              <Image
                source={require('./../../assets/ic-calendar2.png')}
                style={{
                    width: Common.getLengthByIPhone7(17),
                    height: Common.getLengthByIPhone7(17),
                    resizeMode: 'contain',
                    tintColor: Colors.textColor,
                }}
              />
              <Text style={{
                marginLeft: Common.getLengthByIPhone7(5),
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(13),
                lineHeight: Common.getLengthByIPhone7(16),
                // letterSpacing: -0.55,
              }}>
                Календарь
              </Text>
            </TouchableOpacity>)}
            {this.state.index !== 1 ? null : (<TouchableOpacity style={{
              // width: Common.getLengthByIPhone7(0),
              marginTop: Common.getLengthByIPhone7(20),
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-start',
              paddingLeft: Common.getLengthByIPhone7(16),
              // paddingRight: Common.getLengthByIPhone7(16),
            }}
            onPress={() => {
              if (this.props.onDelayShow) {
                this.props.onDelayShow();
              }
            }}>
              <Image
                source={require('./../../assets/ic-delay.png')}
                style={{
                    width: Common.getLengthByIPhone7(17),
                    height: Common.getLengthByIPhone7(17),
                    resizeMode: 'contain',
                    tintColor: Colors.textColor,
                }}
              />
              <Text style={{
                marginLeft: Common.getLengthByIPhone7(5),
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(13),
                lineHeight: Common.getLengthByIPhone7(16),
                // letterSpacing: -0.55,
              }}>
                Задержки
              </Text>
            </TouchableOpacity>)}
          </View>
        </View>
        <DraggableFlatList
          style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
            marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? 86 : 52) : 52,
          }}
          // debug={true}
          dragItemOverflow={true}
          data={this.state.rows}
          extraData={this.state.refreshList}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => `draggable-item-${index}`}
          autoscrollThreshold={1}
          onDragEnd={({ data }) => {
            // console.warn(data);
            let order = {};
            if (Network.order !== null) {
              order = JSON.parse(JSON.stringify(Network.order));
            }
            let array = [];
            let array2 = [];
            let array3 = [];
            for (let i = 0; i < data.length; i++) {
              if (data[i].type === 'top') {
                array2[0] = data[i];
              } else if (data[i].type === 'header') {
                array2[1] = data[i];
              } else if (data[i].type === 'tabs') {
                array2[2] = data[i];
              } else if (data[i].type === 'buttons') {
                array2[3] = data[i];
              } else {
                array.push(data[i].id);
                array3.push(data[i]);
              }
            }
            array2 = array2.concat(array3);
            if (this.state.index === 0) {
              order.company = array;
            } else {
              order.case = array;
            }
            Network.order = order;
            AsyncStorage.setItem('order', JSON.stringify(order));
            this.setState({rows: array2})
          }}
        />
        {this.state.storiesShow ? (<Story2ModalView
          show={this.state.storiesShow}
          navigation={this.props.navigation}
          stories={Network.clipsList}
          index={this.state.storyIndex}
          onClose={() => {
            this.setState({
              storiesShow: false,
            });
            StatusBar.setHidden(false);
          }}
        />) : null}
        <Dialog.Container visible={this.state.visible}>
          <Dialog.Title>{this.state.index === 1 ? 'Переименовать дело' : 'Переименовать компанию'}</Dialog.Title>
          <Dialog.Input value={this.state.casename} onChangeText={text => this.setState({casename : text})} />
          <Dialog.Button label="Отмена" onPress={() => {
              this.setState({
                  visible: false,
              });
          }} />
          <Dialog.Button label="Сохранить" onPress={() => {
              this.setState({
                  loading: true,
              }, () => {
                  if (this.state.casename.length) {
                    if (this.state.index === 1) {
                      renameCase(this.state.caseId, this.state.casename)
                      .then(data => {
                          this.setState({
                              loading: false,
                              visible: false,
                          }, () => {
                            if (Network.refreshList) {
                                Network.refreshList();
                            }
                          });
                      })
                      .catch(err => {
                          this.setState({
                              loading: false,
                          }, () => {
                              setTimeout(() => {
                                  Alert.alert(Config.appName, err);
                              }, 100);
                          });
                      });
                    } else {
                      renameCompany(this.state.caseId, this.state.casename)
                      .then(data => {
                          this.setState({
                              loading: false,
                              visible: false,
                          }, () => {
                            if (Network.refreshList) {
                                Network.refreshList();
                            }
                          });
                      })
                      .catch(err => {
                          this.setState({
                              loading: false,
                          }, () => {
                              setTimeout(() => {
                                  Alert.alert(Config.appName, err);
                              }, 100);
                          });
                      });
                    }
                  } else {
                      Alert.alert(Config.appName, 'Укажите название дела!');
                  }
              });
            }} />
        </Dialog.Container>
      </View>);
  }
}
