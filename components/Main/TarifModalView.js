import React from 'react';
import {
  Platform,
  Text,
  View,
  Image,
  Alert,
  Animated,
  Easing,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  ActivityIndicator,
  Linking,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, { validateReceipt } from './../../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import Modal from 'react-native-modal';
import RNIap, {
	purchaseErrorListener,
	purchaseUpdatedListener,
	acknowledgePurchaseAndroid,
	consumePurchaseAndroid,
	type ProductPurchase,
	type PurchaseError
} from 'react-native-iap';

const itemSkus = Platform.select({
    ios: [
      'lt.prof1',
      'lt.prof6',
      'lt.prof12',
    ],
    android: [
      'lt.prof1',
      'lt.prof6',
      'lt.prof12',
    ]
})

@observer
export default class TarifModalView extends React.Component {
    
	spinValue = new Animated.Value(0);

  state = {
    refresh: false,
    show: false,
	index: 1,
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
    });

    RNIap.initConnection()
    .then(() => {
      console.warn('initConnection');

      if (Platform.OS === 'android') {
        RNIap.flushFailedPurchasesCachedAsPendingAndroid()
        .then(() => {

        })
        .catch(err => {
          
        });
      } else {
        RNIap.clearTransactionIOS()
        .then(() => {

        })
        .catch(err => {

        });
      }

      RNIap.getProducts(itemSkus).then(products => {
        //handle success of fetch product list
        console.warn('products: '+JSON.stringify(products));
      }).catch((error) => {
        console.warn(error);
      });
    })
    .catch(err => {
      console.warn('initConnection err: '+err);
    });

    this.purchaseUpdateSubscription = purchaseUpdatedListener((purchase: InAppPurchase | SubscriptionPurchase | ProductPurchase ) => {
      console.warn('purchaseUpdatedListener', purchase);

      // {
      //   transactionReceipt: "base64",
      //   transactionDate: "timestamp",
      //   productId: 'akashi.chakri300',
      //   transactionId: '1012339383489434',
      // }

      const receipt = purchase.transactionReceipt;
      console.warn('receipt '+receipt);
      if (receipt) {
        RNIap.finishTransaction(purchase, true)
        .then(result => {
          console.warn('finishTransaction success: '+JSON.stringify(result));
        })
        .catch(err => {
            console.warn('finishTransaction err: '+JSON.stringify(err));
        });

        this.setState({
          loading: true,
        }, () => {
          validateReceipt(receipt, Platform.OS === 'ios' ? 'appstore' : 'googleplay', purchase.productId)
          .then(id => {
            this.setState({
              loading: false,
            }, () => {
              if (this.props.onSuccess) {
                this.props.onSuccess(id);
              }
            });
          })
          .catch(err => {
            this.setState({
              loading: false,
            }, () => {
              setTimeout(() => {
                Alert.alert(Config.appName, err);
              }, 100);
            });
          });
        });
      }
    });

    this.purchaseErrorSubscription = purchaseErrorListener((error: PurchaseError) => {
      console.warn('purchaseErrorListener', error);

      this.setState({
          loading: false,
      }, () => {
        if (error.code == "E_ALREADY_OWNED") {
          // setTimeout(() => {
          //   Alert.alert(Config.appName, 'Произошла ошибка! Повторите операцию снова.');
          // }, 100);
        } else if (error.code == "E_USER_CANCELLED") {
  
        }
      });
    });
  }

  UNSAFE_componentWillUnmount() {
    if (this.purchaseUpdateSubscription) {
      this.purchaseUpdateSubscription.remove();
      this.purchaseUpdateSubscription = null;
    }
    if (this.purchaseErrorSubscription) {
      this.purchaseErrorSubscription.remove();
      this.purchaseErrorSubscription = null;
    }
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      show: props.show,
    });
  }

  constructor(props) {
    super(props);

  }

  renderButton = (letter, word, index, action) => {
	  return (<TouchableOpacity style={{
		height: Common.getLengthByIPhone7(34),
		borderRadius: Common.getLengthByIPhone7(10),
		// paddingLeft: Common.getLengthByIPhone7(14),
		// paddingRight: Common.getLengthByIPhone7(14),
		width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30))/2,
		backgroundColor: index === this.state.index ? Colors.mainColor : 'transparent',
		alignItems: 'center',
		justifyContent: 'center',
	}}
	onPress={() => {
		if (action) {
			action();
		}
	}}>
		<Text style={{
			color: index === this.state.index ? Colors.orangeColor : Colors.textColor,
			fontSize: Common.getLengthByIPhone7(10),
			lineHeight: Common.getLengthByIPhone7(12),
			fontFamily: 'Montserrat-Regular',
			textAlign: 'center',
			fontWeight: index === this.state.index ? '600' : '500',
		}}
		allowFontScaling={false}>
			{letter}<Text style={{
				color: index === this.state.index ? 'white' : Colors.textColor,
				fontSize: Common.getLengthByIPhone7(10),
				lineHeight: Common.getLengthByIPhone7(12),
				fontFamily: 'Montserrat-Regular',
				textAlign: 'center',
				fontWeight: index === this.state.index ? '600' : '500',
			}}
			allowFontScaling={false}>
				{word}
			</Text>
		</Text>
	</TouchableOpacity>);
  }

  renderTarif = (title, subtitle1, subtitle2, button, style, action) => {
	  return (<View style={[{
		  width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
		  height: Common.getLengthByIPhone7(190),
		  borderRadius: Common.getLengthByIPhone7(12),
		  backgroundColor: '#E8EBF2',
		  alignItems: 'center',
		  justifyContent: 'center',
	  }, style]}>
	  	<Text style={{
			color: 'black',
			fontSize: Common.getLengthByIPhone7(18),
			lineHeight: Common.getLengthByIPhone7(22),
			fontFamily: 'Montserrat-Regular',
			textAlign: 'center',
			fontWeight: 'normal',
		}}
		allowFontScaling={false}>
			{title}
		</Text>
		{subtitle1 !== null ? (<Text style={{
			color: 'black',
			fontSize: Common.getLengthByIPhone7(18),
			lineHeight: Common.getLengthByIPhone7(22),
			fontFamily: 'Montserrat-Bold',
			textAlign: 'center',
			fontWeight: 'bold',
		}}
		allowFontScaling={false}>
			{subtitle1}
		</Text>) : null}
		{subtitle1 !== null ? (<Text style={{
			color: 'black',
			fontSize: Common.getLengthByIPhone7(10),
			// lineHeight: Common.getLengthByIPhone7(22),
			fontFamily: 'Montserrat-Regular',
			textAlign: 'center',
			fontWeight: 'normal',
		}}
		allowFontScaling={false}>
			{subtitle2}
		</Text>) : null}
	  	<TouchableOpacity style={{
			marginTop: Common.getLengthByIPhone7(16),
			width: Common.getLengthByIPhone7(150),
			height: Common.getLengthByIPhone7(35),
			borderRadius: Common.getLengthByIPhone7(4),
			backgroundColor: '#F6895E',
			alignItems: 'center',
			justifyContent: 'center',
		}}
		onPress={() => {
			if (action) {
				action();
			}
		}}>
			<Text style={{
				color: 'white',
				fontSize: Common.getLengthByIPhone7(16),
				// lineHeight: Common.getLengthByIPhone7(22),
				fontFamily: 'Montserrat-Regular',
				textAlign: 'center',
				fontWeight: 'normal',
			}}
			allowFontScaling={false}>
				{button}
			</Text>
		</TouchableOpacity>
	  </View>);
  }

  render() {
    
	let rows = [];

	// if (this.state.index === 0) {
	// 	rows.push(this.renderTarif('Без уведомлений', null, null, 'Перейти', {}, () => {

	// 	}));
	// } else if (this.state.index === 1) {
		rows.push(this.renderTarif('На 1 месяц', '1 490 p', null, 'Перейти', {}, () => {
      this.setState({
        loading: true,
      }, () => {
        RNIap.requestPurchase('lt.prof1', false)
        .then(() => {
          console.warn('RNIap.requestPurchase');
        })
        .catch(err => {
          console.warn('err requestPurchase: '+err);
          this.setState({
            loading: false,
          }, () => {
            // setTimeout(() => {
            //   Alert.alert(Config.appName, 'Произошла ошибка! Повторите операцию снова.');
            // }, 100);
          });
        });
      });
		}));
		rows.push(this.renderTarif('На 6 месяцев', '6 490 p', 'экономия 2 690 рублей', 'Перейти', {
			marginTop: Common.getLengthByIPhone7(16),
		}, () => {
      this.setState({
        loading: true,
      }, () => {
        RNIap.requestPurchase('lt.prof6', false)
        .then(() => {
          console.warn('RNIap.requestPurchase');
          this.setState({
            loading: false,
          });
        })
        .catch(err => {
          console.warn('err requestPurchase: '+err);
          this.setState({
            loading: false,
          }, () => {
            // setTimeout(() => {
            //   Alert.alert(Config.appName, 'Произошла ошибка! Повторите операцию снова.');
            // }, 100);
          });
        });
      });
		}));
		rows.push(this.renderTarif('На 1 год', '8 990 p', 'экономия 8 940 рублей', 'Перейти', {
			marginTop: Common.getLengthByIPhone7(16),
			marginBottom: Common.getLengthByIPhone7(16),
		}, () => {
      this.setState({
        loading: true,
      }, () => {
        RNIap.requestPurchase('lt.prof12', false)
        .then(() => {
          console.warn('RNIap.requestPurchase');
        })
        .catch(err => {
          console.warn('err requestPurchase: '+err);
          this.setState({
            loading: false,
          }, () => {
            // setTimeout(() => {
            //   Alert.alert(Config.appName, 'Произошла ошибка! Повторите операцию снова.');
            // }, 100);
          });
        });
      });
		}));
	// } else if (this.state.index === 2) {
	// 	rows.push(this.renderTarif('На 1 год', '7 990 p', 'за 1 сотрудника', 'Заказать счет', {}, () => {

	// 	}));
	// 	rows.push(this.renderTarif('На 1 год', '44 990 p', 'за 10 сотрудников', 'Заказать счет', {
	// 		marginTop: Common.getLengthByIPhone7(16),
	// 	}, () => {

	// 	}));
	// 	rows.push(this.renderTarif('На 1 год', '74 990 p', 'безлимит', 'Заказать счет', {
	// 		marginTop: Common.getLengthByIPhone7(16),
	// 		marginBottom: Common.getLengthByIPhone7(16),
	// 	}, () => {

	// 	}));
	// }
    return (<View style={{
        // flex: 1,
      }}>
        <Modal isVisible={this.state.show}>
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
                <TouchableOpacity style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'transparent',
                }}
                onPress={() => {
                    this.setState({
                        text: '',
                        companies: [],
                    }, () => {
                        
                    });
                    if (this.props.onClose) {
                        this.props.onClose();
                    }
                }}
                />
                <View style={{
                    width: Common.getLengthByIPhone7(0),
                    height: Dimensions.get('window').height - Common.getLengthByIPhone7(60),
                    backgroundColor: 'white',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    marginBottom: -20,
                }}>
                    <View style={{
						width: Common.getLengthByIPhone7(0),
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'space-between',

					}}>
						<Text style={{
              marginLeft: Common.getLengthByIPhone7(20),
              color: Colors.orangeColor,
              fontSize: Common.getLengthByIPhone7(22),
              lineHeight: Common.getLengthByIPhone7(27),
              letterSpacing: -0.55,
              fontFamily: 'Montserrat-Regular',
              textAlign: 'center',
							fontWeight: '700',
            }}
            allowFontScaling={false}>
              Т<Text style={{
              color: Colors.textColor,
              fontSize: Common.getLengthByIPhone7(22),
              lineHeight: Common.getLengthByIPhone7(27),
              letterSpacing: -0.55,
              fontFamily: 'Montserrat-Regular',
              textAlign: 'center',
              fontWeight: '700',
            }}
            allowFontScaling={false}>
                арифы
              </Text>
            </Text>
						<TouchableOpacity style={{

						}}
						onPress={() => {
							if (this.props.onClose) {
								this.props.onClose();
							}
						}}>
							<Image
								source={require('./../../assets/ic-close3.png')}
								style={{
									width: Common.getLengthByIPhone7(66),
									height: Common.getLengthByIPhone7(66),
									resizeMode: 'contain',
								}}
							/>
						</TouchableOpacity>
					</View>
					<ScrollView style={{
						width: Common.getLengthByIPhone7(0),
						marginTop: Common.getLengthByIPhone7(10),
						flex: 1,
					}}
					contentContainerStyle={{
						alignItems: 'center',
					}}>
						{rows}
            <Text style={{
              width: Common.getLengthByIPhone7(300),
              color: Colors.textColor,
              fontSize: Common.getLengthByIPhone7(16),
              lineHeight: Common.getLengthByIPhone7(22),
              letterSpacing: -0.55,
              fontFamily: 'Montserrat-Regular',
              textAlign: 'center',
              fontWeight: '700',
              marginBottom: Common.getLengthByIPhone7(20),
            }}
            allowFontScaling={false}>
              Тариф для корпоративных клиентов предоставляем по запросу в чате приложения или в письме нам на почту <Text style={{
                width: Common.getLengthByIPhone7(300),
                color: Colors.textColor,
                fontSize: Common.getLengthByIPhone7(16),
                lineHeight: Common.getLengthByIPhone7(22),
                letterSpacing: -0.55,
                fontFamily: 'Montserrat-Regular',
                textAlign: 'center',
                fontWeight: '700',
                textDecorationLine: 'underline',
              }}
              onPress={() => {
                Linking.openURL('mailto:info@legaltrack.ru')
              }}
              allowFontScaling={false}>
                info@legaltrack.ru
              </Text>
            </Text>
					</ScrollView>
                </View>
            </View>
        </Modal>
        <Spinner
          visible={this.state.loading}
          // textContent={'Загрузка...'}
          onShow={() => {
            Animated.loop(
              Animated.timing(
                this.spinValue,
                {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear,
                useNativeDriver: true
                }
              )
            ).start();
          }}
          onDismiss={() => {

          }}
          customIndicator={<View style={{
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 20000,
          }}>
            <View style={{
              width: Common.getLengthByIPhone7(65),
              height: Common.getLengthByIPhone7(65),
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <Animated.Image
                style={{
                  width: Common.getLengthByIPhone7(65),
                  height: Common.getLengthByIPhone7(65),
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  transform: [{rotate: this.spinValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '360deg']
                  })}] 
                }}
                source={require('./../assets/ic-loader.png')}
              />
              <TouchableOpacity style={{
                // marginTop: Common.getLengthByIPhone7(20),
                padding: Common.getLengthByIPhone7(5),
                backgroundColor: 'rgba(255, 255, 255, 0.8)',
                borderRadius: Common.getLengthByIPhone7(5),
              }}
              onPress={() => {
                this.setState({
                  loading: false,
                });
              }}>
                <Image
                  style={{
                    width: Common.getLengthByIPhone7(18),
                    height: Common.getLengthByIPhone7(18),
                  }}
                  source={require('./../assets/ic-loader-close.png')}
                />
              </TouchableOpacity>
            </View>
          </View>}
          overlayColor={'rgba(32, 42, 91, 0.3)'}
          textStyle={{color: '#FFF'}}
        />
      </View>);

// {this.renderButton('Б', 'есплатный', 0, () => {
// 	this.setState({
// 		index: 0,
// 	});
// })}
  }
}
