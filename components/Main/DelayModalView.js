import React from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
  TextInput,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';
import Colors from './../../constants/Colors';
import Common from './../../Utilites/Common';
import Network, {searchDelays} from './../../Utilites/Network';
import { observer } from 'mobx-react';
import Modal from 'react-native-modal';

@observer
export default class DelayModalView extends React.Component {
    
  state = {
    show: false,
	rows: [],
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
		rows: Network.delayList,
      });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      show: props.show,
	  rows: Network.delayList,
    });
  }

  constructor(props) {
    super(props);

  }

  renderRow = (item, index) => {
	console.warn(item);
	let time = new Date(item.datetime_start);
	let hh = time.getHours();
	let str2 = '';
	if (isNaN(hh)) {
		str2 = item.datetime_start;
	} else {
		hh = hh < 10 ? '0' + hh : hh;

		let mm = time.getMinutes();
		mm = mm < 10 ? '0' + mm : mm;
		str2 = hh + ':' + mm;
	}

	let str = '';
	let t1 = ((new Date()).getTime() - (new Date(item.delay_update)).getTime()) / 1000;
	if (t1 < 60) {
		str = 'обновлено ' + t1 + `\n секунд назад`;
	} else if (t1 < 60*60) {
		let t = (t1 - (t1 % 60)) / 60;
		str = 'обновлено ' + t + `\n минут назад`;
	} else {
		var sec_num = parseInt(t1, 10)
		var hours   = Math.floor(sec_num / 3600)
		var minutes = Math.floor(sec_num / 60) % 60

		str = [hours,minutes]
			.map(v => v < 10 ? "0" + v : v)
			.filter((v,i) => v !== "00" || i > 0)
			.join(":");
		str = 'обновлено ' + str + `\n назад`;
	}

    return (<TouchableOpacity style={{
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(16),
        paddingTop: Common.getLengthByIPhone7(5),
        paddingBottom: Common.getLengthByIPhone7(10),
        borderBottomWidth: 1,
        borderBottomColor: '#E8E8E8',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    }}
    onPress={() => {
		if (item.id) {
			item.id = item.case_id;
			if (this.props.onOpen) {
				this.props.onOpen(item);
			}
		}
    }}>
		<View style={{
			flexDirection: 'row',
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(16),
			alignItems: 'flex-start',
			justifyContent: 'space-between',
		}}>
			<View style={{
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'flex-start',
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(9),
					height: Common.getLengthByIPhone7(9),
					borderRadius: Common.getLengthByIPhone7(9)/2,
					backgroundColor: Colors.orangeColor,
				}}/>
				<Text style={{
					marginLeft: Common.getLengthByIPhone7(7),
					color: Colors.textColor,
					fontSize: Common.getLengthByIPhone7(16),
					letterSpacing: -0.55,
					fontFamily: 'Montserrat-Semibold',
					fontWeight: '600',
					textAlign: 'left',
				}}
				allowFontScaling={false}>
					{str2}
				</Text>
				<Text style={{
					color: Colors.textColor,
					fontSize: Common.getLengthByIPhone7(16),
					letterSpacing: -0.55,
					fontFamily: 'Montserrat-Regular',
					fontWeight: 'normal',
					textAlign: 'left',
				}}
				allowFontScaling={false}>
					{` `}-{` `}
				</Text>
				<Text style={{
					width: Common.getLengthByIPhone7(210),
					color: Colors.textColor,
					fontSize: Common.getLengthByIPhone7(16),
					letterSpacing: -0.55,
					fontFamily: 'Montserrat-Regular',
					fontWeight: 'normal',
					textAlign: 'left',
				}}
				allowFontScaling={false}>
					{item.head}
				</Text>
			</View>
			<Text style={{
				color: '#BDBDBD',
				fontSize: Common.getLengthByIPhone7(9),
				letterSpacing: -0.55,
				fontFamily: 'Montserrat-Regular',
				fontWeight: 'normal',
				textAlign: 'left',
			}}
			allowFontScaling={false}>
				{str}
			</Text>
		</View>
        <Text style={{
			marginTop: Common.getLengthByIPhone7(7),
			color: Colors.textColor,
			fontSize: Common.getLengthByIPhone7(12),
			lineHeight: Common.getLengthByIPhone7(15),
			fontFamily: 'Montserrat-Bold',
			fontWeight: 'bold',
			textAlign: 'left',
		}}
		allowFontScaling={false}>
			{item.second_line}
		</Text>
		<Text style={{
			color: '#717275',
			fontSize: Common.getLengthByIPhone7(12),
			lineHeight: Common.getLengthByIPhone7(15),
			fontFamily: 'Montserrat-Regular',
			fontWeight: 'normal',
			textAlign: 'left',
		}}
		allowFontScaling={false}>
			{item.delay_text}
		</Text>
    </TouchableOpacity>);
  }

  renderEmpty = () => {
	  return (<View style={{
		  width: Common.getLengthByIPhone7(0),
		  flex: 1,
		  alignItems: 'center',
		  justifyContent: 'center',
	  }}>
	  	<Image
			source={require('./../../assets/ic-delay-empty.png')}
			style={{
				width: Common.getLengthByIPhone7(150),
				height: Common.getLengthByIPhone7(150),
				resizeMode: 'contain',
			}}
		/>
		<Text style={{
			marginTop: Common.getLengthByIPhone7(28),
			color: Colors.textColor,
			fontSize: Common.getLengthByIPhone7(18),
			lineHeight: Common.getLengthByIPhone7(22),
			fontFamily: 'Montserrat-Bold',
			textAlign: 'center',
			fontWeight: 'bold',
		}}
		allowFontScaling={false}>
			Заседаний нет
		</Text>
		<Text style={{
			marginTop: Common.getLengthByIPhone7(13),
			color: Colors.textColor,
			fontSize: Common.getLengthByIPhone7(14),
			lineHeight: Common.getLengthByIPhone7(17),
			fontFamily: 'Montserrat-Regular',
			textAlign: 'center',
			fontWeight: 'normal',
		}}
		allowFontScaling={false}>
			{`Мы не нашли судебных\nзаседаний в пределах 3х часов`}
		</Text>
	  </View>);
  }

	renderList = () => {
		return (<View style={{
			width: Common.getLengthByIPhone7(0),
			flex: 1,
			alignItems: 'center',
			justifyContent: 'center',
		}}>
			<FlatList
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					marginBottom: Common.getLengthByIPhone7(30),
					// backgroundColor: 'red',
				}}
				contentContainerStyle={{
					justifyContent: 'flex-start',
					alignItems: 'center',
				}}
				horizontal={false}
				numColumns={1}
				data={this.state.rows}
				extraData={this.state.rows}
				renderItem={({item, index}) => this.renderRow(item, index)}
				keyExtractor={category => category.id}
			/>
		</View>);
	}

  render() {

    return (
        <Modal
			isVisible={this.state.show}
			avoidKeyboard={false}
		>
				<TouchableOpacity style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'transparent',
                }}
                onPress={() => {
                    this.setState({
                        text: '',
                        companies: [],
                    }, () => {
                        
                    });
                    if (this.props.onClose) {
                        this.props.onClose();
                    }
                }}
                />
                <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "padding" : undefined}
					// keyboardVerticalOffset={-1750}
                    style={{
                        height: Dimensions.get('window').height - Common.getLengthByIPhone7(60),
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                    }}
                >
				<View style={{
					width: Common.getLengthByIPhone7(0),
					height: Dimensions.get('window').height - Common.getLengthByIPhone7(60),
					backgroundColor: 'white',
					alignItems: 'center',
					justifyContent: 'flex-end',
					marginBottom: -20,
				}}>
					<View style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
						alignItems: 'flex-start',
					}}>
						<View style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
							flexDirection: 'row',
							alignItems: 'center',
							justifyContent: 'space-between',
							marginTop: Common.getLengthByIPhone7(40),
						}}>
							<TouchableOpacity style={{
								width: Common.getLengthByIPhone7(30),
								height: Common.getLengthByIPhone7(30),
								alignItems: 'flex-start',
								justifyContent: 'flex-start',
							}}
							onPress={() => {
								this.setState({
									text: '',
									companies: [],
								}, () => {
									
								});
								if (this.props.onClose) {
									this.props.onClose();
								}
							}}>
								<Image
									source={require('./../../assets/ic-close.png')}
									style={{
										width: Common.getLengthByIPhone7(12),
										height: Common.getLengthByIPhone7(12),
										resizeMode: 'contain',
									}}
								/>
							</TouchableOpacity>
						</View>
						<Text style={{
							marginTop: Common.getLengthByIPhone7(10),
							color: Colors.orangeColor,
							fontSize: Common.getLengthByIPhone7(24),
							lineHeight: Common.getLengthByIPhone7(27),
							letterSpacing: -0.55,
							fontFamily: 'Montserrat-Bold',
							textAlign: 'center',
							fontWeight: 'bold',
						}}
						allowFontScaling={false}>
							З<Text style={{
							color: Colors.textColor,
							fontSize: Common.getLengthByIPhone7(24),
							lineHeight: Common.getLengthByIPhone7(27),
							letterSpacing: -0.55,
							fontFamily: 'Montserrat-Bold',
							textAlign: 'center',
							fontWeight: 'bold',
						}}
						allowFontScaling={false}>
							адержки
						</Text>
						</Text>
					</View>
					<TextInput
						style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
							height: Common.getLengthByIPhone7(40),
							borderRadius: Common.getLengthByIPhone7(15),
							borderWidth: 1,
							borderColor: Colors.grayBorderColor,
							marginTop: Common.getLengthByIPhone7(10),
							marginBottom: Common.getLengthByIPhone7(10),
							fontFamily: 'Rubik-Regular',
							textAlign: 'left',
							// lineHeight: Common.getLengthByIPhone7(20),
							fontSize: Common.getLengthByIPhone7(16),
							color: Colors.textColor,
							  paddingLeft: Common.getLengthByIPhone7(10),
						}}
						placeholderTextColor={Colors.placeholderColor}
						placeholder={'Поиск по номеру дела'}
						contextMenuHidden={false}
						autoCorrect={false}
						// autoCompleteType={'off'}
						inputAccessoryViewID={this.props.inputAccessoryViewID}
						returnKeyType={'done'}
						secureTextEntry={false}
						autoCapitalize={this.props.autoCapitalize}
						// keyboardType={'number-pad'}
						allowFontScaling={false}
						underlineColorAndroid={'transparent'}
						onSubmitEditing={() => {
							
						}}
						ref={el => (this.codeRef = el)}
						onFocus={() => {}}
						onBlur={() => {
						
						}}
						onChangeText={search => {
							this.setState({
								search,
							}, () => {
								if (search.length === 0) {
									this.setState({
										rows: Network.delayList,
									});
								} else {
									if (search.length > 6) {
										searchDelays(search)
										.then(data => {
											this.setState({
												rows: data,
											});
										})
										.catch(err => {

										});
									} else {
										this.setState({
											rows: [],
										});
									}
								}
							});
						}}
						value={this.state.search}
					/>
					{this.state.rows.length ? this.renderList() : this.renderEmpty()}
				</View>
			</KeyboardAvoidingView>
        </Modal>);
  }
}
