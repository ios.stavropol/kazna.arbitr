import React from 'react';
import {
    Image,
    Text,
    View,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network from './../../Utilites/Network';
import { observer } from 'mobx-react';

@observer
export default class ProfileView extends React.Component {

  state = {
    refresh: false,
  };

  UNSAFE_componentWillMount() {
    
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  render() {

    return (<View style={{
        marginTop: Common.getLengthByIPhone7(80),
        width: Common.getLengthByIPhone7(0),
        paddingLeft: Common.getLengthByIPhone7(25),
        paddingRight: Common.getLengthByIPhone7(25),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
        <View style={{

        }}>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(24),
                lineHeight: Common.getLengthByIPhone7(24),
            }}>
                {`ИВАН\nАТКНИН`}
            </Text>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(10),
                lineHeight: Common.getLengthByIPhone7(18),
            }}>
                Тариф Старт
            </Text>
        </View>
        <View style={{
            width: Common.getLengthByIPhone7(61),
            height: Common.getLengthByIPhone7(61),
            borderRadius: Common.getLengthByIPhone7(61)/2,
            overflow: 'hidden',
        }}>
            <Image
              source={require('./../../assets/ic-user1.png')}
              style={{
                  width: Common.getLengthByIPhone7(61),
                  height: Common.getLengthByIPhone7(61),
                  resizeMode: 'contain',
              }}/>
        </View>
      </View>);
  }
}
