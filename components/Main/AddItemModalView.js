import React from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  Animated,
  Easing,
  TouchableOpacity,
  Linking,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native';
import Colors from './../../constants/Colors';
import Config from './../../constants/Config';
import Common from './../../Utilites/Common';
import Network, {newSubscribtion} from './../../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import Modal from 'react-native-modal';
import AnimatedButton from '../components/Buttons/AnimatedButton';
import AnimatedTextInputView from './../AnimatedTextInputView';

@observer
export default class AddItemModalView extends React.Component {

    spinValue = new Animated.Value(0);
    
  state = {
    refresh: false,
    show: false,
    index: null,
    text: '',
  };

  UNSAFE_componentWillMount() {
    this.setState({
        show: this.props.show,
        index: this.props.index,
      });
  }

  UNSAFE_componentWillUnmount() {

  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      show: props.show,
      index: props.index,
    });
  }

  constructor(props) {
    super(props);

  }

  render() {

    let scanButton = (<TouchableOpacity style={{
        // marginTop: Common.getLengthByIPhone7(40),
        // width: Common.getLengthByIPhone7(12),
        // height: Common.getLengthByIPhone7(12),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    }}
    onPress={() => {
        if (this.props.onScan) {
            this.props.onScan();
        }
    }}>
        <Text style={{
            marginRight: Common.getLengthByIPhone7(5),
            color: Colors.textColor,
            fontSize: Common.getLengthByIPhone7(18),
            // lineHeight: Common.getLengthByIPhone7(27),
            letterSpacing: -0.55,
            fontFamily: 'Montserrat-Bold',
            textAlign: 'center',
        }}
        allowFontScaling={false}>
            QR
        </Text>
        <Image
            source={require('./../../assets/ic-scan.png')}
            style={{
                width: Common.getLengthByIPhone7(27),
                height: Common.getLengthByIPhone7(27),
                resizeMode: 'contain',
            }}
        />
    </TouchableOpacity>);

    // scanButton = null;
    
    return (<View style={{
        // flex: 1,
      }}>
        <Modal isVisible={this.state.show}>
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
            }}>
                <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "padding" : "height"}
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                    }}
                >
                <TouchableOpacity style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'transparent',
                }}
                onPress={() => {
                    this.setState({
                        text: '',
                    }, () => {
                        
                    });
                    if (this.props.onClose) {
                        this.props.onClose();
                    }
                }}
                />
                <View style={{
                    width: Common.getLengthByIPhone7(0),
                    backgroundColor: 'white',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    marginBottom: -20,
                }}>
                    <View style={{
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                        alignItems: 'flex-start',
                    }}>
                        <View style={{
                            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginTop: Common.getLengthByIPhone7(40),
                        }}>
                            <TouchableOpacity style={{
                                width: Common.getLengthByIPhone7(30),
                                height: Common.getLengthByIPhone7(30),
                                alignItems: 'flex-start',
                                justifyContent: 'center',
                            }}
                            onPress={() => {
                                this.setState({
                                    text: '',
                                }, () => {
                                    
                                });
                                if (this.props.onClose) {
                                    this.props.onClose();
                                }
                            }}>
                                <Image
                                    source={require('./../../assets/ic-close.png')}
                                    style={{
                                        width: Common.getLengthByIPhone7(12),
                                        height: Common.getLengthByIPhone7(12),
                                        resizeMode: 'contain',
                                    }}
                                />
                            </TouchableOpacity>
                            <Text style={{
                                color: Colors.textColor,
                                fontSize: Common.getLengthByIPhone7(22),
                                // lineHeight: Common.getLengthByIPhone7(27),
                                letterSpacing: -0.55,
                                fontFamily: 'Montserrat-Semibold',
                                textAlign: 'center',
                            }}
                            allowFontScaling={false}>
                                Дела
                            </Text>
                            {scanButton}
                        </View>
                        <Text style={{
                            marginTop: Common.getLengthByIPhone7(24),
                            color: Colors.textColor,
                            fontSize: Common.getLengthByIPhone7(22),
                            lineHeight: Common.getLengthByIPhone7(27),
                            letterSpacing: -0.55,
                            fontFamily: 'Montserrat-Regular',
                            textAlign: 'center',
                        }}
                        allowFontScaling={false}>
                            {this.state.index === 'company' ? 'Добавление новой компании' : 'Добавление нового дела'}
                        </Text>
                        <AnimatedTextInputView
                            style={{
                                marginTop: Common.getLengthByIPhone7(42),
                                marginBottom: Common.getLengthByIPhone7(5),
                            }}
                            value={this.state.text}
                            title={this.state.index === 'company' ? 'Введите ИНН компании' : 'Введите номер дела или ссылку'}
                            autoCapitalize={'words'}
                            autoCorrect={false}
                            // keyboardType={this.state.index === 'company' ? 'number-pad' : 'default'}
                            onChangeText={text => {
                                this.setState({
                                    text,
                                });
                            }}
                        />
                        {this.state.index === 'company' ? null : (<TouchableOpacity style={{
                            marginTop: Common.getLengthByIPhone7(5),
                            flexDirection: 'row',
                            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                        }}
                        activeOpacity={1}
                        onPress={() => {
                            let url = 'http://m.kad.arbitr.ru/';
                            Linking.canOpenURL(url).then(supported => {
                                if (supported) {
                                    Linking.openURL(url)
                                    .catch((err) => {
                                        console.warn(err);
                                    });
                                } else {
                                    Alert(Config.appName, 'Данный тип документа невозможно открыть!');
                                    console.log("Don't know how to open URI: " + this.props.url);
                                }
                            });
                        }}>
                            <Image
                                source={require('./../../assets/ic-info.png')}
                                style={{
                                    width: Common.getLengthByIPhone7(12),
                                    height: Common.getLengthByIPhone7(12),
                                    resizeMode: 'contain',
                                    marginTop: 3,
                                }}
                            />
                            <Text style={{
                                marginLeft: Common.getLengthByIPhone7(5),
                                color: Colors.textColor,
                                fontSize: Common.getLengthByIPhone7(12),
                                // lineHeight: Common.getLengthByIPhone7(27),
                                letterSpacing: -0.55,
                                fontFamily: 'Montserrat-Regular',
                                textAlign: 'left',
                            }}
                            allowFontScaling={false}>
                                {`Не знаете номер дела? - нажмите для\nперехода на сайт `}<Text style={{
                                marginLeft: Common.getLengthByIPhone7(5),
                                color: '#EB5E1F',
                                fontSize: Common.getLengthByIPhone7(12),
                                // lineHeight: Common.getLengthByIPhone7(27),
                                letterSpacing: -0.55,
                                fontFamily: 'Montserrat-Regular',
                                textAlign: 'left',
                                textDecorationLine: 'underline',
                            }}
                            allowFontScaling={false}>
                                {`kad.arbitr.ru`}
                            </Text>{` для поиска`}
                            </Text>
                        </TouchableOpacity>)}
                    </View>
                    <AnimatedButton style={{
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
                        height: Common.getLengthByIPhone7(35),
                        borderRadius: Common.getLengthByIPhone7(4),
                        marginTop: Common.getLengthByIPhone7(120),
                        marginBottom: Common.getLengthByIPhone7(44),
                        backgroundColor: '#F6895E',
                    }}
                    textStyle={{
                        fontSize: Common.getLengthByIPhone7(16),
                    }}
                    onLayout={this.onLayout}
                    onPress={() => {
                        
                        if (this.state.text.length) {
                            this.setState({
                                loading: true,
                            }, () => {
                                let strs = this.state.text.split(',');
                                let promiseArray = [];
                                for (let i = 0; i < strs.length; i++) {
                                    let sou = null;
                                    if (strs[i].indexOf('http://') !== -1 || strs[i].indexOf('https://') !== -1) {
                                        sou = true;
                                    }
                                    promiseArray.push(newSubscribtion(this.state.index, strs[i], sou));
                                }

                                Promise.all(promiseArray).then(values => {
                                    console.warn('all values: '+JSON.stringify(values));
                                    if (Network.refreshList != null) {
                                        Network.refreshList();
                                    }
                                    this.setState({
                                        loading: false,
                                        text: '',
                                    }, () => {
                                        if (this.props.onClose) {
                                            this.props.onClose();
                                        }
                                    });
                                })
                                .catch(err => {
                                    console.warn('all err: '+err);
                                    this.setState({
                                        loading: false,
                                    }, () => {
                                        setTimeout(() => {
                                            Alert.alert(Config.appName, err);
                                        }, 100);
                                    });
                                });
                                // newSubscribtion(this.state.index, this.state.text)
                                // .then(() => {
                                    // if (Network.refreshList != null) {
                                    //     Network.refreshList();
                                    // }
                                    // this.setState({
                                    //     loading: false,
                                    //     text: '',
                                    // }, () => {
                                    //     if (this.props.onClose) {
                                    //         this.props.onClose();
                                    //     }
                                    // });
                                // })
                                // .catch(err => {
                                //     this.setState({
                                //         loading: false,
                                //     }, () => {
                                //         setTimeout(() => {
                                //             Alert.alert(Config.appName, err);
                                //         }, 100);
                                //     });
                                // });
                            });
                        } else {
                            Alert.alert(Config.appName, 'Заполните поле!');
                        }
                    }}
                    borderColor={'#F6895E'}
                    title={'Загрузить дело'}
                    activeColor={'#F6895E'}
                    inactiveColor={'white'}
                    />
                </View>
                </KeyboardAvoidingView>
            </View>
        </Modal>
        <Spinner
          visible={this.state.loading}
          // textContent={'Загрузка...'}
          onShow={() => {
            Animated.loop(
              Animated.timing(
                this.spinValue,
                {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear,
                useNativeDriver: true
                }
              )
            ).start();
          }}
          onDismiss={() => {

          }}
          customIndicator={<View style={{
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 20000,
          }}>
            <View style={{
              width: Common.getLengthByIPhone7(65),
              height: Common.getLengthByIPhone7(65),
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <Animated.Image
                style={{
                  width: Common.getLengthByIPhone7(65),
                  height: Common.getLengthByIPhone7(65),
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  transform: [{rotate: this.spinValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '360deg']
                  })}] 
                }}
                source={require('./../assets/ic-loader.png')}
              />
              <TouchableOpacity style={{
                // marginTop: Common.getLengthByIPhone7(20),
                padding: Common.getLengthByIPhone7(5),
                backgroundColor: 'rgba(255, 255, 255, 0.8)',
                borderRadius: Common.getLengthByIPhone7(5),
              }}
              onPress={() => {
                this.setState({
                  loading: false,
                });
              }}>
                <Image
                  style={{
                    width: Common.getLengthByIPhone7(18),
                    height: Common.getLengthByIPhone7(18),
                  }}
                  source={require('./../assets/ic-loader-close.png')}
                />
              </TouchableOpacity>
            </View>
          </View>}
          overlayColor={'rgba(32, 42, 91, 0.3)'}
          textStyle={{color: '#FFF'}}
        />
      </View>);
  }
}
