import React from 'react';
import {
  Alert,
  View,
  Image,
  Platform,
  Text,
} from 'react-native';
import Colors from './../constants/Colors';
import Common from './../Utilites/Common';
import { observer } from 'mobx-react';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Config from '../constants/Config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Network from '../Utilites/Network';

@observer
export default class InfoNavBar extends React.Component {

  state = {
    show: false,
  };

  UNSAFE_componentWillMount() {
    Network.setShowMenu = this.setShow;
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  setShow = show => {
    this.setState({
      show,
    });
  }

  render() {

    return (<View style={{
        // flex: 1,
        width: Common.getLengthByIPhone7(0),
        height: isIphoneX() ? Common.getLengthByIPhone7(88) : (Platform.OS === 'ios' ? Common.getLengthByIPhone7(68) + 20 : Common.getLengthByIPhone7(68)),
        // height: Dimensions.get('window').height,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        backgroundColor: 'white',
        paddingBottom: isIphoneX() ? Common.getLengthByIPhone7(20) : (Platform.OS === 'ios' ? Common.getLengthByIPhone7(30) : Common.getLengthByIPhone7(30)),
      }}>
        <TouchableOpacity style={{
            marginLeft: Common.getLengthByIPhone7(16),
        }}
        onPress={() => {
          this.props.navigation.goBack(null);
        }}>
            <Image
            source={require('./../../assets/ic-arrow-back.png')}
            style={{
                width: Common.getLengthByIPhone7(23),
                height: Common.getLengthByIPhone7(27),
                resizeMode: 'contain',
            }}/>
        </TouchableOpacity>
      </View>);
  }
}
