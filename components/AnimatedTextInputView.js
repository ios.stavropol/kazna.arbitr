import React from 'react';
import { Platform, Text, Image, TextInput, View, Dimensions, TouchableOpacity, Animated, Easing, StyleSheet } from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config'
import Common from './../Utilites/Common'
import Network, { setView } from './../Utilites/Network'

export default class AnimatedTextInputView extends React.Component {

  titleTop = new Animated.Value(0);

  state = {
    text: '',
    focus: false,
    fontWeight: '500',
    title: '',
  };

  UNSAFE_componentWillMount() {
    this.setState({
      text: this.props.value != null ? this.props.value.toString() : this.props.value,
      title: this.props.title,
      placeholder: this.props.placeholder,
    }, () => {
      if (this.props.value != null && this.props.value.length) {
        setTimeout(() => {
          Animated.timing(
            this.titleTop,
            {
              toValue: (Platform.OS === 'ios' ? Common.getLengthByIPhone7(25) : Common.getLengthByIPhone7(25)),
              duration: 300,
            }
          ).start(() => {
            this.setState({
              title: this.props.title,
            });
          });
        }, 100);
      }
    });
  }

  UNSAFE_componentDidMount() {

  }

  constructor(props) {
    super(props);
  }

  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      text: props.value != null ? props.value.toString() : props.value,
      title: props.title,
      placeholder: props.placeholder,
    }, () => {
      if (props.value != null && props.value.length) {
        setTimeout(() => {
          Animated.timing(
            this.titleTop,
            {
              toValue: (Platform.OS === 'ios' ? Common.getLengthByIPhone7(25) : Common.getLengthByIPhone7(25)),
              duration: 300,
            }
          ).start(() => {
            this.setState({
              title: props.title,
            });
          });
        }, 100);
      }
    });
  }

  focus = () => {
    this.textInputRef.focus();
  }

  render() {

    return (
      <View style={[{
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
      }, this.props.style]}>
        <View style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          height: Common.getLengthByIPhone7(60),
          alignItems: 'flex-start',
          justifyContent: 'flex-end',
          backgroundColor: 'transparent',
          // backgroundColor: 'red',
          // flexDirection: 'row',
          marginTop: Common.getLengthByIPhone7(10),
          borderBottomWidth: 1,
          borderBottomColor: '#f5f5f5',
        }}
        ref={el => this.backView = el}>
          <Animated.Text style={{
            color: '#999999',
            fontSize: this.titleTop.interpolate({
              inputRange: [0, Common.getLengthByIPhone7(50)],
              outputRange: [Common.getLengthByIPhone7(14), Common.getLengthByIPhone7(11)],
            }),
            fontFamily: 'Montserrat-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            position: 'absolute',
            left: 0,
            bottom: this.titleTop,
            lineHeight: (Platform.OS === 'ios' ? Common.getLengthByIPhone7(30) : Common.getLengthByIPhone7(38)),
            // color: '#ADACAC'
          }}
            allowFontScaling={false}>
            {this.state.title}
          </Animated.Text>
          <View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            height: (Platform.OS === 'ios' ? Common.getLengthByIPhone7(30) : Common.getLengthByIPhone7(38)),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            // marginTop: Common.getLengthByIPhone7(),
          }}>
            <TextInput
              style={[{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                height: (Platform.OS === 'ios' ? Common.getLengthByIPhone7(30) : Common.getLengthByIPhone7(38)),
                paddingLeft: 0,
              }, this.props.style2, styles.text]}
              allowFontScaling={false}
              contextMenuHidden={false}
              spellCheck={this.props.spellCheck}
              autoCorrect={this.props.autoCorrect}
              placeholder={this.state.placeholder}
              placeholderTextColor={this.props.placeholderTextColor}
              autoCompleteType={'off'}
              inputAccessoryViewID={this.props.inputAccessoryViewID}
              multiline={this.props.multiline}
              numberOfLines={this.props.numberOfLines}
              returnKeyType={this.props.returnKeyType}
              secureTextEntry={false}
              autoCapitalize={this.props.autoCapitalize}
              keyboardType={this.props.keyboardType}
              allowFontScaling={false}
              underlineColorAndroid={'transparent'}
              onSubmitEditing={() => {
                if(this.props.onSubmitEditing != null) {
                  this.props.onSubmitEditing();
                }
              }}
              ref={el => this.textInputRef = el}
              onFocus={() => {
                this.setState({
                  focus: true,
                });
                Animated.timing(
                  this.titleTop,
                  {
                    toValue: (Platform.OS === 'ios' ? Common.getLengthByIPhone7(25) : Common.getLengthByIPhone7(25)),
                    duration: 300,
                  }
                ).start();
                if(this.props.onFocus) {
                  this.props.onFocus();
                }
              }}
              onBlur={() => {
                this.setState({
                  focus: false,
                }, () => {
                  if(this.props.onBlur) {
                    this.props.onBlur();
                  }

                  if (this.state.text == null || this.state.text.length === 0) {
                    Animated.timing(
                      this.titleTop,
                      {
                        toValue: 0,
                        duration: 300,
                      }
                    ).start(() => {
                      this.setState({
                        fontWeight: '500',
                      });
                    });
                  }
                });
              }}
              onChangeText={(text) => {
                this.setState({text});
                if(this.props.onChangeText != null) {
                  this.props.onChangeText(text);
                }
              }}
              value={this.state.text}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    color: '#3e3e3e',
    fontSize: Common.getLengthByIPhone7(16),
    lineHeight: Common.getLengthByIPhone7(20),
    letterSpacing: -0.24,
    fontFamily: 'Montserrat-Regular',
    fontWeight: 'normal',
    textAlign: 'left',
  },
})
