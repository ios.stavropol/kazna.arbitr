// @flow
import React, { Fragment, PureComponent, Component } from "react";
import { Dimensions, View, Alert, Image, Text, TouchableOpacity, ActivityIndicator, Animated } from "react-native";
import Common from './../Utilites/Common'
import Network, { getPicture } from './../Utilites/Network'
import Config from './../constants/Config'
import styles from "./storyItemStyles";
import StoryImageView from './StoryImageView';
import { observer } from 'mobx-react';
import DropShadow from "react-native-drop-shadow";

@observer
export default class extends Component {

  index = 0;
  items = [];
  globalWidth = 0;

  state = {
    index: 0,
    reload: false,
    stateIndex: 0,
  }

  constructor(props) {
    super(props);
    // this.items = props.story.clips.map(() => React.createRef());
    // Network.storyItemWidth[props.index] = props.story.clips.map(() => new Animated.Value(0));
  }

  UNSAFE_componentWillMount() {
    // if (this.props.story.clips !== null && this.props.story.clips != undefined) {
      this.items = this.props.story.clips.map(() => React.createRef());
      Network.storyItemWidth[this.props.index] = this.props.story.clips.map(() => new Animated.Value(0));
    // }
    this.setState({
      stateIndex: this.props.index,
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    // if (props.story.clips !== null && props.story.clips != undefined) {
      this.items = props.story.clips.map(() => React.createRef());
      Network.storyItemWidth[props.index] = props.story.clips.map(() => new Animated.Value(0));
    // }
    
    this.setState({
      stateIndex: props.index,
    });
  }

  prevPage = () => {
    if(Network.storyIndex == 0) {
      if(Network.storyItemIndex > 0) {
        Network.storyItemIndex--;
        this.setState({
          reload: !this.state.reload,
        });
      } else {
        this.setState({
          reload: !this.state.reload,
        });
      }
    } else {
      if(Network.storyItemIndex > 0) {
        Network.storyItemIndex--;
        this.setState({
          reload: !this.state.reload,
        });
      } else {
        Animated.timing(Network.storyItemWidth[Network.storyIndex][Network.storyItemIndex], {
          toValue: 0,
          duration: 0,
        }).start(() => {
          // Animated.timing(Network.storyItemWidth[Network.storyIndex][(this.props.display === 'case' ? Network.caseClipsList[Network.storyIndex - 1].clips.length : Network.clipsList[Network.storyIndex - 1].clips.length) - 1], {
          //   toValue: 0,
          //   duration: 0,
          // }).start(() => {
            Network.storyItemWidth[Network.storyIndex][(this.props.display === 'case' ? Network.caseClipsList[Network.storyIndex - 1].clips.length : Network.clipsList[Network.storyIndex - 1].clips.length) - 1] = new Animated.Value(0);
            Network.storyIndex--;
            Network.storyItemIndex = (this.props.display === 'case' ? Network.caseClipsList[Network.storyIndex].clips.length : Network.clipsList[Network.storyIndex].clips.length) - 1;
            // Network.storyScroll.getNode().scrollTo({x: Network.storyIndex*Dimensions.get("window").width, y: 0, animated: true});
            Network.storyScroll.scrollTo({x: Network.storyIndex*Dimensions.get("window").width, y: 0, animated: true});
            this.setState({
              reload: !this.state.reload,
              stateIndex: Network.storyIndex,
            });
          // });
        });
      }
    }
  }

  nextPage = () => {
    if(Network.storyIndex + 1 == (this.props.display === 'case' ? Network.caseClipsList.length : Network.clipsList.length)) {
      if(Network.storyItemIndex + 1 < (this.props.display === 'case' ? Network.caseClipsList[Network.storyIndex].clips.length : Network.clipsList[Network.storyIndex].clips.length)) {
        console.warn('nextPage: 1: ' + Network.storyIndex + ' : ' + Network.storyItemIndex);
        Network.storyItemIndex++;
        this.setState({
          reload: !this.state.reload,
        });
      } else {
        console.warn('nextPage: 2: ' + Network.storyIndex + ' : ' + Network.storyItemIndex);
        if (this.props.onClose) {
          this.props.onClose();
        }
      }
    } else {
      if(Network.storyItemIndex + 1 < (this.props.display === 'case' ? Network.caseClipsList[Network.storyIndex].clips.length : Network.clipsList[Network.storyIndex].clips.length)) {
        Network.storyItemIndex++;
        this.setState({
          reload: !this.state.reload,
        });
      } else {
        // Network.storyIndex++;
        // Network.storyItemIndex = 0;
        if (this.props.onClose) {
          this.props.onClose();
        }
        // Network.storyScroll.getNode().scrollTo({x: Network.storyIndex*Common.getLengthByIPhone7(0), y: 0, animated: true});
        // Network.storyScroll.scrollTo({x: Network.storyIndex*Common.getLengthByIPhone7(0), y: 0, animated: true});
        // this.setState({
        //   reload: !this.state.reload,
        //   stateIndex: Network.storyIndex,
        // });
      }
    }
  }

  render() {

    const {
      story,
      selectedStory,
    } = this.props;

    console.warn('story render: '+JSON.stringify(story));
    
    let itemWidth = (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20) - Common.getLengthByIPhone7(5)*(story.clips.length - 1))/story.clips.length;
    this.globalWidth = (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20) - Common.getLengthByIPhone7(5)*(story.clips.length - 1))/story.clips.length;

    // if(this.props.index == Network.storyIndex && Network.storyReady) {
    if(this.state.stateIndex == Network.storyIndex) {
      this.index = Network.storyItemIndex;
      if (Network.storyItemWidth[this.state.stateIndex][Network.storyItemIndex] !== null && Network.storyItemWidth[this.state.stateIndex][Network.storyItemIndex] !== undefined) {
        Animated.timing(Network.storyItemWidth[this.state.stateIndex][Network.storyItemIndex], {
          toValue: this.globalWidth,
          duration: 10000,
        }).start(event => {
          if(Network.direction == 0) {
            if(!event.finished) {
              console.warn('if(Network.direction == 0) { false');
              Animated.timing(Network.storyItemWidth[this.state.stateIndex][Network.storyItemIndex], {
                toValue: 0,
                duration: 0,
              }).start(() => { 
                if(Network.storyItemIndex) {
                  Animated.timing(Network.storyItemWidth[this.state.stateIndex][Network.storyItemIndex - 1], {
                    toValue: 0,
                    duration: 0,
                  }).start(() => {
                    this.prevPage();
                  });
                } else {
                  this.prevPage();
                }
              });
            } else {
              console.warn('if(Network.direction == 0) { true');
              Network.direction = 1;
              this.nextPage();
            }
          } else {
            if(!event.finished) {
              console.warn('if(!event.finished) false');
              Animated.timing(Network.storyItemWidth[this.state.stateIndex][Network.storyItemIndex], {
                toValue: this.globalWidth,
                duration: 0,
              }).start(() => {
                this.nextPage();
              });
            } else {
              console.warn('if(!event.finished) true');
              this.nextPage();
            }
          }
        });
      }
    }

    let items = [];

    for(let i=0;i<story.clips.length;i++) {
      items.push(<View style={{
          width: itemWidth + 1,
          paddingLeft: 0.5,
          height: Common.getLengthByIPhone7(3) + 1,
          alignItems: 'flex-start',
          justifyContent: 'center',
          borderRadius: Common.getLengthByIPhone7(3)/2,
          backgroundColor: 'rgba(0, 0, 0, 0.2)',
          marginLeft: (i == 0 ? 0 : Common.getLengthByIPhone7(5)),
        }}
        key={i}>
          <Animated.View style={{
            width: Network.storyItemWidth[this.state.stateIndex][i],
            height: Common.getLengthByIPhone7(3),
            borderRadius: Common.getLengthByIPhone7(3)/2,
            backgroundColor: 'white',
          }}
          ref={this.items[i]}/>
        </View>);
    }

    // Alert.alert(Config.appName, JSON.stringify(story.clips[this.index]));
    return (
      <View style={{
        flex: 1,
        backgroundColor: "black"
      }}>
        <View style={styles.container}>
          <StoryImageView
            data={story.clips[this.index]}
            onClose={() => {
              if (this.props.onClose) {
                console.warn('itemView onClose');
                this.props.onClose();
              }
            }}
          />
          <View style={{
            position: 'absolute',
            left: Common.getLengthByIPhone7(10),
            right: Common.getLengthByIPhone7(10),
            top: Common.getLengthByIPhone7(15),
            height: Common.getLengthByIPhone7(3),
            // backgroundColor: 'rgba(0, 0, 0, 0.3)',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}>
            {items}
          </View>
        </View>
      </View>
    );
  }
}
