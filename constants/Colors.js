const Colors = {
  textColor: '#424347',
  grayC4: 'rgba(196, 196, 196, 0.38)',
  grayC1: '#c1c1c1',
  mainColor: '#322661',
  orangeColor: '#EB5E1F',
  backColor: '#f1f4fa',


  textGrayColor: '#5A5E75',
  blueTextColor: '#7D819A',
  violetColor: '#5E4BAD',
  redColor: '#EA5055',
  greenColor: '#4BAD55',
  placeholderColor: '#9799AE',
  grayBorderColor: '#CED1E0',
  fonColor: '#F3F6FC',
  shadowColor: '#D7DEEC',
  lineColor: '#E5E7F1',
};

export default Colors;
