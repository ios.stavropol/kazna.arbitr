const Config = {
  apiDomain:              'https://arbitr.kazna.tech',
  version:                '1.0',
  appName:                'Legal.Track',
};

export default Config;
