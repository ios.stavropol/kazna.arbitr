const fonts = {
  'UbuntuRegular': require('./../assets/fonts/Ubuntu_Regular.ttf'),
  'UbuntuLight': require('./../assets/fonts/Ubuntu_Light.ttf'),
  'UbuntuMedium': require('./../assets/fonts/Ubuntu_Medium.ttf'),
  'MontserratMedium': require('./../assets/fonts/Montserrat_Medium.ttf'),
  'MontserratSemibold': require('./../assets/fonts/Montserrat_SemiBold.ttf'),
};

export default fonts;
