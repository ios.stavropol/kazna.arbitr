import React from 'react';
import {
  Dimensions,
  Text,
  View,
  BackHandler,
  Animated,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  Platform,
  TextInput,
  Switch,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, { muteSidesCase, mutePushCase } from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { observer } from 'mobx-react';

@observer
export default class CaseSettingsScreen extends React.Component {

    calculate = true;
    top = new Animated.Value(0);

    state = {
        loading: false,
        data: null,
        selected: [],
        allSelected: [],
        pushed: false,
        all: false,
        search: '',
        rows: [],
    };

    UNSAFE_componentWillMount() {
        
        BackHandler.addEventListener(
            "hardwareBackPress",
            () => {
                this.props.navigation.goBack(null);
                return true;
            }
        );

        this.setState({
            data: this.props.navigation.state.params.data,
            selected: this.props.navigation.state.params.data.muted_side,
            pushed: this.props.navigation.state.params.data.muted_all === false ? true : false,
        }, () => {
            this.filter();
            console.warn(this.state.pushed);
        });
    }

    UNSAFE_componentWillUnmount() {

    }

    constructor(props) {
        super(props);

    }

    filter = () => {
        let rows = [];
        let allSelected = [];
        // console.warn(this.state.data.is_bankrot);
        // console.warn('this.state.data.sides: '+Object.keys(this.state.data.sides));
        // console.warn("this.state.data.sides.Plaintiffs: "+this.state.data.sides.Plaintiffs.length);
        if (this.state.data.sides.Plaintiffs !== null && this.state.data.sides.Plaintiffs !== undefined && this.state.data.sides.Plaintiffs.length) {
            rows.push(<Text style={{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: Colors.textColor,
                fontSize: Common.getLengthByIPhone7(17),
                fontFamily: 'Montserrat',
                fontWeight: 'bold',
                textAlign: 'left',
                marginTop: Common.getLengthByIPhone7(20),
            }}>
                    Истцы
            </Text>);
            for (let i = 0; i < this.state.data.sides.Plaintiffs.length; i++) {
                if (this.state.search.length) {
                    if (this.state.data.sides.Plaintiffs[i].Name.toLocaleLowerCase().indexOf(this.state.search.toLocaleLowerCase()) !== -1) {
                        rows.push(this.renderSwitch(this.state.data.sides.Plaintiffs[i]));
                    }
                } else {
                    rows.push(this.renderSwitch(this.state.data.sides.Plaintiffs[i]));
                }
                allSelected.push(this.state.data.sides.Plaintiffs[i].Id);
            }
        }

        if (this.state.data.sides.Defendants !== null && this.state.data.sides.Defendants !== undefined && this.state.data.sides.Defendants.length) {
            rows.push(<Text style={{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: Colors.textColor,
                fontSize: Common.getLengthByIPhone7(17),
                fontFamily: 'Montserrat',
                fontWeight: 'bold',
                textAlign: 'left',
                marginTop: Common.getLengthByIPhone7(20),
              }}>
                Ответчики
              </Text>);
            for (let i = 0; i < this.state.data.sides.Defendants.length; i++) {
                if (this.state.search.length) {
                    if (this.state.data.sides.Defendants[i].Name.toLocaleLowerCase().indexOf(this.state.search.toLocaleLowerCase()) !== -1) {
                        rows.push(this.renderSwitch(this.state.data.sides.Defendants[i]));
                    }
                } else {
                    rows.push(this.renderSwitch(this.state.data.sides.Defendants[i]));
                }
                allSelected.push(this.state.data.sides.Defendants[i].Id);
            }
        }

        if (this.state.data.sides.Third !== null && this.state.data.sides.Third !== undefined && this.state.data.sides.Third.length) {
            rows.push(<Text style={{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: Colors.textColor,
                fontSize: Common.getLengthByIPhone7(17),
                fontFamily: 'Montserrat',
                fontWeight: 'bold',
                textAlign: 'left',
                marginTop: Common.getLengthByIPhone7(20),
              }}>
                Третьи лица
              </Text>);
            for (let i = 0; i < this.state.data.sides.Third.length; i++) {
                if (this.state.search.length) {
                    if (this.state.data.sides.Third[i].Name.toLocaleLowerCase().indexOf(this.state.search.toLocaleLowerCase()) !== -1) {
                        rows.push(this.renderSwitch(this.state.data.sides.Third[i]));
                    }
                } else {
                    rows.push(this.renderSwitch(this.state.data.sides.Third[i]));
                }
                allSelected.push(this.state.data.sides.Third[i].Id);
            }
        }

        if (this.state.data.sides.Others !== null && this.state.data.sides.Others !== undefined && this.state.data.sides.Others.length) {
            rows.push(<Text style={{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: Colors.textColor,
                fontSize: Common.getLengthByIPhone7(17),
                fontFamily: 'Montserrat',
                fontWeight: 'bold',
                textAlign: 'left',
                marginTop: Common.getLengthByIPhone7(20),
              }}>
                Другие лица
              </Text>);
            for (let i = 0; i < this.state.data.sides.Others.length; i++) {
                if (this.state.search.length) {
                    if (this.state.data.sides.Others[i].Name.toLocaleLowerCase().indexOf(this.state.search.toLocaleLowerCase()) !== -1) {
                        rows.push(this.renderSwitch(this.state.data.sides.Others[i]));
                    }
                } else {
                    rows.push(this.renderSwitch(this.state.data.sides.Others[i]));
                }
                allSelected.push(this.state.data.sides.Others[i].Id);
            }
        }

        this.setState({
            rows,
            allSelected,
            all: this.state.selected.length === 0 ? true : false,
        });
    }

    renderSwitch = data => {
        return (<View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            marginTop: Common.getLengthByIPhone7(20),
            // marginBottom: Common.getLengthByIPhone7(10),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
            <Text style={{
                width: Common.getLengthByIPhone7(270),
                color: '#3e3e3e',
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(16),
                lineHeight: Common.getLengthByIPhone7(20),
            }}>
              {data.Name}
            </Text>
            <Switch
              onValueChange={value => {
                    let selected = this.state.selected;
                    if (value) {
                        for(let i = 0; i < selected.length; i++){   
                            if (selected[i] === data.Id) {
                                selected.splice(i, 1);
                            }
                        }
                    } else {
                        selected.push(data.Id);
                    }
                    this.setState({
                        selected,
                        all: selected.length === 0 ? true : false,
                    }, () => {
                        this.filter();
                        muteSidesCase(this.state.data.id, selected)
                        .then(() => {
                            if (Network.refreshCase) {
                                Network.refreshCase();
                            }
                        })
                        .catch(err => {

                        });
                    });
              }}
              value={!this.state.selected.includes(data.Id)}
            />
        </View>);
    }

    renderBankrot = () => {
        return (<View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            flex: 1,
            // marginLeft: Common.getLengthByIPhone7(20),
            marginTop: Common.getLengthByIPhone7(10),
            marginBottom: Common.getLengthByIPhone7(10),
            // flexDirection: 'row',
            alignItems: 'center',
            // justifyContent: 'space-between',
          }}>
            <Text style={{
              color: Colors.textColor,
              fontSize: Common.getLengthByIPhone7(17),
            //   lineHeight: Common.getLengthByIPhone7(17)*1.4,
              fontFamily: 'Montserrat',
              fontWeight: '600',
              textAlign: 'center',
            }}>
              Фильтр по обособленным сторонам
            </Text>
            <View style={{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: Common.getLengthByIPhone7(10),
            }}>
                <TextInput
                    style={{
                        width: Common.getLengthByIPhone7(260),
                        height: Common.getLengthByIPhone7(40),
                        borderRadius: Common.getLengthByIPhone7(15),
                        borderWidth: 1,
                        borderColor: Colors.grayBorderColor,
                        fontFamily: 'Rubik-Regular',
                        textAlign: 'left',
                        // lineHeight: Common.getLengthByIPhone7(20),
                        fontSize: Common.getLengthByIPhone7(16),
                        color: Colors.textColor,
                          paddingLeft: Common.getLengthByIPhone7(10),
                    }}
                    placeholderTextColor={Colors.placeholderColor}
                    placeholder={'Поиск'}
                    contextMenuHidden={false}
                    autoCorrect={false}
                    autoCompleteType={'off'}
                    inputAccessoryViewID={this.props.inputAccessoryViewID}
                    returnKeyType={'done'}
                    secureTextEntry={false}
                    autoCapitalize={this.props.autoCapitalize}
                    // keyboardType={'number-pad'}
                    allowFontScaling={false}
                    underlineColorAndroid={'transparent'}
                    onSubmitEditing={() => {
                        
                    }}
                    ref={el => (this.codeRef = el)}
                    onFocus={() => {}}
                    onBlur={() => {
                    
                    }}
                    onChangeText={search => {
                        this.setState({
                            search,
                        }, () => {
                            this.filter();
                        });
                    }}
                    value={this.state.search}
                />
                <View style={{
                    alignItems: 'center',
                }}>
                    <Text style={{
                        color: Colors.textColor,
                        fontSize: Common.getLengthByIPhone7(17),
                        lineHeight: Common.getLengthByIPhone7(17)*1.4,
                        fontFamily: 'Montserrat',
                        fontWeight: '600',
                        textAlign: 'center',
                    }}>
                        Все
                    </Text>
                    <Switch
                    onValueChange={value => {
                        let selected = [];
                        if (!value) {
                            selected = this.state.allSelected;
                        }
                        this.setState({
                            all: value,
                            selected,
                        }, () => {
                            this.filter();
                            muteSidesCase(this.state.data.id, selected)
                            .then(() => {
                                if (Network.refreshCase) {
                                    Network.refreshCase();
                                }
                            })
                            .catch(err => {

                            });
                        });
                    }}
                    value={this.state.all}
                    />
                </View>
            </View>
            <ScrollView style={{
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                flex: 1,
                marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? 86 : 52) : 52,
            }}
            contentContainerStyle={{
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
            }}>
                {this.state.rows}
            </ScrollView>
        </View>);
    }

    renderNonBankrot = () => {
        return (<View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            // marginLeft: Common.getLengthByIPhone7(20),
            marginTop: Common.getLengthByIPhone7(20),
            // marginBottom: Common.getLengthByIPhone7(20),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
            <Text style={{
              color: Colors.textColor,
              fontSize: Common.getLengthByIPhone7(17),
              lineHeight: Common.getLengthByIPhone7(17)*1.4,
              fontFamily: 'Montserrat',
              fontWeight: '600',
              textAlign: 'center',
            }}>
              Присылать уведомления
            </Text>
            <Switch
              onValueChange={value => {
                  this.setState({
                      pushed: value,
                  }, () => {
                        mutePushCase(this.state.data.id, !value)
                        .then(() => {
                            if (Network.refreshCase) {
                                Network.refreshCase();
                            }
                        })
                        .catch(err => {

                        });
                  });
              }}
              value={this.state.pushed}
            />
          </View>);
    }

    render() {

        let body = null;

        if (this.state.data !== null) {
            if (this.state.data.is_bankrot) {
                body = this.renderBankrot();
            } else {
                body = this.renderNonBankrot();
            }
        }

        return (<View style={{
            flex: 1,
            width: Common.getLengthByIPhone7(0),
            // height: Dimensions.get('window').height,
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor: 'white',
        }}>
            <View style={{
                // flexDirection: 'row',
                // alignItems: 'center',
                // marginTop: 100,//Platform.OS === 'ios' ? Common.getLengthByIPhone7(25) : 0,//Common.getLengthByIPhone7(60),
                marginTop: (isIphoneX() ? Common.getLengthByIPhone7(105) : Common.getLengthByIPhone7(65)),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            }}>
                <Text style={{
                    // maxWidth: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                    color: Colors.textColor,
                    fontFamily: 'Montserrat-Bold',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(24),
                }}
                allowFontScaling={false}>
                    Настройки дела{`\n`}
                    <Text style={{
                        maxWidth: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                        color: Colors.textColor,
                        fontFamily: 'Montserrat-Regular',
                        fontWeight: 'normal',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(18),
                    }}
                    allowFontScaling={false}>
                        {this.state.data.name != null && this.state.data.name.length ? this.state.data.name : this.state.data['case-number']}
                    </Text>
                </Text>
            </View>
            {body}
        </View>);
    }
}
