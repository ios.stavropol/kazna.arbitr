import React from 'react';
import {
  Dimensions,
  Text,
  View,
  BackHandler,
  Animated,
  Easing,
  Image,
  ScrollView,
  ActivityIndicator,
  Alert,
  TouchableOpacity,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, {getKeyword, editKeyword, getCourts, getCategoriesCases, getInstances } from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import CaseList from './../components/Word/CaseList';
import { isIphoneX } from 'react-native-iphone-x-helper';
import Dialog from "react-native-dialog";
import MultiSelect from 'react-native-multiple-select';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';

export default class WordScreen extends React.Component {

  calculate = true;
  top = new Animated.Value(0);
  spinValue = new Animated.Value(0);
  multiSelect1 = null;
  multiSelect2 = null;
  multiSelect3 = null;

  state = {
    url: '',
    loading: false,
    data: null,
    showPdf: false,
    value: '',
    instances: [],
    courts: [],
    categories: [],
    selectedItems: [],
    selectedInstances: [],
    selectedCategories: [],
    courtsView: null,
  };

  UNSAFE_componentWillMount() {

    BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
            this.props.navigation.goBack(null);
            return true;
        }
    );

    this.refreshData();
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  openBrowser = () => {
    InAppBrowser.isAvailable()
    .then(() => {
        console.warn('url: '+this.state.url);
        InAppBrowser.open(this.state.url, {
            // iOS Properties
            dismissButtonStyle: 'cancel',
            preferredBarTintColor: '#453AA4',
            preferredControlTintColor: 'white',
            readerMode: false,
            animated: true,
            modalPresentationStyle: 'fullScreen',
            modalTransitionStyle: 'coverVertical',
            modalEnabled: true,
            enableBarCollapsing: false,
            // Android Properties
            showTitle: true,
            toolbarColor: Colors.mainColor,
            secondaryToolbarColor: 'black',
            navigationBarColor: 'black',
            navigationBarDividerColor: 'white',
            enableUrlBarHiding: true,
            enableDefaultShare: true,
            forceCloseOnRedirection: false,
            // Specify full animation resource identifier(package:anim/name)
            // or only resource name(in case of animation bundled with app).
            animations: {
            startEnter: 'slide_in_right',
            startExit: 'slide_out_left',
            endEnter: 'slide_in_left',
            endExit: 'slide_out_right'
            },
            headers: {
            'my-custom-header': 'my custom header value'
            }
        })
        .catch(err => {
            console.warn('InAppBrowser err: ' + err);
        });
    })
    .catch(err => {
      console.warn('InAppBrowser err: ' + err);
    });
  }

  refreshData = () => {
    this.setState({
        loading: this.state.data !== null ? false : true,
    }, () => {
        let p1 = getInstances();
        let p2 = getCourts();
        let p3 = getCategoriesCases();
        let p4 = getKeyword(this.props.navigation.state.params.data.id);
        Promise.all([p1, p2, p3, p4]).then(values => {
            console.log(values);
            // Alert.alert('fff', JSON.stringify(values[3].categories));
            this.setState({
                loading: false,
                instances: values[0],
                courts: values[1],
                categories: values[2],
                data: values[3],
                value: values[3].value,
                selectedItems: values[3].courts,
                selectedInstances: values[3].instances,
                selectedCategories: values[3].categories,
            }, () => {
                setTimeout(() => {
                    this.setState({
                        courtsView: this.multiSelect1 !== null ? this.multiSelect1.getSelectedItemsExt(values[3].courts) : null,
                        categoriesView: this.multiSelect2 !== null ? this.multiSelect2.getSelectedItemsExt(values[3].categories) : null,
                        instancesView: this.multiSelect3 !== null ? this.multiSelect3.getSelectedItemsExt(values[3].instances) : null,
                    });
                }, 500);
            });
        });
    });
  }

  renderField = (title, value, style) => {
      return (<View style={[{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          minHeight: Common.getLengthByIPhone7(61),
          borderBottomWidth: 1,
          borderBottomColor: '#f5f5f5',
          justifyContent: 'space-between',
          alignItems: 'flex-start',
      }, style]}>
        <Text style={{
            marginTop: Common.getLengthByIPhone7(10),
            color: '#999999',
            fontFamily: 'Montserrat-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(12),
            lineHeight: Common.getLengthByIPhone7(15),
        }}
        allowFontScaling={false}>
            {title}
        </Text>
        <Text style={{
            marginBottom: Common.getLengthByIPhone7(9),
            color: '#3e3e3e',
            fontFamily: 'Montserrat-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(16),
            lineHeight: Common.getLengthByIPhone7(20),
        }}
        allowFontScaling={false}>
            {value}
        </Text>
      </View>);
  }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems }, () => {
            let body = {
                id: this.state.data.id,
                value: this.state.value,
                courts: selectedItems,
                categories: this.state.data.categories,
                instances: this.state.data.instances,
            };
            editKeyword(body)
            .then(() => {
                this.refreshData();
                this.setState({
                    // loading: false,
                }, () => {
                    if (Network.refreshList) {
                        Network.refreshList();
                    }
                });
            })
            .catch(err => {
                this.setState({
                    // loading: false,
                }, () => {
                    setTimeout(() => {
                        Alert.alert(Config.appName, err);
                    }, 100);
                });
            });
        });
    };

    onSelectedCategoriesChange = selectedItems => {
        this.setState({ selectedCategories: selectedItems }, () => {
            let body = {
                id: this.state.data.id,
                value: this.state.value,
                courts: this.state.data.courts,
                categories: selectedItems,
                instances: this.state.data.instances,
            };
            editKeyword(body)
            .then(() => {
                this.refreshData();
                this.setState({
                    // loading: false,
                }, () => {
                    if (Network.refreshList) {
                        Network.refreshList();
                    }
                });
            })
            .catch(err => {
                this.setState({
                    // loading: false,
                }, () => {
                    setTimeout(() => {
                        Alert.alert(Config.appName, err);
                    }, 100);
                });
            });
        });
    };

    onSelectedInstancesChange = selectedItems => {
        this.setState({ selectedInstances: selectedItems }, () => {
            let body = {
                id: this.state.data.id,
                value: this.state.value,
                courts: this.state.data.courts,
                categories: this.state.data.categories,
                instances: selectedItems,
            };
            console.warn('body: '+JSON.stringify(body));
            editKeyword(body)
            .then(() => {
                this.refreshData();
                this.setState({
                    // loading: false,
                }, () => {
                    if (Network.refreshList) {
                        Network.refreshList();
                    }
                });
            })
            .catch(err => {
                this.setState({
                    // loading: false,
                }, () => {
                    setTimeout(() => {
                        Alert.alert(Config.appName, err);
                    }, 100);
                });
            });
        });
    };

  render() {

    const { selectedItems } = this.state;

    if (this.state.loading) {
        return (<View style={{
            flex: 1,
            width: Common.getLengthByIPhone7(0),
            // height: Dimensions.get('window').height,
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor: 'white',
          }}>
                <Spinner
                    visible={this.state.loading}
                    // textContent={'Загрузка...'}
                    onShow={() => {
                        Animated.loop(
                        Animated.timing(
                            this.spinValue,
                            {
                            toValue: 1,
                            duration: 1000,
                            easing: Easing.linear,
                            useNativeDriver: true
                            }
                        )
                        ).start();
                    }}
                    onDismiss={() => {

                    }}
                    customIndicator={<View style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        zIndex: 20000,
                    }}>
                        <View style={{
                        width: Common.getLengthByIPhone7(65),
                        height: Common.getLengthByIPhone7(65),
                        alignItems: 'center',
                        justifyContent: 'center',
                        }}>
                        <Animated.Image
                            style={{
                            width: Common.getLengthByIPhone7(65),
                            height: Common.getLengthByIPhone7(65),
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{rotate: this.spinValue.interpolate({
                                inputRange: [0, 1],
                                outputRange: ['0deg', '360deg']
                            })}] 
                            }}
                            source={require('./../assets/ic-loader.png')}
                        />
                        <TouchableOpacity style={{
                            // marginTop: Common.getLengthByIPhone7(20),
                            padding: Common.getLengthByIPhone7(5),
                            backgroundColor: 'rgba(255, 255, 255, 0.8)',
                            borderRadius: Common.getLengthByIPhone7(5),
                        }}
                        onPress={() => {
                            this.setState({
                            loading: false,
                            });
                        }}>
                            <Image
                            style={{
                                width: Common.getLengthByIPhone7(18),
                                height: Common.getLengthByIPhone7(18),
                            }}
                            source={require('./../assets/ic-loader-close.png')}
                            />
                        </TouchableOpacity>
                        </View>
                    </View>}
                    overlayColor={'rgba(32, 42, 91, 0.3)'}
                    textStyle={{color: '#FFF'}}
                    />
          </View>);
    }

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                // marginTop: 100,//Platform.OS === 'ios' ? Common.getLengthByIPhone7(25) : 0,//Common.getLengthByIPhone7(60),
                marginTop: (isIphoneX() ? Common.getLengthByIPhone7(105) : Common.getLengthByIPhone7(65)),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            }}>
                <Text style={{
                    // marginTop: Common.getLengthByIPhone7(65),
                    // marginTop: (isIphoneX() ? Common.getLengthByIPhone7(105) : Common.getLengthByIPhone7(65)),
                    maxWidth: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                    color: Colors.textColor,
                    fontFamily: 'Montserrat-Bold',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(24),
                    textDecorationLine: 'underline',
                }}
                allowFontScaling={false}>
                    {this.state.value}
                </Text>
                <TouchableOpacity style={{
                    // marginLeft: Common.getLengthByIPhone7(10),
                    width: Common.getLengthByIPhone7(35),
                    height: Common.getLengthByIPhone7(35),
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
                onPress={() => {
                    this.setState({
                        visible: true,
                    });
                }}>
                    <Image
                        source={require('./../assets/ic-pen.png')}
                        style={{
                            width: Common.getLengthByIPhone7(15),
                            height: Common.getLengthByIPhone7(15),
                            resizeMode: 'contain',
                            // tintColor: '#424347',
                        }}
                    />
                </TouchableOpacity>
            </View>
            <View style={{
                marginTop: Common.getLengthByIPhone7(5),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
            }}>
                <Text style={{
                    color: Colors.textColor,
                    fontFamily: 'Montserrat-Regular',
                    fontWeight: 'normal',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(10),
                }}
                allowFontScaling={false}>
                    12 событий
                </Text>
            </View>
            <ScrollView style={{
                width: Common.getLengthByIPhone7(0),
                flex: 1,
                paddingBottom: Common.getLengthByIPhone7(20),
                marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? 86 : 52) : 52,
            }}
            contentContainerStyle={{
                alignItems: 'center',
            }}>
                <MultiSelect
                    styleMainWrapper={{
                        marginTop: Common.getLengthByIPhone7(20),
                        minHeight: Common.getLengthByIPhone7(50),
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                        // backgroundColor: 'red',
                    }}
                    hideTags
                    items={this.state.courts}
                    uniqueKey="id"
                    ref={component => { this.multiSelect1 = component }}
                    onSelectedItemsChange={this.onSelectedItemsChange}
                    selectedItems={this.state.selectedItems}
                    selectText="Суд"
                    searchInputPlaceholderText="Поиск..."
                    onChangeInput={ (text)=> console.log(text)}
                    altFontFamily="Montserrat-Regular"
                    tagRemoveIconColor={Colors.mainColor}
                    tagBorderColor={Colors.mainColor}
                    tagTextColor={Colors.mainColor}
                    tagContainerStyle={{
                        maxWidth: Common.getLengthByIPhone7(300),
                        // minHeight: Common.getLengthByIPhone7(30),
                    }}
                    selectedItemTextColor="#CCC"
                    selectedItemIconColor="#CCC"
                    itemTextColor="#000"
                    displayKey="text"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Готово"
                />
                <View style={{
                    width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                }}>
                    {this.state.courtsView}
                </View>
                <MultiSelect
                    styleMainWrapper={{
                        marginTop: Common.getLengthByIPhone7(20),
                        minHeight: Common.getLengthByIPhone7(50),
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                        // backgroundColor: 'red',
                    }}
                    hideTags
                    items={this.state.categories}
                    uniqueKey="id"
                    ref={(component) => { this.multiSelect2 = component }}
                    onSelectedItemsChange={this.onSelectedCategoriesChange}
                    selectedItems={this.state.selectedCategories}
                    selectText="Категория дела"
                    searchInputPlaceholderText="Поиск..."
                    onChangeInput={ (text)=> console.log(text)}
                    altFontFamily="Montserrat-Regular"
                    tagRemoveIconColor={Colors.mainColor}
                    tagBorderColor={Colors.mainColor}
                    tagTextColor={Colors.mainColor}
                    tagContainerStyle={{
                        maxWidth: Common.getLengthByIPhone7(300),
                        // minHeight: Common.getLengthByIPhone7(30),
                    }}
                    selectedItemTextColor="#CCC"
                    selectedItemIconColor="#CCC"
                    itemTextColor="#000"
                    displayKey="value"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Готово"
                />
                <View style={{
                    width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                }}>
                    {this.multiSelect2 !== null ? this.multiSelect2.getSelectedItemsExt(this.state.selectedCategories) : null}
                </View>
                <MultiSelect
                    styleMainWrapper={{
                        marginTop: Common.getLengthByIPhone7(20),
                        minHeight: Common.getLengthByIPhone7(50),
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                        // backgroundColor: 'red',
                    }}
                    hideTags
                    items={this.state.instances}
                    uniqueKey="id"
                    ref={(component) => { this.multiSelect3 = component }}
                    onSelectedItemsChange={this.onSelectedInstancesChange}
                    selectedItems={this.state.selectedInstances}
                    selectText="Инстанция"
                    searchInputPlaceholderText="Поиск..."
                    onChangeInput={ (text)=> console.log(text)}
                    altFontFamily="Montserrat-Regular"
                    tagRemoveIconColor={Colors.mainColor}
                    tagBorderColor={Colors.mainColor}
                    tagTextColor={Colors.mainColor}
                    tagContainerStyle={{
                        maxWidth: Common.getLengthByIPhone7(300),
                        // minHeight: Common.getLengthByIPhone7(30),
                    }}
                    selectedItemTextColor="#CCC"
                    selectedItemIconColor="#CCC"
                    itemTextColor="#000"
                    displayKey="value"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Готово"
                />
                <View style={{
                    width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                }}>
                    {this.multiSelect3 !== null ? this.multiSelect3.getSelectedItemsExt(this.state.selectedInstances) : null}
                </View>
                <CaseList
                    onOpen={url => {
                        this.setState({
                            url: encodeURI(url),
                        }, () => {
                            this.openBrowser();
                        });
                    }}
                    navigation={this.props.navigation}
                    data={this.state.data.documents}
                />
            </ScrollView>
            <Dialog.Container visible={this.state.visible}>
                <Dialog.Title>Переименовать ключевое слово</Dialog.Title>
                <Dialog.Input value={this.state.value} onChangeText={text => this.setState({value : text})} />
                <Dialog.Button label="Отмена" onPress={() => {
                    this.setState({
                        visible: false,
                    });
                }} />
                <Dialog.Button label="Сохранить" onPress={() => {
                    this.setState({
                        loading: true,
                    }, () => {
                        if (this.state.value.length) {
                            let body = {
                                id: this.state.data.id,
                                value: this.state.value,
                                courts: this.state.data.courts,
                                categories: this.state.data.categories,
                                instances: this.state.data.instances,
                            };
                            editKeyword(body)
                            .then(() => {
                                this.refreshData();
                                this.setState({
                                    loading: false,
                                    visible: false,
                                }, () => {
                                    if (Network.refreshList) {
                                        Network.refreshList();
                                    }
                                });
                            })
                            .catch(err => {
                                this.setState({
                                    loading: false,
                                }, () => {
                                    setTimeout(() => {
                                        Alert.alert(Config.appName, err);
                                    }, 100);
                                });
                            });
                        } else {
                            Alert.alert(Config.appName, 'Укажите название ключевого слова!');
                        }
                    });
                }} />
            </Dialog.Container>
      </View>);
  }
}
