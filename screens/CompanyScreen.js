import React from 'react';
import {
  Dimensions,
  Text,
  View,
  BackHandler,
  Animated,
  Easing,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  Alert,
  Image,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, {getCompany} from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import CaseList from './../components/Company/CaseList';
import { isIphoneX } from 'react-native-iphone-x-helper';
import Toast from 'react-native-simple-toast';
import Clipboard from '@react-native-clipboard/clipboard';

export default class CompanyScreen extends React.Component {

  calculate = true;
  top = new Animated.Value(0);
  spinValue = new Animated.Value(0);

  state = {
    loading: false,
    data: null,
    showPdf: false,
  };

  UNSAFE_componentWillMount() {

    BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
            this.props.navigation.goBack(null);
            return true;
        }
    );

    this.setState({
        loading: true,
    }, () => {
        getCompany(this.props.navigation.state.params.data.id)
        .then(data => {
            this.setState({
                loading: false,
                data,
            });
        })
        .catch(err => {
            this.setState({
                loading: false,
            });
        });
    });
    if (Network.refreshList) {
        Network.refreshList();
    }
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  renderField = (title, value, style, onCopy) => {
      return (<View style={[{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          minHeight: Common.getLengthByIPhone7(61),
          borderBottomWidth: 1,
          borderBottomColor: '#f5f5f5',
          justifyContent: 'space-between',
          alignItems: 'flex-start',
      }, style]}>
        <Text style={{
            marginTop: Common.getLengthByIPhone7(10),
            color: '#999999',
            fontFamily: 'Montserrat-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(12),
            lineHeight: Common.getLengthByIPhone7(15),
        }}
        allowFontScaling={false}>
            {title}
        </Text>
        <View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
        }}>
            <Text style={{
                marginBottom: Common.getLengthByIPhone7(9),
                color: '#3e3e3e',
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(16),
                lineHeight: Common.getLengthByIPhone7(20),
            }}
            allowFontScaling={false}>
                {value}
            </Text>
            <TouchableOpacity style={{
                marginLeft: Common.getLengthByIPhone7(10),
            }}
            onPress={() => {
                if (onCopy) {
                    onCopy();
                }
            }}>
                <Image
                    source={require('./../assets/ic-copy.png')}
                    style={{
                        width: Common.getLengthByIPhone7(17),
                        height: Common.getLengthByIPhone7(17),
                        resizeMode: 'contain',
                        tintColor: Colors.textColor,
                    }}
                />
            </TouchableOpacity>
        </View>
      </View>);
  }

  render() {

    if (this.state.loading) {
        return (<View style={{
            flex: 1,
            width: Common.getLengthByIPhone7(0),
            // height: Dimensions.get('window').height,
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor: 'white',
          }}>
                <Spinner
                    visible={this.state.loading}
                    // textContent={'Загрузка...'}
                    onShow={() => {
                        Animated.loop(
                        Animated.timing(
                            this.spinValue,
                            {
                            toValue: 1,
                            duration: 1000,
                            easing: Easing.linear,
                            useNativeDriver: true
                            }
                        )
                        ).start();
                    }}
                    onDismiss={() => {

                    }}
                    customIndicator={<View style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        zIndex: 20000,
                    }}>
                        <View style={{
                        width: Common.getLengthByIPhone7(65),
                        height: Common.getLengthByIPhone7(65),
                        alignItems: 'center',
                        justifyContent: 'center',
                        }}>
                        <Animated.Image
                            style={{
                            width: Common.getLengthByIPhone7(65),
                            height: Common.getLengthByIPhone7(65),
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{rotate: this.spinValue.interpolate({
                                inputRange: [0, 1],
                                outputRange: ['0deg', '360deg']
                            })}] 
                            }}
                            source={require('./../assets/ic-loader.png')}
                        />
                        <TouchableOpacity style={{
                            // marginTop: Common.getLengthByIPhone7(20),
                            padding: Common.getLengthByIPhone7(5),
                            backgroundColor: 'rgba(255, 255, 255, 0.8)',
                            borderRadius: Common.getLengthByIPhone7(5),
                        }}
                        onPress={() => {
                            this.setState({
                            loading: false,
                            });
                        }}>
                            <Image
                            style={{
                                width: Common.getLengthByIPhone7(18),
                                height: Common.getLengthByIPhone7(18),
                            }}
                            source={require('./../assets/ic-loader-close.png')}
                            />
                        </TouchableOpacity>
                        </View>
                    </View>}
                    overlayColor={'rgba(32, 42, 91, 0.3)'}
                    textStyle={{color: '#FFF'}}
                    />
          </View>);
    }

    // return null;

    if (this.state.data === null) {
        return null;
    }

    // Alert.alert('dsdd', this.state.data.subscribed);

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
            <Text style={{
                marginTop: (isIphoneX() ? Common.getLengthByIPhone7(105) : Common.getLengthByIPhone7(65)),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: Colors.textColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(24),
            }}
            allowFontScaling={false}>
                {this.state.data.value}
            </Text>
            <View style={{
                marginTop: Common.getLengthByIPhone7(5),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
            }}>
                <Text style={{
                    color: Colors.textColor,
                    fontFamily: 'Montserrat-Regular',
                    fontWeight: 'normal',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(10),
                }}
                allowFontScaling={false}>
                    {Common.getDigitStr(this.state.data.cases.length, {d1: ' судебное дело', d2_d4: ' судебных дела', d5_d10: ' судебных дел', d11_d19: ' судебных дел'})}
                </Text>
            </View>
            <ScrollView style={{
                width: Common.getLengthByIPhone7(0),
                flex: 1,
                paddingBottom: Common.getLengthByIPhone7(20),
                marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? 86 : 52) : 52,
            }}
            contentContainerStyle={{
                alignItems: 'center',
            }}>
                {this.renderField('ОГРН', this.state.data.data.ogrn, {marginTop: Common.getLengthByIPhone7(18)}, () => {
                    Clipboard.setString(this.state.data.data.ogrn);
                    Toast.show('ОГРН скопирован');
                })}
                {this.renderField('ИНН/КПП', this.state.data.data.inn, {}, () => {
                    Clipboard.setString(this.state.data.data.inn);
                    Toast.show('ИНН/КПП скопирован');
                })}
                <TouchableOpacity style={{
                    marginTop: Common.getLengthByIPhone7(20),
                    // width: Common.getLengthByIPhone7(200),
                    // height: Common.getLengthByIPhone7(40),
                    // borderRadius: Common.getLengthByIPhone7(10),
                    // borderWidth: 1,
                    // borderColor: Colors.textColor,
                    // alignItems: 'center',
                    // justifyContent: 'center'
                }}
                onPress={() => {
                    this.props.navigation.navigate('CompanyInfo', {data: this.state.data});
                }}>
                    <Text style={{
                        color: Colors.textColor,
                        fontFamily: 'Montserrat-Regular',
                        fontWeight: 'normal',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(13),
                        lineHeight: Common.getLengthByIPhone7(16),
                    }}
                    allowFontScaling={false}>
                        Подробнее
                    </Text>
                </TouchableOpacity>
                <CaseList
                    navigation={this.props.navigation}
                    openUrl={url => {
                        this.setState({
                            url,
                            showPdf: true,
                        });
                    }}
                    status={this.state.data.status}
                    data={this.state.data.cases}
                />  
            </ScrollView>
      </View>);
    //   {this.renderField('Дата регистрации', Common.getRusDate(new Date(this.state.data.data.state.registration_date)), {})}
  }
}
