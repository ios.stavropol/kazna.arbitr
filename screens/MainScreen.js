import React from 'react';
import {
  Text,
  View,
  BackHandler,
  Alert,
  StatusBar,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Platform,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, { newSubscribtion, setPushId, getDelays } from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import MainScrollView from './../components/Main/MainScrollView';
import AddItemModalView from './../components/Main/AddItemModalView';
import AddKeywordModalView from './../components/Main/AddKeywordModalView';
import AddCompanyModalView from './../components/Main/AddCompanyModalView';
import CalendarModalView from './../components/Main/CalendarModalView';
import DelayModalView from './../components/Main/DelayModalView';
import TarifModalView from './../components/Main/TarifModalView';
import OneSignal from 'react-native-onesignal';
import NavigationService from './../Utilites/NavigationService';

@observer
export default class MainScreen extends React.Component {

  spinValue = new Animated.Value(0);

  state = {
    refresh: false,
    show: false,
    data: [],
    storyIndex: 0,
    storiesShow: false,
    showCompany: false,
    showCalendar: false,
    showKeyword: false,
    loading: false,
    showTarif: false,
  };

  UNSAFE_componentWillMount() {
    this.props.navigation.addListener('willFocus', () => {
      BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
          BackHandler.exitApp()
          return true;
        }
      );
      if (Platform.OS === 'ios') {
        StatusBar.setBarStyle('dark-content', true);
      }
    });

    this.refreshDelay();
    setInterval(() => {
      this.refreshDelay();
    }, 60*5000);

    if (!Network.userProfile.is_tarif_active) {
      this.setState({
        showTarif: true,
      });
    }
    /* O N E S I G N A L   S E T U P */
    OneSignal.setAppId("ea4c198c-ce69-4724-bbc4-22528e581180");
    OneSignal.setLogLevel(6, 0);
    OneSignal.setRequiresUserPrivacyConsent(false);
    OneSignal.promptForPushNotificationsWithUserResponse(response => {
      console.warn('promptForPushNotificationsWithUserResponse: '+JSON.stringify(response));
        // this.OSLog("Prompt response:", response);
    });

    /* O N E S I G N A L  H A N D L E R S */
    OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
      // console.warn('setNotificationWillShowInForegroundHandler: '+JSON.stringify(notificationReceivedEvent));
      // console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
      // let notification = notificationReceivedEvent.getNotification();
      // console.log("notification: ", notification);
      // const data = notification.additionalData
      // console.log("additionalData: ", data);
      // //Silence notification by calling complete() with no argument
      // notificationReceivedEvent.complete(notification);
      // Alert.alert('aaaa', 'ffff');
      if (Network.refreshChat) {
        Network.refreshChat();
      }
        
    });
    OneSignal.setNotificationOpenedHandler(notification => {
      console.warn('setNotificationOpenedHandler: '+JSON.stringify(notification));
      console.warn('company: ' + notification.notification.additionalData.company);
      if (Network.refreshChat) {
        Network.refreshChat();
      }
      if (notification.notification.additionalData.type === 'company') {
        NavigationService.navigate('Company', {data: {id: notification.notification.additionalData.id}});
      } else if (notification.notification.additionalData.type === 'message') {
        NavigationService.navigate('Chat');
      } else if (notification.notification.additionalData.type === 'case') {
        NavigationService.navigate('Item', {data: {id: notification.notification.additionalData.id}});
      } else if (notification.notification.additionalData.type === 'keyword') {
        NavigationService.navigate('Word', {data: {id: notification.notification.additionalData.id}});
      }
    });
    OneSignal.setInAppMessageClickHandler(event => {
      console.warn('setInAppMessageClickHandler: '+JSON.stringify(event));
        // this.OSLog("OneSignal IAM clicked:", event);
    });
    OneSignal.addEmailSubscriptionObserver((event) => {
      console.warn('addEmailSubscriptionObserver: '+JSON.stringify(event));
        // this.OSLog("OneSignal: email subscription changed: ", event);
    });
    OneSignal.addSubscriptionObserver(event => {
      console.warn('addSubscriptionObserver: '+JSON.stringify(event));
        // this.OSLog("OneSignal: subscription changed:", event);
        // this.setState({ isSubscribed: event.to.isSubscribed})
    });
    OneSignal.addPermissionObserver(event => {
      console.warn('addPermissionObserver: '+JSON.stringify(event));
        // this.OSLog("OneSignal: permission changed:", event);
    });

    OneSignal.getDeviceState()
    .then(device => {
      console.warn('getDeviceState: '+JSON.stringify(device));
      Network.push_token = device.userId;
      if (Network.access_token != null && Network.access_token.length) {
        setPushId(device.userId)
        .then(() => {

        })
        .catch(err => {

        });
      }
    })
    .catch(err => {

    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  refreshDelay = () => {
    getDelays()
    .then(() => {

    })
    .catch(err => {

    });
  }

  render() {

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
        <MainScrollView
          navigation={this.props.navigation}
          onCalendarShow={() => {
            this.setState({
              showCalendar: true,
            });
          }}
          onDelayShow={() => {
            this.setState({
              showDelay: true,
            });
          }}
          onClick={index => {
            // console.warn('index: '+index);
            if (index === 'company') {
              this.setState({
                showCompany: true,
              });
            } else if (index === 'keyword') {
              this.setState({
                showKeyword: true,
              });
            } else {
              this.setState({
                show: true,
                index,
              });
            }
          }}
        />
        <AddItemModalView
          show={this.state.show}
          index={this.state.index}
          onClose={() => {
            this.setState({
              show: false,
            });
          }}
          onScan={() => {
            this.setState({
              show: false,
            }, () => {
              this.props.navigation.navigate('Scanner', {action: case_id => {
                newSubscribtion('case', case_id)
                .then(() => {
                    if (Network.refreshList != null) {
                        Network.refreshList();
                    }
                    this.setState({
                        loading: false,
                        text: '',
                    }, () => {
                        
                    });
                })
                .catch(err => {
                    this.setState({
                        loading: false,
                    }, () => {
                        setTimeout(() => {
                            Alert.alert(Config.appName, err);
                        }, 100);
                    });
                });
              }});
            });
          }}
        />
        <AddKeywordModalView
          show={this.state.showKeyword}
          onClose={() => {
            this.setState({
              showKeyword: false,
            });
          }}
          onAdd={text => {
            this.setState({
              showKeyword: false,
            }, () => {
              setTimeout(() => {
                this.setState({
                  loading: true,
                }, () => {
                  newSubscribtion('keyword', text)
                  .then(() => {
                      if (Network.refreshList != null) {
                          Network.refreshList();
                      }
                      this.setState({
                          loading: false,
                          showKeyword: false,
                      }, () => {
                          
                      });
                  })
                  .catch(err => {
                      this.setState({
                          loading: false,
                      }, () => {
                          setTimeout(() => {
                              Alert.alert(Config.appName, err);
                          }, 100);
                      });
                  });
                });
              }, 500);
            });
          }}
        />
        <AddCompanyModalView
          show={this.state.showCompany}
          onClose={() => {
            this.setState({
              showCompany: false,
            });
          }}
        />
        <CalendarModalView
          show={this.state.showCalendar}
          onClose={() => {
            this.setState({
              showCalendar: false,
            });
          }}
          onClickCase={case_id => {
            this.setState({
              showCalendar: false,
            }, () => {
              this.props.navigation.navigate('Item', {data: {id: case_id}});
            });
          }}
        />
        <Spinner
          visible={this.state.loading}
          // textContent={'Загрузка...'}
          onShow={() => {
            Animated.loop(
              Animated.timing(
                this.spinValue,
                {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear,
                useNativeDriver: true
                }
              )
            ).start();
          }}
          onDismiss={() => {

          }}
          customIndicator={<View style={{
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 20000,
          }}>
            <View style={{
              width: Common.getLengthByIPhone7(65),
              height: Common.getLengthByIPhone7(65),
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <Animated.Image
                style={{
                  width: Common.getLengthByIPhone7(65),
                  height: Common.getLengthByIPhone7(65),
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  transform: [{rotate: this.spinValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '360deg']
                  })}] 
                }}
                source={require('./../assets/ic-loader.png')}
              />
              <TouchableOpacity style={{
                // marginTop: Common.getLengthByIPhone7(20),
                padding: Common.getLengthByIPhone7(5),
                backgroundColor: 'rgba(255, 255, 255, 0.8)',
                borderRadius: Common.getLengthByIPhone7(5),
              }}
              onPress={() => {
                this.setState({
                  loading: false,
                });
              }}>
                <Image
                  style={{
                    width: Common.getLengthByIPhone7(18),
                    height: Common.getLengthByIPhone7(18),
                  }}
                  source={require('./../assets/ic-loader-close.png')}
                />
              </TouchableOpacity>
            </View>
          </View>}
          overlayColor={'rgba(32, 42, 91, 0.3)'}
          textStyle={{color: '#FFF'}}
        />
        <TarifModalView
          show={this.state.showTarif}
          onSuccess={() => {
            this.setState({
              showTarif: false,
            });
          }}
          onClose={() => {
            this.setState({
              showTarif: false,
            });
          }}
        />
        <DelayModalView
          show={this.state.showDelay}
          onOpen={data => {
            this.setState({
              showDelay: false,
            }, () => {
              this.props.navigation.navigate('Item', {data: data});
            });
          }}
          onClose={() => {
            this.setState({
              showDelay: false,
            });
          }}
        />
      </View>);
  }
}
