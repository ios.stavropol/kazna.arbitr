import React from 'react';
import {
  Dimensions,
  Text,
  View,
  Alert,
  BackHandler,
  TouchableOpacity,
  Image,
  Linking,
  Animated,
  Easing,
  Keyboard,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, {getCode} from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import AnimatedButton from '../components/Buttons/AnimatedButton';
import PhoneInput, {isValidNumber} from "react-native-phone-number-input";

export default class LoginScreen extends React.Component {

  calculate = true;
  top = new Animated.Value(0);
  spinValue = new Animated.Value(0);

  state = {
    phoneFormated: '',
    phoneRaw: '',
    loading: false,
    buttonY: 0,
    buttonH: 0,
    viewY: 0,
    viewH: 0,
  };

  UNSAFE_componentWillMount() {
    this.props.navigation.addListener('willFocus', this.willFocus)
    this.props.navigation.addListener('willBlur', this.willBlur)
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  willFocus = () => {
    Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    BackHandler.addEventListener(
      "hardwareBackPress",
      () => {
          return true;
      }
    );
  }

  willBlur = () => {
    Keyboard.removeListener('keyboardDidShow');
    Keyboard.removeListener('keyboardDidHide');
  }

  _keyboardDidHide = () => {
    this.calculate = false;
    Animated.timing(this.top, {
      toValue: (Dimensions.get('window').height - this.state.viewH)/2,
      duration: 300,
    }).start();
  }

  _keyboardDidShow = e => {
    this.calculate = false;
    let kbView = (Dimensions.get('window').height - e.endCoordinates.height);
    let view = this.state.buttonY+this.state.viewY;
    if (view + Common.getLengthByIPhone7(50) > kbView) {
      Animated.timing(this.top, {
        toValue: (Dimensions.get('window').height - this.state.viewH)/2 - (view + Common.getLengthByIPhone7(50) - kbView),
        duration: 300,
      }).start();
    }
  }

  nextClick = phone => {
    this.setState({
      loading: true,
    }, () => {
      getCode(phone)
      .then(() => {
        this.setState({
          loading: false,
        }, () => {
          this.props.navigation.navigate('Code', {phone: phone});
        });
      })
      .catch(err => {
        this.setState({
          loading: false,
        }, () => {
          setTimeout(() => {
            Alert.alert(Config.appName, err);
          }, 100);
        });
      });
    });
  }

  onLayout = e => {
    if (this.calculate) {
      this.setState({
        buttonH: e.nativeEvent.layout.height,
        buttonY: e.nativeEvent.layout.y,
      })
    }
  }

  onLayout2 = e => {
    if (this.calculate) {
      this.setState({
        viewY: e.nativeEvent.layout.y,
        viewH: e.nativeEvent.layout.height,
      }, () => {
        Animated.timing(this.top, {
          toValue: (Dimensions.get('window').height - this.state.viewH)/2,
          duration: 0,
        }).start();
      })
    }
  }

  render() {

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
        <Animated.View style={{
          width: Common.getLengthByIPhone7(343),
          borderRadius: Common.getLengthByIPhone7(25),
          paddingLeft: Common.getLengthByIPhone7(30),
          paddingRight: Common.getLengthByIPhone7(30),
          paddingTop: Common.getLengthByIPhone7(30),
          paddingBottom: Common.getLengthByIPhone7(30),
          backgroundColor: 'white',
          shadowColor: '#D7DEEC',
          shadowOffset: { width: 0, height: Common.getLengthByIPhone7(5) },
          shadowOpacity: 1,
          shadowRadius: Common.getLengthByIPhone7(20),
          elevation: 6,
          alignItems: 'center',
          position: 'absolute',
          top: this.top,
        }}
        onLayout={this.onLayout2}>
          <Text style={{
            color: Colors.textColor,
            fontFamily: 'Rubik',
            fontWeight: '500',
            textAlign: 'center',
            fontSize: Common.getLengthByIPhone7(24),
          }}
          allowFontScaling={false}>
              {`Вход`}
          </Text>
          <View style={{
              width: Common.getLengthByIPhone7(283),
              height: Common.getLengthByIPhone7(50),
              borderRadius: Common.getLengthByIPhone7(15),
              borderWidth: 1,
              borderColor: Colors.grayBorderColor,
              marginTop: Common.getLengthByIPhone7(30),
              overflow: 'hidden',
            }}>
            <PhoneInput
              textInputStyle={{
                // color: 'red',
                height: 46,
                // fontSize: 100,
              }}
              containerStyle={{
                // marginBottom: 40,
                // backgroundColor: 'red'
              }}
              textContainerStyle={{
                // paddingHorizontal: 0,
                paddingVertical: 2,
              }}
              ref={this.phoneInput}
              defaultValue={''}
              defaultCode="RU"
              layout="first"
              placeholder={'Номер телефона'}
              onChangeText={text => {
                this.setState({
                  phoneRaw: text,
                });
              }}
              onChangeFormattedText={text => {
                this.setState({
                  phoneFormated: text,
                });
              }}
              // withDarkTheme
              // withShadow
              autoFocus
            />
          </View>
          <AnimatedButton style={{
            width: Common.getLengthByIPhone7(283),
            height: Common.getLengthByIPhone7(50),
            marginTop: Common.getLengthByIPhone7(20),
            marginBottom: Common.getLengthByIPhone7(20),
          }}
          onLayout={this.onLayout}
          onPress={() => {
            if (isValidNumber(this.state.phoneFormated)) {
              let phone = this.state.phoneFormated.replace(/[^\d.-]/g, '');
              this.nextClick(phone);
            } else {
              Alert.alert(Config.appName, 'Введите корректный номер телефона!');
            }
          }}
          title={'Далее'}
          activeColor={Colors.mainColor}
          inactiveColor={'white'}/>
          <Text style={{
            color: Colors.textColor,
            fontFamily: 'Rubik',
            fontWeight: 'normal',
            textAlign: 'center',
            fontSize: Common.getLengthByIPhone7(14),
          }}
          allowFontScaling={false}>
              {`Нажимая на кнопку, вы принимаете `}<Text style={{
            color: Colors.textColor,
            fontFamily: 'Rubik',
            fontWeight: 'normal',
            textAlign: 'center',
            fontSize: Common.getLengthByIPhone7(14),
            textDecorationLine: 'underline',
          }}
          onPress={() => {
            let url = 'https://kazna.tech/politics';
            Linking.canOpenURL(url).then(supported => {
              if (supported) {
                Linking.openURL(url);
              } else {
                console.log("Don't know how to open URI: " + this.props.url);
              }
            });
          }}
          allowFontScaling={false}>
              {`соглашение с политикой персональных данных`}
          </Text>
          </Text>
        </Animated.View>
        <Spinner
          visible={this.state.loading}
          // textContent={'Загрузка...'}
          onShow={() => {
            Animated.loop(
              Animated.timing(
                this.spinValue,
                {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear,
                useNativeDriver: true
                }
              )
            ).start();
          }}
          onDismiss={() => {

          }}
          customIndicator={<View style={{
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 20000,
          }}>
            <View style={{
              width: Common.getLengthByIPhone7(65),
              height: Common.getLengthByIPhone7(65),
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <Animated.Image
                style={{
                  width: Common.getLengthByIPhone7(65),
                  height: Common.getLengthByIPhone7(65),
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  transform: [{rotate: this.spinValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '360deg']
                  })}] 
                }}
                source={require('./../assets/ic-loader.png')}
              />
              <TouchableOpacity style={{
                // marginTop: Common.getLengthByIPhone7(20),
                padding: Common.getLengthByIPhone7(5),
                backgroundColor: 'rgba(255, 255, 255, 0.8)',
                borderRadius: Common.getLengthByIPhone7(5),
              }}
              onPress={() => {
                this.setState({
                  loading: false,
                });
              }}>
                <Image
                  style={{
                    width: Common.getLengthByIPhone7(18),
                    height: Common.getLengthByIPhone7(18),
                  }}
                  source={require('./../assets/ic-loader-close.png')}
                />
              </TouchableOpacity>
            </View>
          </View>}
          overlayColor={'rgba(32, 42, 91, 0.3)'}
          textStyle={{color: '#FFF'}}
        />
      </View>);

{/* <ImageBackground
source={require('./../assets/splash.png')}
style={{
  width: Common.getLengthByIPhone7(0),
  flex: 1,
  // height: Dimensions.get('window').height,
  resizeMode: 'cover',
  alignItems: 'center',
  justifyContent: 'center',
}}> */}
  }
}
