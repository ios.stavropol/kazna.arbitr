import React from 'react';
import {
  Text,
  View,
  BackHandler,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network from './../Utilites/Network';

export default class OkvedsScreen extends React.Component {

  state = {
    data: null,
  };

  UNSAFE_componentWillMount() {

    BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
            this.props.navigation.goBack(null);
            return true;
        }
    );

    this.setState({
        data: this.props.navigation.state.params.data,
    }, () => {
        
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  renderField = title => {
        return (<View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            borderBottomWidth: 1,
            borderBottomColor: 'rgba(0, 0, 0, 0.2)',
            marginTop: Common.getLengthByIPhone7(15),
            paddingBottom: Common.getLengthByIPhone7(15),
        }}>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(15),
                marginTop: Common.getLengthByIPhone7(10),
            }}
            allowFontScaling={false}>
                {title}
            </Text>
        </View>);
    }

  render() {

    if (this.state.data === null || this.state.data.length === 0) {
        return (<View style={{
            flex: 1,
            width: Common.getLengthByIPhone7(0),
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white',
          }}>
                <Text style={{
                    color: 'black',
                    fontFamily: 'Montserrat-Regular',
                    fontWeight: 'normal',
                    textAlign: 'center',
                    fontSize: Common.getLengthByIPhone7(16),
                    // lineHeight: Common.getLengthByIPhone7(15),
                }}
                allowFontScaling={false}>
                    Данных нет
                </Text>
          </View>);
    }

    let okveds = [];

    if (this.state.data !== null) {
        for (let i = 0; i < this.state.data.length; i++) {
            okveds.push(this.renderField(this.state.data[i].name + ' (' + this.state.data[i].code + ')'));
        }
    }

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
        // marginTop: Common.getLengthByIPhone7(18)
      }}>
            <Text style={{
                marginBottom: Common.getLengthByIPhone7(15),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: Colors.textColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(24),
            }}
            allowFontScaling={false}>
                Виды деятельности
            </Text>
            <ScrollView style={{
                width: Common.getLengthByIPhone7(0),
                flex: 1,
            }}
            contentContainerStyle={{
                alignItems: 'center',
            }}>
                {okveds}
            </ScrollView>
      </View>);
  }
}
