import React from 'react';
import {
  Dimensions,
  Text,
  View,
  Alert,
  BackHandler,
  ScrollView,
  Linking,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, {getNotifications} from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { isIphoneX } from 'react-native-iphone-x-helper';

export default class NotificationsScreen extends React.Component {

    spinValue = new Animated.Value(0);
    
  state = {
    loading: false,
    data: [],
  };

  UNSAFE_componentWillMount() {

    BackHandler.addEventListener(
      "hardwareBackPress",
      () => {
          this.props.navigation.goBack(null);
          return true;
      }
    );
    
    this.setState({
        loading: true,
    }, () => {
        getNotifications()
        .then(data => {
            this.setState({
                loading: false,
                data,
            }, () => {
                
            });
        })
        .catch(err => {
            this.setState({
                loading: false,
            }, () => {
                
            });
        });
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  renderCase = data => {
      return (<TouchableOpacity style={{
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
        borderBottomColor: '#e8e8e8',
        borderBottomWidth: 1,
        paddingTop: Common.getLengthByIPhone7(16),
        paddingBottom: Common.getLengthByIPhone7(16),
    }}
    onPress={() => {
        this.props.navigation.navigate('Item', {data: {id: data.case}});
    }}>
        <View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'space-between',
        }}>
            <View style={{

            }}>
                <Text style={{
                    maxWidth: Common.getLengthByIPhone7(200),
                    color: 'black',
                    fontFamily: 'Montserrat-Bold',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(16),
                    lineHeight: Common.getLengthByIPhone7(19),
                }}
                allowFontScaling={false}>
                    {data.text_header}
                </Text>
                <Text style={{
                    maxWidth: Common.getLengthByIPhone7(200),
                    color: 'black',
                    fontFamily: 'Montserrat-Regular',
                    fontWeight: 'normal',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(14),
                    lineHeight: Common.getLengthByIPhone7(17),
                }}
                allowFontScaling={false}>
                    {data.text_sub_header}
                </Text>
            </View>
            <Text style={{
                maxWidth: Common.getLengthByIPhone7(140),
                color: '#bdbdbd',
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'right',
                fontSize: Common.getLengthByIPhone7(9),
            }}
            allowFontScaling={false}>
                {data.meta}
            </Text>
        </View>
        <View style={{
            marginTop: Common.getLengthByIPhone7(10),
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
        }}>
            <Text style={{
                maxWidth: Common.getLengthByIPhone7(240),
                color: 'black',
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(14),
            }}
            allowFontScaling={false}>
                {data.text}
            </Text>
            {data.has_document ? (<TouchableOpacity style={{

            }}
            onPress={() => {
                Alert.alert(
                    Config.appName,
                    'Вы хотите открыть документ?',
                    [
                        {
                        text: "Нет",
                        onPress: () => console.log("Cancel Pressed"),
                        style: "cancel"
                        },
                        { text: "Да", onPress: () => {
                            let url = data.document_link;
                            Linking.canOpenURL(url).then(supported => {
                                if (supported) {
                                    Linking.openURL(url)
                                    .catch((err) => {
                                        console.warn(err);
                                    });
                                } else {
                                    Alert(Config.appName, 'Данный тип документа невозможно открыть!');
                                    console.log("Don't know how to open URI: " + this.props.url);
                                }
                            });
                        }}
                    ]
                );
            }}>
            <Image
                source={require('./../assets/ic-box.png')}
                style={{
                    // marginRight: Common.getLengthByIPhone7(51),
                    width: Common.getLengthByIPhone7(32),
                    height: Common.getLengthByIPhone7(32),
                    resizeMode: 'contain',
            }}/>
            </TouchableOpacity>) : null}
        </View>
    </TouchableOpacity>);
  }

  renderCompany = data => {
    return (<TouchableOpacity style={{
      width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
      borderBottomColor: '#e8e8e8',
      borderBottomWidth: 1,
      paddingTop: Common.getLengthByIPhone7(16),
      paddingBottom: Common.getLengthByIPhone7(16),
  }}
  onPress={() => {
    this.props.navigation.navigate('Company', {data: {id: data.company}});
  }}>
      <View style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
          flexDirection: 'row',
          alignItems: 'flex-start',
          justifyContent: 'space-between',
      }}>
          <View style={{

          }}>
              <Text style={{
                  maxWidth: Common.getLengthByIPhone7(200),
                  color: 'black',
                  fontFamily: 'Montserrat-Bold',
                  fontWeight: 'bold',
                  textAlign: 'left',
                  fontSize: Common.getLengthByIPhone7(16),
                  lineHeight: Common.getLengthByIPhone7(19),
              }}
              allowFontScaling={false}>
                  {data.text_header}
              </Text>
              <Text style={{
                  maxWidth: Common.getLengthByIPhone7(200),
                  color: 'black',
                  fontFamily: 'Montserrat-Regular',
                  fontWeight: 'normal',
                  textAlign: 'left',
                  fontSize: Common.getLengthByIPhone7(14),
                  lineHeight: Common.getLengthByIPhone7(17),
              }}
              allowFontScaling={false}>
                  {data.text_sub_header}
              </Text>
          </View>
          <Text style={{
              maxWidth: Common.getLengthByIPhone7(140),
              color: '#bdbdbd',
              fontFamily: 'Montserrat-Regular',
              fontWeight: 'normal',
              textAlign: 'right',
              fontSize: Common.getLengthByIPhone7(9),
          }}
          allowFontScaling={false}>
              {data.meta}
          </Text>
      </View>
      <Text style={{
          marginTop: Common.getLengthByIPhone7(10),
        maxWidth: Common.getLengthByIPhone7(240),
        color: 'black',
        fontFamily: 'Montserrat-Regular',
        fontWeight: 'normal',
        textAlign: 'left',
        fontSize: Common.getLengthByIPhone7(14),
    }}
    allowFontScaling={false}>
        {data.text}
    </Text>
  </TouchableOpacity>);
}

  render() {

    let rows = [];

    for (let i=0;i<this.state.data.length;i++) {
        if (this.state.data[i].case !== null) {
            rows.push(this.renderCase(this.state.data[i]));
        } else {
            rows.push(this.renderCompany(this.state.data[i]));
        }
    }
    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
            <Text style={{
                // marginTop: Common.getLengthByIPhone7(105),
                marginTop: (isIphoneX() ? Common.getLengthByIPhone7(105) : Common.getLengthByIPhone7(65)),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: Colors.textColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(24),
            }}
            allowFontScaling={false}>
                Уведомления
            </Text>
            <ScrollView style={{
                marginTop: Common.getLengthByIPhone7(20),
                width: Common.getLengthByIPhone7(0),
                flex: 1,
                marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? 86 : 52) : 52,
            }}
            contentContainerStyle={{
                alignItems: 'center',
            }}>
                {rows}
            </ScrollView>
            <Spinner
                visible={this.state.loading}
                // textContent={'Загрузка...'}
                onShow={() => {
                    Animated.loop(
                    Animated.timing(
                        this.spinValue,
                        {
                        toValue: 1,
                        duration: 1000,
                        easing: Easing.linear,
                        useNativeDriver: true
                        }
                    )
                    ).start();
                }}
                onDismiss={() => {

                }}
                customIndicator={<View style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    zIndex: 20000,
                }}>
                    <View style={{
                    width: Common.getLengthByIPhone7(65),
                    height: Common.getLengthByIPhone7(65),
                    alignItems: 'center',
                    justifyContent: 'center',
                    }}>
                    <Animated.Image
                        style={{
                        width: Common.getLengthByIPhone7(65),
                        height: Common.getLengthByIPhone7(65),
                        position: 'absolute',
                        bottom: 0,
                        left: 0,
                        transform: [{rotate: this.spinValue.interpolate({
                            inputRange: [0, 1],
                            outputRange: ['0deg', '360deg']
                        })}] 
                        }}
                        source={require('./../assets/ic-loader.png')}
                    />
                    <TouchableOpacity style={{
                        // marginTop: Common.getLengthByIPhone7(20),
                        padding: Common.getLengthByIPhone7(5),
                        backgroundColor: 'rgba(255, 255, 255, 0.8)',
                        borderRadius: Common.getLengthByIPhone7(5),
                    }}
                    onPress={() => {
                        this.setState({
                        loading: false,
                        });
                    }}>
                        <Image
                        style={{
                            width: Common.getLengthByIPhone7(18),
                            height: Common.getLengthByIPhone7(18),
                        }}
                        source={require('./../assets/ic-loader-close.png')}
                        />
                    </TouchableOpacity>
                    </View>
                </View>}
                overlayColor={'rgba(32, 42, 91, 0.3)'}
                textStyle={{color: '#FFF'}}
                />
      </View>);
  }
}
