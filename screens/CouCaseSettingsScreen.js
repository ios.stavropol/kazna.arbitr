import React from 'react';
import {
  Dimensions,
  Text,
  View,
  BackHandler,
  Animated,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  Platform,
  TextInput,
  Switch,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, { muteSidesCase, mutePushCase } from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { observer } from 'mobx-react';

@observer
export default class CouCaseSettingsScreen extends React.Component {

    calculate = true;
    top = new Animated.Value(0);

    state = {
        loading: false,
        data: null,
        selected: [],
        allSelected: [],
        pushed: false,
        all: false,
        search: '',
        rows: [],
    };

    UNSAFE_componentWillMount() {
        
        BackHandler.addEventListener(
            "hardwareBackPress",
            () => {
                this.props.navigation.goBack(null);
                return true;
            }
        );

        this.setState({
            data: this.props.navigation.state.params.data,
            pushed: this.props.navigation.state.params.data.muted_all === false ? true : false,
        }, () => {
			
        });
    }

    UNSAFE_componentWillUnmount() {

    }

    constructor(props) {
        super(props);

    }

    renderNonBankrot = () => {
        return (<View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            // marginLeft: Common.getLengthByIPhone7(20),
            marginTop: Common.getLengthByIPhone7(20),
            // marginBottom: Common.getLengthByIPhone7(20),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
            <Text style={{
              color: Colors.textColor,
              fontSize: Common.getLengthByIPhone7(17),
              lineHeight: Common.getLengthByIPhone7(17)*1.4,
              fontFamily: 'Montserrat',
              fontWeight: '600',
              textAlign: 'center',
            }}>
              Присылать уведомления
            </Text>
            <Switch
              onValueChange={value => {
                  this.setState({
                      pushed: value,
                  }, () => {
                        mutePushCase(this.state.data.id, !value)
                        .then(() => {
                            if (Network.refreshCase) {
                                Network.refreshCase();
                            }
                        })
                        .catch(err => {

                        });
                  });
              }}
              value={this.state.pushed}
            />
          </View>);
    }

    render() {

        let body = null;

        if (this.state.data !== null) {
			body = this.renderNonBankrot();
        }

        return (<View style={{
            flex: 1,
            width: Common.getLengthByIPhone7(0),
            // height: Dimensions.get('window').height,
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor: 'white',
        }}>
            <View style={{
                // flexDirection: 'row',
                // alignItems: 'center',
                // marginTop: 100,//Platform.OS === 'ios' ? Common.getLengthByIPhone7(25) : 0,//Common.getLengthByIPhone7(60),
                marginTop: (isIphoneX() ? Common.getLengthByIPhone7(105) : Common.getLengthByIPhone7(65)),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            }}>
                <Text style={{
                    // maxWidth: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                    color: Colors.textColor,
                    fontFamily: 'Montserrat-Bold',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(24),
                }}
                allowFontScaling={false}>
                    Настройки дела{`\n`}
                    <Text style={{
                        maxWidth: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                        color: Colors.textColor,
                        fontFamily: 'Montserrat-Regular',
                        fontWeight: 'normal',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(18),
                    }}
                    allowFontScaling={false}>
                        {this.state.data.name != null && this.state.data.name.length ? this.state.data.name : this.state.data.value}
                    </Text>
                </Text>
            </View>
            {body}
        </View>);
    }
}
