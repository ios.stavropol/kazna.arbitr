import React from 'react';
import {
  Dimensions,
  Text,
  View,
  Alert,
  Image,
  BackHandler,
  ImageBackground,
  Animated,
  Keyboard,
  TouchableOpacity,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network from './../Utilites/Network';

export default class SettingsScreen extends React.Component {

  calculate = true;
  top = new Animated.Value(0);

  state = {
    code: '',
    loading: false,
    buttonY: 0,
    buttonH: 0,
    viewY: 0,
    viewH: 0,
    firstname: '',
    lastname: '',
    email: '',
  };

  UNSAFE_componentWillMount() {

    BackHandler.addEventListener(
      "hardwareBackPress",
      () => {
          this.props.navigation.goBack(null);
          return true;
      }
    );

    let firstname = '';
    let lastname = '';
    let email = '';
    if (Network.userProfile.first_name !== null) {
        firstname = Network.userProfile.first_name;
    }
    if (Network.userProfile.last_name !== null) {
        lastname = Network.userProfile.last_name;
    }
    if (Network.userProfile.email !== null) {
        email = Network.userProfile.email;
    }

    this.setState({
        firstname,
        lastname,
        email,
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  renderField = (title, value, style) => {
    return (<View style={[{
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        minHeight: Common.getLengthByIPhone7(61),
        borderBottomWidth: 1,
        borderBottomColor: '#f5f5f5',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    }, style]}>
      <Text style={{
          marginTop: Common.getLengthByIPhone7(10),
          color: '#999999',
          fontFamily: 'Montserrat-Regular',
          fontWeight: 'normal',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(12),
          lineHeight: Common.getLengthByIPhone7(15),
      }}
      allowFontScaling={false}>
          {title}
      </Text>
      <Text style={{
          marginBottom: Common.getLengthByIPhone7(9),
          color: '#3e3e3e',
          fontFamily: 'Montserrat-Regular',
          fontWeight: 'normal',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(16),
          lineHeight: Common.getLengthByIPhone7(20),
      }}
      allowFontScaling={false}>
          {value}
      </Text>
    </View>);
}

renderButton = (title, icon, style, arrow, action) => {
    return (<TouchableOpacity style={[{
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        minHeight: Common.getLengthByIPhone7(61),
        borderBottomWidth: 1,
        borderBottomColor: '#f5f5f5',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    }, style]}
    onPress={() => {
        action();
    }}>
        <View style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
        }}>
            <Image
                source={icon}
                style={{
                    width: Common.getLengthByIPhone7(24),
                    height: Common.getLengthByIPhone7(24),
                    resizeMode: 'contain',
                    tintColor: '#959595',
                }}
            />
            <Text style={{
                marginLeft: Common.getLengthByIPhone7(8),
                color: '#3e3e3e',
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(16),
                lineHeight: Common.getLengthByIPhone7(20),
            }}
            allowFontScaling={false}>
                {title}
            </Text>
        </View>
        {arrow ? (<Image
            source={require('./../assets/ic-arrow-right.png')}
            style={{
                width: Common.getLengthByIPhone7(16),
                height: Common.getLengthByIPhone7(16),
                resizeMode: 'contain',
                tintColor: '#959595',
            }}
        />) : null}
    </TouchableOpacity>);
}

  render() {

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
            {this.renderField('Фамилия', this.state.lastname, {marginTop: Common.getLengthByIPhone7(80),})}
            {this.renderField('Имя', this.state.firstname, {})}
            {this.renderField('E-mail', this.state.email, {})}
            {this.renderButton('Изменить профиль', require('./../assets/ic-setting.png'), {marginTop: Common.getLengthByIPhone7(30)}, true, () => {
                this.props.navigation.navigate('Profile2', {from: 'settings'});
            })}
            {this.renderButton('Уведомления', require('./../assets/ic-notification.png'), {}, true, () => {
                this.props.navigation.navigate('Notification');
            })}
            {this.renderButton('Выход', require('./../assets/ic-logout.png'), {}, false, () => {
                Alert.alert(
                Config.appName,
                "Вы хотите выйти?",
                [
                    {
                        text: "Нет",
                        onPress: () => {

                        },
                        style: "cancel"
                    },
                    { text: "Да", onPress: () => {
                        AsyncStorage.setItem('token', '');
                        Network.access_token = '';
                        Network.userProfile = null;
                        this.props.navigation.navigate('LoginTab')
                    }}
                ],
                { cancelable: false }
            );
            })}
      </View>);
  }
}
