import React from 'react';
import {Platform, BackHandler, Button, StatusBar, InputAccessoryView, Text, View, TouchableOpacity, Linking, TextInput, Image, Alert, ImageBackground, Dimensions} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { observer } from 'mobx-react';
import { isIphoneX, getStatusBarHeight } from 'react-native-iphone-x-helper';
import LinearGradient from 'react-native-linear-gradient';
import { RNCamera } from 'react-native-camera';
import MaskView from '../components/Scanner/MaskView';
import {check, request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import BackButton from './../components/Buttons/BackButton';

@observer
export default class ScannerScreen extends React.Component {

  state = {
    type: 'back',
    flashMode: RNCamera.Constants.FlashMode.off,
    loading: false,
    reload: false,
  };

  UNSAFE_componentWillMount() {
    console.warn('getStatusBarHeight: '+getStatusBarHeight());
    check(Platform.OS == 'ios' ? PERMISSIONS.IOS.CAMERA : PERMISSIONS.ANDROID.CAMERA)
    .then((result) => {
      switch (result) {
        case RESULTS.UNAVAILABLE:
          console.warn('This feature is not available (on this device / in this context)');
          break;
        case RESULTS.DENIED:
          console.warn('The permission has not been requested / is denied but requestable');
          // request(Platform.OS == 'ios' ? PERMISSIONS.IOS.CAMERA : PERMISSIONS.ANDROID.CAMERA)
          // then(result => {
          //   console.warn('result: '+result);
          //   this.setState({
          //     reload: !this.state.reload,
          //   });
          // });
          break;
        case RESULTS.LIMITED:
          console.warn('The permission is limited: some actions are possible');
          break;
        case RESULTS.GRANTED:
          console.warn('The permission is granted');
          break;
        case RESULTS.BLOCKED:
          console.warn('The permission is denied and not requestable anymore');
          Alert.alert(
            Config.appName,
            "Для работы приложения необходим доступ к камере телефона!",
            [
              {
                text: "Отмена",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "Настройки", onPress: () => {
                Linking.openSettings();
              }}
            ],
            { cancelable: false }
          );
          break;
      }
    })
    .catch((error) => {
      // …
    });

    this.props.navigation.addListener('willFocus', this.willFocus)
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  willFocus = () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    this.props.navigation.goBack(null);
    return true;
  }

  handleMountError = ({ message }) => {
    console.error(message);
  }

  collectPictureSizes = async () => {
    if (this.camera) {
      const pictureSizes = await this.camera.getAvailablePictureSizesAsync(this.state.ratio);
      let pictureSizeId = 0;
      if (Platform.OS === 'ios') {
        pictureSizeId = pictureSizes.indexOf('High');
      } else {
        // returned array is sorted in ascending order - default size is the largest one
        pictureSizeId = pictureSizes.length-1;
      }
      this.setState({ pictureSizes, pictureSizeId, pictureSize: pictureSizes[pictureSizeId] });
    }
  };

  render() {

    // console.log(JSON.stringify({event_id:8,user_id:567}));
    return (<View style={{
      flex: 1,
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: Colors.blue,
    }}>
        <View style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
            backgroundColor: 'white',
            // marginTop: Common.getLengthByIPhone7(100),
            borderTopLeftRadius: Common.getLengthByIPhone7(30),
            borderTopRightRadius: Common.getLengthByIPhone7(30),
            // overflow: 'hidden',
            alignItems: 'center',
        }}>
            <RNCamera
              ref={ref => {
                this.camera = ref;
              }}
              style={{
                width: Common.getLengthByIPhone7(0),
                position: 'absolute',
                left: 0,
                top: 0,
                bottom: 0,
              }}
              barCodeTypes={[
                RNCamera.Constants.BarCodeType.qr,
              ]}
              onBarCodeRead={code => {
                // console.warn('code2: '+JSON.stringify(code));
                // return;
                let width = 0;
                let height = 0;

                if(Platform.OS == 'ios') {
                  width = code.bounds.size.width;
                  height = code.bounds.size.height;
                  if(width < Common.getLengthByIPhone7(190) && height < Common.getLengthByIPhone7(190)) {
                    if(
                        (code.bounds.origin.x > Math.round((Dimensions.get('window').width - Common.getLengthByIPhone7(200))/2)) &&
                    (code.bounds.origin.y > Math.round((Dimensions.get('window').height - Common.getLengthByIPhone7(100) - (Common.getLengthByIPhone7(194) - 4))/2))) {
                      if (Network.barcode != code.data) {
                        Network.barcode = code.data;
                        // console.warn('code: '+JSON.stringify(code));
                        let case_id = code.data.split('.ru/');
                        if (case_id !== null && case_id.length === 2) {
                          case_id = case_id[1].split('/');
                          case_id = case_id[0];
                          if (this.props.navigation.state.params.action) {
                            this.props.navigation.state.params.action(case_id);
                          }
                          this.props.navigation.goBack(null);
                        }
                      }
                    }
                  }
                } else {
                  if (Network.barcode != code.data) {
                    Network.barcode = code.data;
                    console.warn('code: '+JSON.stringify(code));
                    let case_id = code.data.split('.ru/');
                    if (case_id !== null && case_id.length === 2) {
                      case_id = case_id[1].split('/');
                      case_id = case_id[0];
                      if (this.props.navigation.state.params.action) {
                        this.props.navigation.state.params.action(case_id);
                      }
                      this.props.navigation.goBack(null);
                    }
                  }
                }

              }}
              onCameraReady={this.collectPictureSizes}
              type={this.state.type}

              flashMode={this.state.flashMode}
              autoFocus='on'
              zoom={0}
              whiteBalance='auto'
              ratio='4:3'
              pictureSize={undefined}
              onMountError={this.handleMountError}
            />
            <MaskView />
            <View style={{
              position: 'absolute',
              left: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(186))/2 - 8,
              top: (Dimensions.get('window').height - Common.getLengthByIPhone7(186))/2 - 8,
              width: Common.getLengthByIPhone7(50),
              height: Common.getLengthByIPhone7(50),
              borderTopWidth: 4,
              borderTopColor: 'white',
              borderLeftWidth: 4,
              borderLeftColor: 'white',
              borderTopLeftRadius: 8,
            }}/>
            <View style={{
              position: 'absolute',
              right: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(186))/2 - 8,
              top: (Dimensions.get('window').height - Common.getLengthByIPhone7(186))/2 - 8,
              width: Common.getLengthByIPhone7(50),
              height: Common.getLengthByIPhone7(50),
              borderTopWidth: 4,
              borderTopColor: 'white',
              borderRightWidth: 4,
              borderRightColor: 'white',
              borderTopRightRadius: 8,
            }}/>
            <View style={{
              position: 'absolute',
              left: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(186))/2 - 8,
              bottom: (Dimensions.get('window').height - getStatusBarHeight() + (Platform.OS === 'ios' ? 22 : -22) - Common.getLengthByIPhone7(186))/2 - 8,// + Common.getLengthByIPhone7(186),
              width: Common.getLengthByIPhone7(50),
              height: Common.getLengthByIPhone7(50),
              borderBottomWidth: 4,
              borderBottomColor: 'white',
              borderLeftWidth: 4,
              borderLeftColor: 'white',
              borderBottomLeftRadius: 8,
            }}/>
            <View style={{
              position: 'absolute',
              right: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(186))/2 - 8,
              bottom: (Dimensions.get('window').height - getStatusBarHeight() + (Platform.OS === 'ios' ? 22 : -22) - Common.getLengthByIPhone7(186))/2 - 8,// + Common.getLengthByIPhone7(186),
              width: Common.getLengthByIPhone7(50),
              height: Common.getLengthByIPhone7(50),
              borderBottomWidth: 4,
              borderBottomColor: 'white',
              borderRightWidth: 4,
              borderRightColor: 'white',
              borderBottomRightRadius: 8,
            }}/>
        </View>
        <View style={{
          width: Common.getLengthByIPhone7(0),
          height: Common.getLengthByIPhone7(100),
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingTop: isIphoneX() ? 30 : 0,
          position: 'absolute',
          left : 0,
          top: 0,
          zIndex: 100,
        }}>
          <BackButton navigation={this.props.navigation}/>
          <Text style={{
            fontFamily: 'Montserrat-Bold',
            textAlign: 'center',
            fontSize: Common.getLengthByIPhone7(18),
            lineHeight: Common.getLengthByIPhone7(24),
            fontWeight: 'bold',
            color: 'white',
          }}>
            Сканер QR-кода
          </Text>
          <View style={{
            width: Common.getLengthByIPhone7(45),
          }}/>
        </View>
    </View>);
  }
}
