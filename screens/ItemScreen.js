import React from 'react';
import {
  Dimensions,
  Text,
  View,
  BackHandler,
  Animated,
  Easing,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  Platform,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, {getCase, renameCase, newSubscribtion} from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import EventsView from './../components/Item/EventsView';
import PdfModalView from './../components/Item/PdfModalView';
import Dialog from "react-native-dialog";
import TopButtonView from './../components/Main/TopButtonView';
import StoryModalView from './../components/StoryModalView';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { observer } from 'mobx-react';
import { InAppBrowser } from 'react-native-inappbrowser-reborn';

@observer
export default class ItemScreen extends React.Component {

  calculate = true;
  top = new Animated.Value(0);
  spinValue = new Animated.Value(0);

  state = {
    loading: false,
    data: null,
    showPdf: false,
    visible: false,
    name: '',
    reload: false,
  };

  UNSAFE_componentWillMount() {
    
    Network.subscribeCase = this.subscribeCase;
    Network.toCaseSettings = this.toCaseSettings;

    BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
            this.props.navigation.goBack(null);
            return true;
        }
    );

    this.refreshCase();
    Network.refreshCase = this.refreshCase;

    if (Network.refreshList) {
        Network.refreshList();
    }
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  toCaseSettings = () => {
      console.warn('toCaseSettings');
      this.props.navigation.navigate('CaseSettings', {data: this.state.data});
  }

  refreshCase = () => {
    this.setState({
        loading: true,
    }, () => {
        getCase(this.props.navigation.state.params.data.id)
        .then(data => {
            if (Network.setShowMenu) {
                Network.setShowMenu(!data.subscribed);
            }
            this.setState({
                loading: false,
                data,
                name: data.name,
            });
        })
        .catch(err => {
            this.setState({
                loading: false,
            }, () => {
                setTimeout(() => {
                    Alert.alert(Config.appName, err);
                }, 100);
            });
        });
    });
  }

  subscribeCase = () => {
      this.setState({
          loading: true,
      }, () => {
          newSubscribtion('case', this.state.data['case-number'])
          .then(() => {
            this.setState({
                loading: false,
            }, () => {
                setTimeout(() => {
                  Alert.alert(Config.appName, 'Новое дело добавлено!');
                }, 100);
                Network.setShowMenu(false);
            });
          })
          .catch(err => {
              this.setState({
                  loading: false,
              }, () => {
                setTimeout(() => {
                    Alert.alert(Config.appName, err);
                  }, 100);
              });
          });
      });
  }

  renderField = (title, value, style) => {
      return (<View style={[{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          minHeight: Common.getLengthByIPhone7(61),
          borderBottomWidth: 1,
          borderBottomColor: '#f5f5f5',
          justifyContent: 'space-between',
          alignItems: 'flex-start',
      }, style]}>
        <Text style={{
            marginTop: Common.getLengthByIPhone7(10),
            color: '#999999',
            fontFamily: 'Montserrat-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(12),
            lineHeight: Common.getLengthByIPhone7(15),
        }}
        allowFontScaling={false}>
            {title}
        </Text>
        <Text style={{
            marginBottom: Common.getLengthByIPhone7(9),
            color: '#3e3e3e',
            fontFamily: 'Montserrat-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(16),
            lineHeight: Common.getLengthByIPhone7(20),
        }}
        allowFontScaling={false}>
            {value}
        </Text>
      </View>);
  }

  openBrowser = () => {
    InAppBrowser.isAvailable()
    .then(() => {
      InAppBrowser.open(this.state.url, {
        // iOS Properties
        dismissButtonStyle: 'cancel',
        preferredBarTintColor: '#453AA4',
        preferredControlTintColor: 'white',
        readerMode: false,
        animated: true,
        modalPresentationStyle: 'fullScreen',
        modalTransitionStyle: 'coverVertical',
        modalEnabled: true,
        enableBarCollapsing: false,
        // Android Properties
        showTitle: true,
        toolbarColor: Colors.mainColor,
        secondaryToolbarColor: 'black',
        navigationBarColor: 'black',
        navigationBarDividerColor: 'white',
        enableUrlBarHiding: true,
        enableDefaultShare: true,
        forceCloseOnRedirection: false,
        // Specify full animation resource identifier(package:anim/name)
        // or only resource name(in case of animation bundled with app).
        animations: {
          startEnter: 'slide_in_right',
          startExit: 'slide_out_left',
          endEnter: 'slide_in_left',
          endExit: 'slide_out_right'
        },
        headers: {
          'my-custom-header': 'my custom header value'
        }
      });
    })
    .catch(err => {
      console.warn('InAppBrowser err: '+err);
    });
  }

  render() {

    if (this.state.loading) {
        return (<View style={{
            flex: 1,
            width: Common.getLengthByIPhone7(0),
            // height: Dimensions.get('window').height,
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor: 'white',
          }}>
                <Spinner
                    visible={this.state.loading}
                    // textContent={'Загрузка...'}
                    onShow={() => {
                        Animated.loop(
                        Animated.timing(
                            this.spinValue,
                            {
                            toValue: 1,
                            duration: 1000,
                            easing: Easing.linear,
                            useNativeDriver: true
                            }
                        )
                        ).start();
                    }}
                    onDismiss={() => {

                    }}
                    customIndicator={<View style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        zIndex: 20000,
                    }}>
                        <View style={{
                        width: Common.getLengthByIPhone7(65),
                        height: Common.getLengthByIPhone7(65),
                        alignItems: 'center',
                        justifyContent: 'center',
                        }}>
                        <Animated.Image
                            style={{
                            width: Common.getLengthByIPhone7(65),
                            height: Common.getLengthByIPhone7(65),
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{rotate: this.spinValue.interpolate({
                                inputRange: [0, 1],
                                outputRange: ['0deg', '360deg']
                            })}] 
                            }}
                            source={require('./../assets/ic-loader.png')}
                        />
                        <TouchableOpacity style={{
                            // marginTop: Common.getLengthByIPhone7(20),
                            padding: Common.getLengthByIPhone7(5),
                            backgroundColor: 'rgba(255, 255, 255, 0.8)',
                            borderRadius: Common.getLengthByIPhone7(5),
                        }}
                        onPress={() => {
                            this.setState({
                            loading: false,
                            });
                        }}>
                            <Image
                            style={{
                                width: Common.getLengthByIPhone7(18),
                                height: Common.getLengthByIPhone7(18),
                            }}
                            source={require('./../assets/ic-loader-close.png')}
                            />
                        </TouchableOpacity>
                        </View>
                    </View>}
                    overlayColor={'rgba(32, 42, 91, 0.3)'}
                    textStyle={{color: '#FFF'}}
                    />
          </View>);
    }

    if (this.state.data === null) {
        return null;
    }

    // Alert.alert('dsdd: ', this.state.data.subscribed);

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
            <TopButtonView
                navigation={this.props.navigation}
                display={'case'}
                refresh={() => {
                    this.setState({
                        reload: !this.state.reload,
                    });
                }}
                onClick={data => {
                    Network.storyId = data;
                    Network.storyIndex = data;
                    Network.storyItemIndex = 0;
                    Network.storyReady = true;
                    this.setState({
                        storiesShow: true,
                        storyIndex: data,
                    });
                }}
            />
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                // marginTop: 100,//Platform.OS === 'ios' ? Common.getLengthByIPhone7(25) : 0,//Common.getLengthByIPhone7(60),
                marginTop: (Network.caseClipsList.length ? (Platform.OS === 'ios' ? Common.getLengthByIPhone7(25) : 0) : (isIphoneX() ? Common.getLengthByIPhone7(105) : Common.getLengthByIPhone7(65))),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            }}>
                <Text style={{
                    maxWidth: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                    color: Colors.textColor,
                    fontFamily: 'Montserrat-Bold',
                    fontWeight: 'bold',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(24),
                }}
                allowFontScaling={false}>
                    {this.state.data.name != null && this.state.data.name.length ? this.state.data.name : this.state.data['case-number']}
                </Text> 
                <TouchableOpacity style={{
                    // marginLeft: Common.getLengthByIPhone7(10),
                    width: Common.getLengthByIPhone7(35),
                    height: Common.getLengthByIPhone7(35),
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
                onPress={() => {
                    this.setState({
                        visible: true,
                    });
                }}>
                    <Image
                        source={require('./../assets/ic-pen.png')}
                        style={{
                            width: Common.getLengthByIPhone7(15),
                            height: Common.getLengthByIPhone7(15),
                            resizeMode: 'contain',
                            // tintColor: '#424347',
                        }}
                    />
                </TouchableOpacity>
            </View>
            <View style={{
                marginTop: Common.getLengthByIPhone7(5),
                marginBottom: Common.getLengthByIPhone7(10),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
            }}>
                <Text style={{
                    // marginLeft: Common.getLengthByIPhone7(9),
                    color: Colors.textColor,
                    fontFamily: 'Montserrat-Regular',
                    fontWeight: 'normal',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(10),
                }}
                allowFontScaling={false}>
                    {this.state.data['case-number']}
                </Text>
            </View>
            <ScrollView style={{
                width: Common.getLengthByIPhone7(0),
                flex: 1,
                paddingBottom: Common.getLengthByIPhone7(20),
                marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? 86 : 52) : 52,
            }}
            contentContainerStyle={{
                alignItems: 'center',
            }}>
                {this.renderField('Истец', this.state.data.plaintiffs, {marginTop: Common.getLengthByIPhone7(18)})}
                {this.renderField('Ответчик', this.state.data.defendants, {})}
                {this.renderField('Третьи лица', this.state.data.third, {})}
                <EventsView
                    openUrl={url => {
                        this.setState({
                            // url,
                            // url: 'https://docs.google.com/viewer?url=http://kad.crona.tech/api/v3/pdf?caseId=c60babc9-20b1-446f-bd19-3a6d643ab3de&eventId=b0bfb54f-84c2-417b-9f83-7bf5527c38c2&name=A11-11111-2011_20120116_Reshenija_i_postanovlenija.pdf,
                            url,
                            // showPdf: true,
                        }, () => {
                            this.openBrowser();
                        });
                    }}
                    data={this.state.data.instances}
                />
            </ScrollView>
            <PdfModalView
                url={this.state.url}
                show={this.state.showPdf}
                onClose={() => {
                    this.setState({
                        showPdf: false,
                    });
                }}
            />
            <Dialog.Container visible={this.state.visible}>
                <Dialog.Title>Переименовать дело</Dialog.Title>
                <Dialog.Input value={this.state.name} onChangeText={text => this.setState({name : text})} />
                <Dialog.Button label="Отмена" onPress={() => {
                    this.setState({
                        visible: false,
                    });
                }} />
                <Dialog.Button label="Сохранить" onPress={() => {
                    this.setState({
                        loading: true,
                    }, () => {
                        if (this.state.name.length) {
                            renameCase(this.props.navigation.state.params.data.id, this.state.name)
                            .then(data => {
                                this.setState({
                                    loading: false,
                                    data,
                                    visible: false,
                                    name: data.name,
                                }, () => {
                                    if (Network.refreshList) {
                                        Network.refreshList();
                                    }
                                });
                            })
                            .catch(err => {
                                this.setState({
                                    loading: false,
                                }, () => {
                                    setTimeout(() => {
                                        Alert.alert(Config.appName, err);
                                    }, 100);
                                });
                            }); 
                        } else {
                            Alert.alert(Config.appName, 'Укажите название дела!');
                        }
                    });
                }} />
            </Dialog.Container>
            {this.state.storiesShow ? (<StoryModalView
                show={this.state.storiesShow}
                navigation={this.props.navigation}
                stories={Network.caseClipsList}
                index={this.state.storyIndex}
                display={'case'}
                onClose={() => {
                    this.setState({
                    storiesShow: false,
                    });
                    StatusBar.setHidden(false);
                }}
            />) : null}
      </View>);
  }
}
