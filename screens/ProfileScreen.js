import React from 'react';
import {
  Dimensions,
  Text,
  View,
  Alert,
  BackHandler,
  TouchableOpacity,
  Animated,
  Easing,
  Image,
  Keyboard,
  ActivityIndicator,
  Platform,
  ScrollView,
  StatusBar
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, {editProfile} from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import AnimatedButton from '../components/Buttons/AnimatedButton';
import AnimatedTextInputView from './../components/AnimatedTextInputView';
import ActionSheet from 'react-native-actionsheet';
import { isIphoneX } from 'react-native-iphone-x-helper';

const statuses = {
  '0': 'Не юрист',
  '1': 'Инхаус',
  '2': 'Консалтинг',
  '3': 'Арбитражный управляющий',
};

export default class ProfileScreen extends React.Component {

  calculate = true;
  top = new Animated.Value(0);
  spinValue = new Animated.Value(0);

  state = {
    code: '',
    loading: false,
    buttonY: 0,
    buttonH: 0,
    viewY: 0,
    viewH: 0,
    firstname: '',
    lastname: '',
    email: '',
    status: 'Не указано',
    type: null,
  };

  UNSAFE_componentWillMount() {

    BackHandler.addEventListener(
      "hardwareBackPress",
      () => {
          this.props.navigation.goBack(null);
          return true;
      }
    );
    this.props.navigation.addListener('willFocus', this.willFocus);
    this.props.navigation.addListener('willBlur', this.willBlur);

    let firstname = '';
    let lastname = '';
    let email = '';
    let status = 'Не указано';
    let type = null;

    if (Network.userProfile.first_name !== null) {
        firstname = Network.userProfile.first_name;
    }
    if (Network.userProfile.last_name !== null) {
        lastname = Network.userProfile.last_name;
    }
    if (Network.userProfile.email !== null) {
        email = Network.userProfile.email;
    }

    if (Network.userProfile.type !== null) {
      status = statuses[Network.userProfile.type];
      type = Network.userProfile.type;
    }

    this.setState({
        firstname,
        lastname,
        email,
        status,
        type,
    });
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  willFocus = () => {
    Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('dark-content', true);
    }
  }

  willBlur = () => {
    Keyboard.removeListener('keyboardDidShow');
    Keyboard.removeListener('keyboardDidHide');
  }

  _keyboardDidHide = () => {
    this.calculate = false;
    Animated.timing(this.top, {
      toValue: (Dimensions.get('window').height - this.state.viewH)/2,
      duration: 300,
    }).start();
  }

  _keyboardDidShow = e => {
    this.calculate = false;
    let kbView = (Dimensions.get('window').height - e.endCoordinates.height);
    let view = this.state.buttonY+this.state.viewY;
    if (view + Common.getLengthByIPhone7(50) > kbView) {
      Animated.timing(this.top, {
        toValue: (Dimensions.get('window').height - this.state.viewH)/2 - (view + Common.getLengthByIPhone7(50) - kbView),
        duration: 300,
      }).start();
    }
  }

  nextClick = () => {
    if (this.state.lastname.length && this.state.firstname.length && this.state.email.length) {
      this.setState({
        loading: true,
      }, () => {
        editProfile(this.state.firstname, this.state.lastname, this.state.email, this.state.type)
        .then(() => {
          this.setState({
            loading: false,
          }, () => {
            // Alert.alert(Config.appName, 'Изменения сохранены!');
            if (this.props.navigation.state.params && this.props.navigation.state.params.from === 'login') {
              this.props.navigation.navigate('Main');
            } else {
              Alert.alert(Config.appName, 'Изменения сохранены!');
            }
          });
        })
        .catch(err => {
          this.setState({
            loading: false,
          }, () => {
            setTimeout(() => {
              Alert.alert(Config.appName, err);
            }, 100);
          });
        });
      });
    } else {
      Alert.alert(Config.appName, 'Заполните все поля!');
    }
  }

  onLayout = e => {
    if (this.calculate) {
      this.setState({
        buttonH: e.nativeEvent.layout.height,
        buttonY: e.nativeEvent.layout.y,
      })
    }
  }

  onLayout2 = e => {
    if (this.calculate) {
      this.setState({
        viewY: e.nativeEvent.layout.y,
        viewH: e.nativeEvent.layout.height,
      }, () => {
        Animated.timing(this.top, {
          toValue: (Dimensions.get('window').height - this.state.viewH)/2,
          duration: 0,
        }).start();
      })
    }
  }

  render() {

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
            <Text style={{
                // marginTop: Common.getLengthByIPhone7(105),
                marginTop: (isIphoneX() ? Common.getLengthByIPhone7(105) : Common.getLengthByIPhone7(65)),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: Colors.textColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(24),
            }}
            allowFontScaling={false}>
                ПРОФИЛЬ
            </Text>
            <ScrollView style={{
              flex: 1,
              width: Common.getLengthByIPhone7(0),
              // height: Dimensions.get('window').height,
              marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? 86 : 52) : 52,
            }}
            contentContainerStyle={{
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}>
              <AnimatedTextInputView
                  style={{
                      marginTop: Common.getLengthByIPhone7(20),
                  }}
                  autoCorrect={false}
                  value={this.state.lastname}
                  title={'Фамилия'}
                  autoCapitalize={'words'}
                  onChangeText={lastname => {
                  this.setState({
                      lastname,
                  });
                  }}
              />
              <AnimatedTextInputView
                  style={{
                      // marginTop: Common.getLengthByIPhone7(28),
                  }}
                  autoCorrect={false}
                  value={this.state.firstname}
                  title={'Имя'}
                  autoCapitalize={'words'}
                  onChangeText={firstname => {
                  this.setState({
                      firstname,
                  });
                  }}
              />
              <AnimatedTextInputView
                  style={{
                      // marginTop: Common.getLengthByIPhone7(28),
                  }}
                  value={this.state.email}
                  title={'E-mail'}
                  keyboardType={'email-address'}
                  autoCapitalize={'words'}
                  autoCorrect={false}
                  onChangeText={email => {
                  this.setState({
                      email,
                  });
                  }}
              />
              <View style={{

              }}>
                <AnimatedTextInputView
                  style={{
                      // marginTop: Common.getLengthByIPhone7(28),
                  }}
                  value={this.state.status}
                  title={'Ваш статус'}
                  onChangeText={email => {
                    
                  }}
                />
                <TouchableOpacity style={{
                  position: 'absolute',
                  left: 0,
                  top: 0,
                  right: 0,
                  bottom: 0,
                }}
                onPress={() => {
                  this.ActionSheet.show();
                }}
                />
              </View>
              <AnimatedButton style={{
                  width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                  height: Common.getLengthByIPhone7(60),
                  marginTop: Common.getLengthByIPhone7(50),
                  borderRadius: Common.getLengthByIPhone7(4),
              }}
              onLayout={this.onLayout}
              onPress={() => {
                  this.nextClick();
              }}
              title={'Сохранить'}
              activeColor={Colors.mainColor}
              inactiveColor={'white'}/>
            </ScrollView>
            <Spinner
              visible={this.state.loading}
              // textContent={'Загрузка...'}
              onShow={() => {
                Animated.loop(
                  Animated.timing(
                    this.spinValue,
                    {
                    toValue: 1,
                    duration: 1000,
                    easing: Easing.linear,
                    useNativeDriver: true
                    }
                  )
                ).start();
              }}
              onDismiss={() => {

              }}
              customIndicator={<View style={{
                alignItems: 'center',
                justifyContent: 'center',
                zIndex: 20000,
              }}>
                <View style={{
                  width: Common.getLengthByIPhone7(65),
                  height: Common.getLengthByIPhone7(65),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                  <Animated.Image
                    style={{
                      width: Common.getLengthByIPhone7(65),
                      height: Common.getLengthByIPhone7(65),
                      position: 'absolute',
                      bottom: 0,
                      left: 0,
                      transform: [{rotate: this.spinValue.interpolate({
                        inputRange: [0, 1],
                        outputRange: ['0deg', '360deg']
                      })}] 
                    }}
                    source={require('./../assets/ic-loader.png')}
                  />
                  <TouchableOpacity style={{
                    // marginTop: Common.getLengthByIPhone7(20),
                    padding: Common.getLengthByIPhone7(5),
                    backgroundColor: 'rgba(255, 255, 255, 0.8)',
                    borderRadius: Common.getLengthByIPhone7(5),
                  }}
                  onPress={() => {
                    this.setState({
                      loading: false,
                    });
                  }}>
                    <Image
                      style={{
                        width: Common.getLengthByIPhone7(18),
                        height: Common.getLengthByIPhone7(18),
                      }}
                      source={require('./../assets/ic-loader-close.png')}
                    />
                  </TouchableOpacity>
                </View>
              </View>}
              overlayColor={'rgba(32, 42, 91, 0.3)'}
              textStyle={{color: '#FFF'}}
            />
            <ActionSheet
              ref={o => this.ActionSheet = o}
              title={Config.appName}
              options={['Не юрист', 'Инхаус', 'Консалтинг', 'Арбитражный управляющий', 'Отмена']}
              cancelButtonIndex={4}
              onPress={index => {
                if (index !== 4) {
                  this.setState({
                    type: index,
                    status: statuses[index],
                  });
                }
              }}
            />
      </View>);
  }
}
