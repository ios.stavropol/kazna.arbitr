import React from 'react';
import {
  Image,
  Text,
  View,
  BackHandler,
  Animated,
  Easing,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  Linking,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, {getCompany} from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import CaseList from './../components/Company/CaseList';
import { isIphoneX } from 'react-native-iphone-x-helper';
import Toast from 'react-native-simple-toast';
import Clipboard from '@react-native-clipboard/clipboard'; 

export default class CompanyInfoScreen extends React.Component {

  calculate = true;
  top = new Animated.Value(0);
  spinValue = new Animated.Value(0);

  state = {
    loading: false,
    data: null,
    showPdf: false,
  };

  UNSAFE_componentWillMount() {

    BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
            this.props.navigation.goBack(null);
            return true;
        }
    );

    this.setState({
        loading: true,
    }, () => {
        getCompany(this.props.navigation.state.params.data.id)
        .then(data => {
            this.setState({
                loading: false,
                data,
            });
        })
        .catch(err => {
            this.setState({
                loading: false,
            });
        });
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);

  }

  renderField = (title, value) => {
      return (<View style={{
        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: Common.getLengthByIPhone7(15),
    }}>
        <Text style={{
            color: Colors.textColor,
            fontFamily: 'Montserrat-Bold',
            fontWeight: 'bold',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(15),
        }}
        allowFontScaling={false}>
            {title}
        </Text>
        <View style={{
            height: Common.getLengthByIPhone7(15),
            flex: 1,
        }} >
            <View style={{
                position: 'absolute',
                left: 0,
                top: 0,
                right: 0,
                bottom: 0,
                borderStyle: 'dotted',
                borderWidth: 2,
                borderColor: Colors.textColor,
            }} />
            <View style={{
                position: 'absolute',
                left: 0,
                top: 0,
                right: 0,
                bottom: 2,
                backgroundColor: 'white',
            }} />
        </View>
        <Text style={{
            color: Colors.textColor,
            fontFamily: 'Montserrat-Bold',
            fontWeight: 'bold',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(15),
        }}
        allowFontScaling={false}>
            {value}
        </Text>
    </View>);
  }

  renderField2 = (title, value, value2) => {
        let text2 = null;

        if (value2 !== null) {
            text2 = (<Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(15),
                marginTop: Common.getLengthByIPhone7(10),
            }}
            allowFontScaling={false}>
                {value2}
            </Text>);
        }
        return (<View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            borderBottomWidth: 1,
            borderBottomColor: 'rgba(0, 0, 0, 0.2)',
            marginTop: Common.getLengthByIPhone7(15),
            paddingBottom: Common.getLengthByIPhone7(15),
        }}>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(15),
            }}
            allowFontScaling={false}>
                {title}
            </Text>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(15),
                marginTop: Common.getLengthByIPhone7(10),
            }}
            allowFontScaling={false}>
                {value}
            </Text>
            {text2}
        </View>);
    }

    renderOkved = okveds => {
        let text2 = null;

        if (okveds.length > 1) {
            text2 = (<TouchableOpacity style={{
                marginTop: Common.getLengthByIPhone7(10),
            }}
            onPress={() => {
                this.props.navigation.navigate('Okveds', {data: okveds});
            }}>
                <Text style={{
                    color: Colors.violetColor,
                    fontFamily: 'Montserrat-Regular',
                    fontWeight: 'normal',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(15),
                }}
                allowFontScaling={false}>
                    Показать еще ({okveds.length - 1})
                </Text>
            </TouchableOpacity>);
        }

        let value = '';

        for (let i = 0; i < okveds.length; i++) {
            if (okveds[i].main) {
                value = okveds[i].name + ' (' + okveds[i].code + ')';
                break;
            }
        }
        return (<View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            borderBottomWidth: 1,
            borderBottomColor: 'rgba(0, 0, 0, 0.2)',
            marginTop: Common.getLengthByIPhone7(15),
            paddingBottom: Common.getLengthByIPhone7(15),
        }}>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(15),
            }}
            allowFontScaling={false}>
                Основной вид деятельности
            </Text>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(15),
                marginTop: Common.getLengthByIPhone7(10),
            }}
            allowFontScaling={false}>
                {value}
            </Text>
            {text2}
        </View>);
    }

    renderFounder = (title, value, value2) => {

        return (<View style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            marginTop: Common.getLengthByIPhone7(15),
        }}>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(15),
            }}
            allowFontScaling={false}>
                {title}
            </Text>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(15),
                marginTop: Common.getLengthByIPhone7(10),
            }}
            allowFontScaling={false}>
                {value}
            </Text>
            <Text style={{
                color: Colors.textColor,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(15),
                marginTop: Common.getLengthByIPhone7(10),
            }}
            allowFontScaling={false}>
                {value2}
            </Text>
        </View>);
    }

  render() {

    if (this.state.loading) {
        return (<View style={{
            flex: 1,
            width: Common.getLengthByIPhone7(0),
            // height: Dimensions.get('window').height,
            justifyContent: 'flex-start',
            alignItems: 'center',
            backgroundColor: 'white',
          }}>
                <Spinner
                    visible={this.state.loading}
                    // textContent={'Загрузка...'}
                    onShow={() => {
                        Animated.loop(
                        Animated.timing(
                            this.spinValue,
                            {
                            toValue: 1,
                            duration: 1000,
                            easing: Easing.linear,
                            useNativeDriver: true
                            }
                        )
                        ).start();
                    }}
                    onDismiss={() => {

                    }}
                    customIndicator={<View style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        zIndex: 20000,
                    }}>
                        <View style={{
                        width: Common.getLengthByIPhone7(65),
                        height: Common.getLengthByIPhone7(65),
                        alignItems: 'center',
                        justifyContent: 'center',
                        }}>
                        <Animated.Image
                            style={{
                            width: Common.getLengthByIPhone7(65),
                            height: Common.getLengthByIPhone7(65),
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{rotate: this.spinValue.interpolate({
                                inputRange: [0, 1],
                                outputRange: ['0deg', '360deg']
                            })}] 
                            }}
                            source={require('./../assets/ic-loader.png')}
                        />
                        <TouchableOpacity style={{
                            // marginTop: Common.getLengthByIPhone7(20),
                            padding: Common.getLengthByIPhone7(5),
                            backgroundColor: 'rgba(255, 255, 255, 0.8)',
                            borderRadius: Common.getLengthByIPhone7(5),
                        }}
                        onPress={() => {
                            this.setState({
                            loading: false,
                            });
                        }}>
                            <Image
                            style={{
                                width: Common.getLengthByIPhone7(18),
                                height: Common.getLengthByIPhone7(18),
                            }}
                            source={require('./../assets/ic-loader-close.png')}
                            />
                        </TouchableOpacity>
                        </View>
                    </View>}
                    overlayColor={'rgba(32, 42, 91, 0.3)'}
                    textStyle={{color: '#FFF'}}
                    />
          </View>);
    }

    // return null;

    if (this.state.data === null) {
        return null;
    }

    // Alert.alert('dsdd', this.state.data.subscribed);

    let status = (<View style={{
        width: Common.getLengthByIPhone7(170),
        marginBottom: Common.getLengthByIPhone7(5),
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'red',
        borderWidth: 1,
        borderRadius: Common.getLengthByIPhone7(13),
        flex: 0,
    }}>
        <Text style={{
            color: 'red',
            fontFamily: 'Montserrat-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(14),
            lineHeight: Common.getLengthByIPhone7(25),
        }}
        allowFontScaling={false}>
            ЛИКВИДИРОВАНО
        </Text>
    </View>);

    if (this.state.data.data.state.status === 'ACTIVE') {
        status = (<View style={{
            width: Common.getLengthByIPhone7(110),
            marginBottom: Common.getLengthByIPhone7(5),
            alignItems: 'center',
            justifyContent: 'center',
            borderColor: 'green',
            borderWidth: 1,
            borderRadius: Common.getLengthByIPhone7(13),
            flex: 0,
        }}>
            <Text style={{
                color: 'green',
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(14),
                lineHeight: Common.getLengthByIPhone7(25),
            }}
            allowFontScaling={false}>
                ДЕЙСТВУЕТ
            </Text>
        </View>);
    }

    let founders = null;

    if (this.state.data.data.founders !== null && this.state.data.data.founders !== undefined) {
        let array = [];
        for (let i = 0; i < this.state.data.data.founders.length; i++) {
            if (this.state.data.data.founders[i].fio !== undefined && this.state.data.data.founders[i].fio !== null) {
                array.push(this.renderFounder(this.state.data.data.founders[i].fio.source, 'Доля: ' + (this.state.data.data.capital.value*this.state.data.data.founders[i].share.value/100) + ` P` + ' (' + this.state.data.data.founders[i].share.value + '%)', 'ИНН: ' + this.state.data.data.founders[i].inn));
            } else {
                array.push(this.renderFounder(this.state.data.data.founders[i].name, 'Доля: ' + (this.state.data.data.capital.value*this.state.data.data.founders[i].share.value/100) + ` P` + ' (' + this.state.data.data.founders[i].share.value + '%)', 'ИНН: ' + this.state.data.data.founders[i].inn));
            }
        }

        founders = (<View style={{
            marginTop: Common.getLengthByIPhone7(10),
            width: Common.getLengthByIPhone7(0),
            padding: Common.getLengthByIPhone7(20),
            backgroundColor: 'white',
        }}>
            <Text style={{
                marginBottom: Common.getLengthByIPhone7(5),
                color: Colors.textColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(18),
            }}
            allowFontScaling={false}>
                Учредители {this.state.data.value}
            </Text>
            {array}
        </View>);
    }

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: Colors.backColor,
        marginTop: (isIphoneX() ? Common.getLengthByIPhone7(18) : 0),
      }}>
            <ScrollView style={{
                width: Common.getLengthByIPhone7(0),
                flex: 1,
                marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? 86 : 52) : 52,
            }}
            contentContainerStyle={{
                alignItems: 'center',
            }}>
                <View style={{
                    marginTop: Common.getLengthByIPhone7(20),
                    width: Common.getLengthByIPhone7(0),
                    padding: Common.getLengthByIPhone7(20),
                    backgroundColor: 'white',
                }}>
                    {status}
                    <Text style={{
                        marginBottom: Common.getLengthByIPhone7(5),
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                        color: Colors.textColor,
                        fontFamily: 'Montserrat-Bold',
                        fontWeight: 'bold',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(24),
                    }}
                    allowFontScaling={false}>
                        {this.state.data.value}
                    </Text>
                    <Text style={{
                        color: Colors.textGrayColor,
                        fontFamily: 'Montserrat-Bold',
                        fontWeight: 'bold',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(14),
                    }}
                    allowFontScaling={false}>
                        {this.state.data.data.name.full_with_opf}
                    </Text>
                </View>
                <View style={{
                    marginTop: Common.getLengthByIPhone7(10),
                    width: Common.getLengthByIPhone7(0),
                    padding: Common.getLengthByIPhone7(20),
                    backgroundColor: 'white',
                }}>
                    <Text style={{
                        marginBottom: Common.getLengthByIPhone7(5),
                        color: Colors.textColor,
                        fontFamily: 'Montserrat-Bold',
                        fontWeight: 'bold',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(22),
                    }}
                    allowFontScaling={false}>
                        Основные показатели
                    </Text>
                    <Text style={{
                        color: Colors.textColor,
                        fontFamily: 'Montserrat-Regular',
                        fontWeight: 'normal',
                        textAlign: 'left',
                        fontSize: Common.getLengthByIPhone7(14),
                        marginBottom: Common.getLengthByIPhone7(10),
                    }}
                    allowFontScaling={false}>
                        Данные на {Common.getRusDate(new Date(this.state.data.data.state.actuality_date))}
                    </Text>
                    <View style={{
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}>
                        <View style={{
                            width: Common.getLengthByIPhone7(150),
                            minHeight: Common.getLengthByIPhone7(80),
                            borderRadius: Common.getLengthByIPhone7(10),
                            borderWidth: 1,
                            borderColor: Colors.textGrayColor,
                            padding: Common.getLengthByIPhone7(10),
                        }}>
                            <Text style={{
                                marginBottom: Common.getLengthByIPhone7(5),
                                color: Colors.textGrayColor,
                                fontFamily: 'Montserrat-Bold',
                                fontWeight: 'bold',
                                textAlign: 'left',
                                fontSize: Common.getLengthByIPhone7(15),
                            }}
                            allowFontScaling={false}>
                                Уставный капитал
                            </Text>
                            {this.state.data.data.capital !== null && this.state.data.data.capital !== undefined ? (<Text style={{
                                color: Colors.textColor,
                                fontFamily: 'Montserrat-Regular',
                                fontWeight: 'normal',
                                textAlign: 'left',
                                fontSize: Common.getLengthByIPhone7(14),
                            }}
                            allowFontScaling={false}>
                                {this.state.data.data.capital.value} &#8381;
                            </Text>) : (<Text style={{
                                color: Colors.textColor,
                                fontFamily: 'Montserrat-Regular',
                                fontWeight: 'normal',
                                textAlign: 'left',
                                fontSize: Common.getLengthByIPhone7(14),
                            }}
                            allowFontScaling={false}>
                                н/д
                            </Text>)}
                        </View>
                    </View>
                </View>
                <View style={{
                    marginTop: Common.getLengthByIPhone7(10),
                    width: Common.getLengthByIPhone7(0),
                    padding: Common.getLengthByIPhone7(20),
                    backgroundColor: 'white',
                }}>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                    }}>
                        <Text style={{
                            marginBottom: Common.getLengthByIPhone7(5),
                            color: Colors.textColor,
                            fontFamily: 'Montserrat-Bold',
                            fontWeight: 'bold',
                            textAlign: 'left',
                            fontSize: Common.getLengthByIPhone7(15),
                        }}
                        allowFontScaling={false}>
                            Реквизиты {this.state.data.value}
                        </Text>
                        <TouchableOpacity style={{
                            marginLeft: Common.getLengthByIPhone7(10),
                        }}
                        onPress={() => {
                            Clipboard.setString('Реквизиты: '+this.state.data.value
                            +`\nИНН: `+this.state.data.data.inn
                            +`\nКПП: `+this.state.data.data.kpp
                            +`\nОГРН: `+this.state.data.data.ogrn
                            +`\nОКПО: `+this.state.data.data.okpo
                            +`\nДата создания: `+Common.getRusDate(new Date(this.state.data.data.state.registration_date)));
                            Toast.show('Реквизиты скопированы');
                        }}>
                            <Image
                                source={require('./../assets/ic-copy.png')}
                                style={{
                                    width: Common.getLengthByIPhone7(17),
                                    height: Common.getLengthByIPhone7(17),
                                    resizeMode: 'contain',
                                    tintColor: Colors.textColor,
                                }}
                            />
                        </TouchableOpacity>
                    </View>
                    {this.renderField('ИНН', this.state.data.data.inn)}
                    {this.renderField('КПП', this.state.data.data.kpp)}
                    {this.renderField('ОГРН', this.state.data.data.ogrn)}
                    {this.renderField('ОКПО', this.state.data.data.okpo)}
                    {this.renderField('Дата создания', Common.getRusDate(new Date(this.state.data.data.state.registration_date)))}
                    {this.renderField('Кол-во сотрудников', this.state.data.data.employee_count !== null ? this.state.data.data.employee_count : 'н/д')}
                    <View style={{
                        marginTop: Common.getLengthByIPhone7(25),
                        width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                        height: 1,
                        backgroundColor: 'rgba(0, 0, 0, 0.2)'
                    }} />
                    {this.renderOkved(this.state.data.data.okveds)}
                    {this.state.data.data.documents.smb !== null && this.state.data.data.documents.smb !== undefined ? this.renderField2('Реестр МСП', this.state.data.data.documents.smb.category === 'MICRO' ? 'Микропредприятие' : 'Среднее предприятие', 'с ' + Common.getRusDate(new Date(this.state.data.data.documents.smb.issue_date))) : null}
                    {this.renderField2('Налоговый орган', this.state.data.data.authorities.fts_report.name, 'с ' + Common.getRusDate(new Date(this.state.data.data.state.registration_date)))}
                    {this.renderField2('Юридический адрес', this.state.data.data.address.value, null)}
                    {this.state.data.data.management !== null && this.state.data.data.management !== undefined ? this.renderField2('Руководитель', this.state.data.data.management.post, this.state.data.data.management.name) : null}
                </View>
                {founders}
                <View style={{
                    width: Common.getLengthByIPhone7(0),
                    height: Common.getLengthByIPhone7(30),
                }} />
            </ScrollView>
      </View>);
  }
}
