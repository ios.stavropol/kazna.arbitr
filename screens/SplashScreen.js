import React from 'react';
import {
  Platform,
  View,
  Image,
  Animated,
  Easing,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, {getProfile} from './../Utilites/Network';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class SplashScreen extends React.Component {

  spinValue = new Animated.Value(0);

  state = {

  };

  UNSAFE_componentWillMount() {
    // this.props.navigation.navigate('Login');
    Animated.loop(
      Animated.timing(
        this.spinValue,
        {
         toValue: 1,
         duration: 1000,
         easing: Easing.linear,
         useNativeDriver: true
        }
      )
     ).start();
    // return;
    AsyncStorage.getItem('order')
    .then(order => {
      if (order !== null && order !== undefined) {
        order = JSON.parse(order);
        if (order !== null) {
          Network.order = order;
        } else {
          Network.order = null;
        }
      } else {
        Network.order = null;
      }
      this.starter();
    })
    .catch(err => {
      Network.order = null;
      this.starter();
    });
  }

  UNSAFE_componentWillUnmount() {
    
  }

  constructor(props) {
    super(props);
  }

  starter = () => {
    AsyncStorage.getItem('token')
    .then(token => {
      if (token != null && token !== undefined && token.length) {
        Network.access_token = token;
        getProfile()
        .then(() => {
          if (Network.userProfile.first_name == null) {
            this.props.navigation.navigate('Profile', {from: 'login'});
          } else {
            this.props.navigation.navigate('Main');
          }
        })
        .catch(err => {
          this.props.navigation.navigate('LoginTab');
        });
      } else {
        this.props.navigation.navigate('LoginTab');
      }
    })
    .catch(err => {
      this.props.navigation.navigate('LoginTab');
    });
  }

  render() {

    return (<View style={{
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: Colors.mainBackgroundColor,
      }}>
        <Image
          source={require('./../assets/splash.png')}
          style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
            resizeMode: 'cover',
        }}/>
        <Animated.Image
        style={{
          width: Common.getLengthByIPhone7(65),
          height: Common.getLengthByIPhone7(65),
          position: 'absolute',
          bottom: Common.getLengthByIPhone7(180),
          transform: [{rotate: this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
          })}] 
        }}
        source={require('./../assets/ic-loader.png')} />
      </View>);
  }
}
