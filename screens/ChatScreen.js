import React from 'react';
import {
  Platform,
  Text,
  View,
  Alert,
  BackHandler,
  ScrollView,
  Linking,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Keyboard,
  StatusBar
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, { getMessages, sendMessage } from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { GiftedChat, Send, Bubble } from 'react-native-gifted-chat';
import ru from 'dayjs/locale/ru';

export default class ChatScreen extends React.Component {

  chatInterval = null;

  state = {
    loading: false,
    messages: [],
  };

  UNSAFE_componentWillMount() {

    BackHandler.addEventListener(
      "hardwareBackPress",
      () => {
          this.props.navigation.goBack(null);
          return true;
      }
    );

    this.refreshChat();
    if (Platform.OS === 'android') {
      this.chatInterval = setInterval(() => {
        this.refreshChat();
      }, 5000);
    }
    
    Network.refreshChat = this.refreshChat;

    this.props.navigation.addListener('willFocus', () => {
      if (Platform.OS === 'ios') {
        StatusBar.setBarStyle('light-content', true);
      }
    });
  }

  UNSAFE_componentWillUnmount() {
    if (this.chatInterval) {
      clearInterval(this.chatInterval);
    }
  }

  constructor(props) {
    super(props);

  }

  refreshChat = () => {
    getMessages()
    .then(messages => {
      let array = [];

      for (let i = 0; i < messages.length; i++) {
        array.push({
          _id: Math.random(),
          text: messages[i].text,
          createdAt: new Date(messages[i].datetime),
          user: {
            _id: messages[i].is_admin_answer ? 0 : Network.userProfile.id,
            name: messages[i].admin_name,
          }
        });
      }
      array.sort((a, b) => {
        return a.createdAt < b.createdAt;
      });
      this.setState({
        messages: array,
      }, () => {
        
      });
    })
    .catch(err => {

    });
  }

  render() {

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
        <View style={{
          width: Common.getLengthByIPhone7(0),
          // height: Common.getLengthByIPhone7(100),
          height: isIphoneX() ? Common.getLengthByIPhone7(98) : (Platform.OS === 'ios' ? Common.getLengthByIPhone7(68) + 20 : Common.getLengthByIPhone7(68)),
          backgroundColor: Colors.mainColor,
          justifyContent: 'flex-end',
          // alignItems: 'flex-end',
          // flexDirection: 'row',
        }}>
          <View style={{
            marginBottom: Common.getLengthByIPhone7(20),
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
            <TouchableOpacity style={{
              marginLeft: Common.getLengthByIPhone7(20),
            }}
            onPress={() => {
              this.props.navigation.navigate('Main');
            }}>
              <Image source={require('./../../assets/ic-arrow-back.png')} style={{
                resizeMode: 'contain',
                width: Common.getLengthByIPhone7(24),
                height: Common.getLengthByIPhone7(24),
                tintColor: 'white',
              }} />
            </TouchableOpacity>
            <Text style={{
                marginLeft: Common.getLengthByIPhone7(20),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: Colors.orangeColor,
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(24),
            }}
            allowFontScaling={false}>
                L<Text style={{
                // marginTop: Common.getLengthByIPhone7(105),
                // marginTop: (isIphoneX() ? Common.getLengthByIPhone7(65) : Common.getLengthByIPhone7(65)),
                width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                color: 'white',
                fontFamily: 'Montserrat-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(24),
            }}
            allowFontScaling={false}>
                egal.Track чат
            </Text>
            </Text>
          </View>
          </View>
            <View style={{
                width: Common.getLengthByIPhone7(0),
                flex: 1,
                marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? 86 : 52) : 52,
            }}>
                <GiftedChat
                    ref={ref => this.giftedChatRef = ref}
                    messages={this.state.messages}
                    extraData={this.state.messages}
                    bottomOffset={Platform.OS == 'ios' ? 70 : 0}
                    keyboardShouldPersistTaps='handled'
                    alwaysShowSend={true}
                    showUserAvatar={false}
                    renderAvatar={null}
                    locale={ru}
                    dateFormat='L'
                    isAnimated={true}
                    inverted={true}
                    placeholder={'Отправьте сообщение...'}
                    renderBubble={props => {
                      return (
                        <Bubble
                          {...props}
                          wrapperStyle={{
                            right: {
                              backgroundColor: Colors.mainColor,
                            },
                          }}
                        />
                      );
                    }}
                    renderSend={props => {
                      return (<Send 
                        {...props}
                        label={'Отпр.'}
                        textStyle={{
                          color: Colors.mainColor,
                        }}
                        >

                        </Send>);
                    }}
                    onSend={messages => {
                      // console.warn(messages);
                      if (messages !== null && messages.length) {
                        sendMessage(messages[0].text)
                        .then(() => {
                          this.refreshChat();
                          Keyboard.dismiss();
                        })
                        .catch(err => {

                        });
                      }
                    }}
                    user={{
                        _id: Network.userProfile.id,
                    }}
                />
            </View>
      </View>);
  }
}
