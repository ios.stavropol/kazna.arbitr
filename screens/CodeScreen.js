import React from 'react';
import {
  Dimensions,
  Text,
  View,
  Alert,
  TextInput,
  ImageBackground,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Keyboard,
} from 'react-native';
import Colors from './../constants/Colors';
import Config from './../constants/Config';
import Common from './../Utilites/Common';
import Network, {sendCode, getProfile} from './../Utilites/Network';
import Spinner from 'react-native-loading-spinner-overlay';
import AnimatedButton from '../components/Buttons/AnimatedButton';

export default class CodeScreen extends React.Component {

  calculate = true;
  top = new Animated.Value(0);
  spinValue = new Animated.Value(0);

  state = {
    code: '',
    loading: false,
    buttonY: 0,
    buttonH: 0,
    viewY: 0,
    viewH: 0,
  };

  UNSAFE_componentWillMount() {
    this.props.navigation.addListener('willFocus', this.willFocus)
    this.props.navigation.addListener('willBlur', this.willBlur)
  }

  UNSAFE_componentWillUnmount() {

  }

  constructor(props) {
    super(props);

  }

  willFocus = () => {
    Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  willBlur = () => {
    Keyboard.removeListener('keyboardDidShow');
    Keyboard.removeListener('keyboardDidHide');
  }

  _keyboardDidHide = () => {
    this.calculate = false;
    Animated.timing(this.top, {
      toValue: (Dimensions.get('window').height - this.state.viewH)/2,
      duration: 300,
    }).start();
  }

  _keyboardDidShow = e => {
    this.calculate = false;
    let kbView = (Dimensions.get('window').height - e.endCoordinates.height);
    let view = this.state.buttonY+this.state.viewY;
    if (view + Common.getLengthByIPhone7(50) > kbView) {
      Animated.timing(this.top, {
        toValue: (Dimensions.get('window').height - this.state.viewH)/2 - (view + Common.getLengthByIPhone7(50) - kbView),
        duration: 300,
      }).start();
    }
  }

  nextClick = () => {
    if (this.state.code.length) {
      this.setState({
        loading: true,
      }, () => {
        sendCode(this.props.navigation.state.params.phone, this.state.code)
        .then(() => {
          getProfile()
          .then(() => {
            this.setState({
              loading: false,
            }, () => {
              if (Network.userProfile.first_name == null) {
                this.props.navigation.navigate('Profile', {from: 'login'});
              } else {
                this.props.navigation.navigate('Main');
              }
            });
          })
          .catch(err => {
            this.setState({
              loading: false,
            }, () => {
              setTimeout(() => {
                Alert.alert(Config.appName, err);
              }, 100);
            });
          });
        })
        .catch(err => {
          this.setState({
            loading: false,
          }, () => {
            setTimeout(() => {
              Alert.alert(Config.appName, err);
            }, 100);
          });
        });
      });
    } else {
      Alert.alert(Config.appName, 'Введите СМС код!');
    }
  }

  onLayout = e => {
    if (this.calculate) {
      this.setState({
        buttonH: e.nativeEvent.layout.height,
        buttonY: e.nativeEvent.layout.y,
      })
    }
  }

  onLayout2 = e => {
    if (this.calculate) {
      this.setState({
        viewY: e.nativeEvent.layout.y,
        viewH: e.nativeEvent.layout.height,
      }, () => {
        Animated.timing(this.top, {
          toValue: (Dimensions.get('window').height - this.state.viewH)/2,
          duration: 0,
        }).start();
      })
    }
  }

  render() {

    return (<View style={{
        flex: 1,
        width: Common.getLengthByIPhone7(0),
        // height: Dimensions.get('window').height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
        <Animated.View style={{
          width: Common.getLengthByIPhone7(343),
          borderRadius: Common.getLengthByIPhone7(25),
          paddingLeft: Common.getLengthByIPhone7(30),
          paddingRight: Common.getLengthByIPhone7(30),
          paddingTop: Common.getLengthByIPhone7(30),
          paddingBottom: Common.getLengthByIPhone7(30),
          backgroundColor: 'white',
          shadowColor: '#D7DEEC',
          shadowOffset: { width: 0, height: Common.getLengthByIPhone7(5) },
          shadowOpacity: 1,
          shadowRadius: Common.getLengthByIPhone7(20),
          elevation: 6,
          alignItems: 'center',
          position: 'absolute',
          top: this.top,
        }}
        onLayout={this.onLayout2}>
          <Text style={{
            color: Colors.textColor,
            fontFamily: 'Rubik',
            fontWeight: '500',
            textAlign: 'center',
            fontSize: Common.getLengthByIPhone7(24),
          }}
          allowFontScaling={false}>
              {`Вход`}
          </Text>
          <TextInput
            style={{
              width: Common.getLengthByIPhone7(283),
              height: Common.getLengthByIPhone7(50),
              borderRadius: Common.getLengthByIPhone7(15),
              borderWidth: 1,
              borderColor: Colors.grayBorderColor,
              marginTop: Common.getLengthByIPhone7(30),
              fontFamily: 'Rubik-Regular',
              textAlign: 'center',
              // lineHeight: Common.getLengthByIPhone7(20),
              fontSize: Common.getLengthByIPhone7(16),
              color: Colors.textColor,
            //   paddingLeft: Common.getLengthByIPhone7(10),
            }}
            placeholderTextColor={Colors.placeholderColor}
            placeholder={'СМС код'}
            contextMenuHidden={false}
            autoCorrect={false}
            autoCompleteType={'off'}
            inputAccessoryViewID={this.props.inputAccessoryViewID}
            returnKeyType={'done'}
            secureTextEntry={false}
            autoCapitalize={this.props.autoCapitalize}
            keyboardType={'number-pad'}
            allowFontScaling={false}
            underlineColorAndroid={'transparent'}
            onSubmitEditing={() => {
              this.nextClick();
            }}
            ref={el => (this.codeRef = el)}
            onFocus={() => {}}
            onBlur={() => {
              
            }}
            onChangeText={code => {
              this.setState({
                code,
              });
            }}
            value={this.state.code}
          />
          <AnimatedButton style={{
            width: Common.getLengthByIPhone7(283),
            height: Common.getLengthByIPhone7(50),
            marginTop: Common.getLengthByIPhone7(20),
            marginBottom: Common.getLengthByIPhone7(20),
          }}
          onLayout={this.onLayout}
          onPress={() => {
            this.nextClick();
          }}
          title={'Далее'}
          activeColor={Colors.mainColor}
          inactiveColor={'white'}/>
        </Animated.View>
        <Spinner
          visible={this.state.loading}
          // textContent={'Загрузка...'}
          onShow={() => {
            Animated.loop(
              Animated.timing(
                this.spinValue,
                {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear,
                useNativeDriver: true
                }
              )
            ).start();
          }}
          onDismiss={() => {

          }}
          customIndicator={<View style={{
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 20000,
          }}>
            <View style={{
              width: Common.getLengthByIPhone7(65),
              height: Common.getLengthByIPhone7(65),
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <Animated.Image
                style={{
                  width: Common.getLengthByIPhone7(65),
                  height: Common.getLengthByIPhone7(65),
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  transform: [{rotate: this.spinValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '360deg']
                  })}] 
                }}
                source={require('./../assets/ic-loader.png')}
              />
              <TouchableOpacity style={{
                // marginTop: Common.getLengthByIPhone7(20),
                padding: Common.getLengthByIPhone7(5),
                backgroundColor: 'rgba(255, 255, 255, 0.8)',
                borderRadius: Common.getLengthByIPhone7(5),
              }}
              onPress={() => {
                this.setState({
                  loading: false,
                });
              }}>
                <Image
                  style={{
                    width: Common.getLengthByIPhone7(18),
                    height: Common.getLengthByIPhone7(18),
                  }}
                  source={require('./../assets/ic-loader-close.png')}
                />
              </TouchableOpacity>
            </View>
          </View>}
          overlayColor={'rgba(32, 42, 91, 0.3)'}
          textStyle={{color: '#FFF'}}
        />
      </View>);

{/* <ImageBackground
source={require('./../assets/splash.png')}
style={{
  width: Common.getLengthByIPhone7(0),
  flex: 1,
  // height: Dimensions.get('window').height,
  resizeMode: 'cover',
  alignItems: 'center',
  justifyContent: 'center',
}}> */}
  }
}
