import React, {Component} from 'react';
import {Platform, StatusBar, AsyncStorage} from 'react-native';
import { createRootNavigator } from "./router";
import NavigationService from './Utilites/NavigationService'
import Colors from './constants/Colors';

type Props = {};
export default class App extends Component<Props> {

  UNSAFE_componentWillMount() {
    if(Platform.OS === "android") {
      StatusBar.setBackgroundColor(Colors.fonColor, true);
      StatusBar.setBarStyle('dark-content', true);
    } else {
      StatusBar.setBarStyle('dark-content', true);
    }
  }

  render() {
    const Layout = createRootNavigator();
    return <Layout ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}/>;
  }
}
